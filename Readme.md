Pau's Asset Manager is GPL v3 licensed Java Web application 
aimed to provide an integrated asset manager for properties,
stock investments and general incomes and expenses.
The application is designed to ease the calculation of the
Spanish fiscal system taxes.

Even though it is explicitly said in the GPL v3, you must
use this application *at your own risk*. 