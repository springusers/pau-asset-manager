/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.business;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.Property;
import org.pau.assetmanager.entities.PropertyBook;

/**
 * 
 * This class contains database methods related to Books {@link org.pau.assetmanager.entities.Book}
 * 
 * @author Pau Carré Cardona
 *
 */

public class BooksBusiness {


	/**
	 * @param client client from which the books are returned
	 * 
	 * @return all the books from a given client
	 */
	public static List<Book> getBooksFromClient(Client client) {
		String query = "select book from Book book where book.client = :client";
		List<Book> books  = DaoFunction.<Book> querySimpleListFunction("client", client).apply(query);
		return books;
	}

	/**
	 * @param client
	 * @return all the property-related books from a client
	 */
	public static List<PropertyBook> getPropertyBooksFromClient(Client client) {
		String query = "select propertyBook from PropertyBook propertyBook where propertyBook.client = :client";
		List<PropertyBook> books  = DaoFunction.<PropertyBook> querySimpleListFunction("client", client).apply(query);
		return books;
	}
	
	/**
	 * This method stores a new into the database
	 * 
	 * @param book the book to store
	 * 
	 * @return the stored book 
	 */
	public static <T extends Book> T addBook(T book) {
		DaoFunction.persistFunction().apply(book);
		return book;
	}


	/**
	 * This method remove a book from the database together with all its annotations
	 * 
	 * @param book the book to be removed form the database
	 */
	public static void removeBook(Book book) {
		DaoFunction<Book, Void> removeBookFunction = new DaoFunction<Book, Void>() {
			@Override
			protected Void doInTransaction(EntityManager entityManager, Book book
					) {
				Query query = entityManager
						.createQuery("select book from Book book where book=:book");
				query.setParameter("book", book);
				book = (Book) query.getSingleResult();
				// remove direct annotations
				Query queryForAnnotations = entityManager
						.createQuery("select annotation from Annotation annotation where annotation.book=:book");
				queryForAnnotations.setParameter("book", book);
				@SuppressWarnings("unchecked")
				List<Annotation> anotations =  queryForAnnotations
						.getResultList();
				for (Annotation annotation : anotations) {
					entityManager.remove(annotation);
				}
				// remove the book
				entityManager.remove(book);
				return null;
			}
		};
		removeBookFunction.apply(book);
	}

	/**
	 * @return all the books form all the clients in the database
	 */
	public static List<Book> getBooks() {
		String query = "select book from Book book";
		List<Book> books  = DaoFunction.<Book> querySimpleListFunction().apply(query);
		return books;
	}

	/**
	 * @param property from which the books are retrieved
	 * @return the collection of property books liked with 'property'
	 */
	public static Collection<PropertyBook> getBooksFromProperty(Property property) {
		DaoFunction<Property, Collection<PropertyBook>> removeBookFunction = new DaoFunction<Property, Collection<PropertyBook>>() {
			@Override
			protected Collection<PropertyBook> doInTransaction(EntityManager entityManager, Property property
					) {
				Query query = entityManager
						.createQuery("select propertyBook from PropertyBook propertyBook where propertyBook.property=:property");
				query.setParameter("property", property);
				@SuppressWarnings("unchecked")
				List<PropertyBook> propertyBooks = (List<PropertyBook>) query.getResultList();
				return propertyBooks;
			}
		};
		return removeBookFunction.apply(property);
	}
}
