/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.business;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.User;
import org.pau.assetmanager.entities.UserClientAssociation;
import org.pau.assetmanager.utils.AssetManagerRuntimeException;
import org.pau.assetmanager.utils.UserUtil;

import com.google.common.base.Optional;

/**
 * 
 * This class contains database methods related to Clients {@link org.pau.assetmanager.entities.Client}
 * 
 * 
 * @author Pau Carré Cardona
 *
 */

public class ClientsBusiness {
	
	private static Logger logger = LogManager
			.getLogger(ClientsBusiness.class);
	
	/**
	 * This method removes a client from the database 
	 * 
	 * @param selectedClient client to remove
	 * 
	 * @return the client removed
	 */
	public static Client removeClient(Client selectedClient) {
		String queryBooks = "select book from Book book where book.client=:client";
		List<Book> books =  DaoFunction.<Book>querySimpleListFunction("client", selectedClient).apply(queryBooks);
		for (Book book : books) {
			BooksBusiness.removeBook(book);
		}
		String queryClients = "select client from Client client where client=:client";
		Optional<Client> optionalClient  =  DaoFunction.<Client>querySimpleUniqueFunction("client", selectedClient).apply(queryClients);
		if(optionalClient.isPresent()){
			Client client = optionalClient.get();
			DaoFunction.deleteDetachedFunction().apply(client);
			return client;
		}else{
			String message = "Client '"+selectedClient.toString()+"' not found in the database";
			logger.error(message);
			throw new AssetManagerRuntimeException(message);
		}		
	}
	
	/**
	 * @return returns all the clients from the current user logged in the database
	 */
	public static List<Client> getClients() {
		User user = UserUtil.getExecutionUser();
		return getClientsFromUser(user);
	}
	
	/**
	 * @param user
	 * @return returns the list of clients from the user
	 */
	public static List<Client> getClientsFromUser(User user) {
		String query = "select userClientAssociation.client from  UserClientAssociation userClientAssociation "
				+ "where userClientAssociation.user = :user";
		List<Client> clients =  DaoFunction.<Client>querySimpleListFunction("user", user).apply(query);
		return clients;
	}
	
	/**
	 * @param name of the client
	 * 
	 * @return the client that has the name of the parameter
	 */
	public static Optional<Client> getClientByName(String name) {
		String query = "select client from Client client where client.name=:name";
		Optional<Client> optionalClient  =  DaoFunction.<Client>querySimpleUniqueFunction("name", name).apply(query);
		return optionalClient;
	}
	
	/**
	 * Adds a client into the database given a client name
	 * 
	 * @param clientName the name of the client to be inserted in the database
	 */
	public static Client addClientByClientName(final User user, String clientName) {
		DaoFunction<String, Client> addClientFuncion = new DaoFunction<String, Client>() {
			@Override
			protected Client doInTransaction(
					EntityManager entityManager, String clientName) {
				Client client = new Client();
				client.setName(clientName);
				entityManager.persist(client);
				UserClientAssociation userClientAssociation = new UserClientAssociation();			
				userClientAssociation.setUser(user);
				userClientAssociation.setClient(client);
				entityManager.persist(userClientAssociation);
				return client;
			}
		};
		return addClientFuncion.apply(clientName);
	}

	/**
	 * @return the list of all the clients available in the system
	 */
	public static List<Client> getAllClients() {
		String query = "select client from Client client";
		List<Client> clients  =  DaoFunction.<Client>querySimpleListFunction().apply(query);
		return clients;
	}

	/**
	 * @param user
	 * @return the list of clients not related with the 'user' passed as parameter
	 */
	public static List<Client> getClientsNotLinkedWithUser(User user) {
		String query = "select client from Client client "
				+ "where client not in ( select userClientAssociation.client from UserClientAssociation userClientAssociation "
				+ "where userClientAssociation.user = :user)";
		List<Client> clients =  DaoFunction.<Client>querySimpleListFunction("user", user).apply(query);
		return clients;
	}

}
