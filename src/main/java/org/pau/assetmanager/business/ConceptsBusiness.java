/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pau.assetmanager.entities.PredefinedConcept;
import org.pau.assetmanager.utils.AssetManagerRuntimeException;
import org.pau.assetmanager.viewmodel.utils.BookSelection;

import com.google.common.base.Optional;

/**
 * This class contains database methods related to Concepts {@link org.pau.assetmanager.entities.Concept}
 * 
 * @author Pau Carré Cardona
 *
 */

public class ConceptsBusiness {

	private static final Logger logger = LogManager.getLogger(ConceptsBusiness.class);
		  
	/**
	 * @param annotationClassName name of the class of the annotation
	 * 
	 * @return the default concepts given the simple class name of a Annotation
	 */
	public static String getDefaultConcept(String annotationClassName) {
		String queryString = "select predefinedConcept from PredefinedConcept predefinedConcept where predefinedConcept.isDefault = true and predefinedConcept.annotationClassName = :annotationClassName order by predefinedConcept.name asc";
		Optional<PredefinedConcept> optionalPredefinedConcept = DaoFunction
				.<PredefinedConcept> querySimpleUniqueFunction("annotationClassName",
						annotationClassName).apply(queryString);
		//we assume there should be one predefined concept for all the annotation classes
		if (optionalPredefinedConcept.isPresent()) {
			return optionalPredefinedConcept.get().getName();
		}
		String message = "'" + annotationClassName
				+ "' annotation class name has no default predefined concept";
		logger.error(message);
		throw new AssetManagerRuntimeException(message);
	}

	/**
	 * @param selectedBook book selection to have into account
	 * 
	 * @return all the concept names involved in the annotations of the book selection
	 */
	public static List<String> getConceptsUsedInBook(BookSelection bookSelection) {
		if (!bookSelection.isAllBooks()) {
			String queryString = "select distinct annotation.concept from Annotation annotation inner join annotation.book book where book = :book order by concept asc";
			List<String> currentConcepts = DaoFunction
					.<String> querySimpleListFunction("book", bookSelection.getSelectedBook()).apply(queryString);
			return currentConcepts;
		} else {
			String queryString = "select distinct annotation.concept from Annotation annotation inner join annotation.book book inner join book.client client where client = :client order by concept asc";
			List<String> currentConcepts = DaoFunction
					.<String>  querySimpleListFunction("client", bookSelection.getSelectedClient()).apply(queryString);
			return currentConcepts;
		}
	}

}
