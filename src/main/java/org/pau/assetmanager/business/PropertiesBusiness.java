/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.pau.assetmanager.business;

import java.util.List;

import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.Property;

/**
 * This class contains database methods related to Properties {@link org.pau.assetmanager.entities.Property}
 * 
 * @author Pau Carré Cardona
 *
 */
public class PropertiesBusiness {
	
	public static Property createProperty(Property property) {
		DaoFunction.persistFunction().apply(property);
		return property;
	}
	
	/**
	 * @param client from whom the properties are retrieved
	 * 
	 * @return the list of properties from the client
	 */
	public static List<Property> getPropertiesByClient(Client client) {
		String queryString = "select property from Property property where property.client=:client";
		List<Property> properties = DaoFunction.<Property>querySimpleListFunction("client", client).apply(queryString);
		return properties;
	}
	
	/**
	 * This method removes the properties from a client that are not linked with any book (i.e. unused)
	 * 
	 * @param client the clent from whom the properties are removed
	 */
	public static void removePropertiesOfUsersWithoutBooksByClient(Client client) {
		String queryString = "select property from Property property where property.client=:client";
		List<Property> propertiesToRemove = DaoFunction.<Property>querySimpleListFunction("client", client).apply(queryString);
		for (Property property : propertiesToRemove) {
			Boolean propertyHasBooks = propertyHasBooks(property);
			if (!propertyHasBooks) {
				DaoFunction.deleteDetachedFunction().apply(property);
			}
		}
	}

	/**
	 * @param property from which it is check the number of books
	 * 
	 * @return whether the property is linked with at least one book
	 */
	private static  Boolean propertyHasBooks(Property property) {
		String queryString = "select propertyBook from PropertyBook propertyBook where propertyBook.property=:property";
		List<Book> books = DaoFunction.<Book>querySimpleListFunction("property", property).apply(queryString);
		Boolean propertyHasBooks = books.size() > 0;
		return propertyHasBooks;
	}
	
}
