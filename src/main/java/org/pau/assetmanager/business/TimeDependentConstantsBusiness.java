/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.business;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pau.assetmanager.entities.TimeDependentConstantValue;
import org.pau.assetmanager.utils.AssetManagerRuntimeException;
import org.pau.assetmanager.viewmodel.type.TimeDependentConstantType;

import com.google.common.base.Optional;

/**
 * This class contains database methods related to TimeDependentConstantValue {@link org.pau.assetmanager.entities.TimeDependentConstantValue}
 * and TimeDependentConstant {@link org.pau.assetmanager.entities.TimeDependentConstant}
 * 
 * @author Pau Carré Cardona
 *
 */

public class TimeDependentConstantsBusiness {

	private static final Logger logger = LogManager.getLogger(TimeDependentConstantsBusiness.class);
	
	/**
	 * @param year year in which the constant is applied
	 * @param timeDependentConstantType type of constant to retrieve
	 * 
	 * @return returns the constant value for the given year, and it case there is non for that year, the last one
	 */
	public static TimeDependentConstantValue getTimeDependentConstantValueByYearAndTimeDependentConstantType(
			Integer year, TimeDependentConstantType timeDependentConstantType) {
		String queryString = "select timeDependentConstantValue from "
				+ "TimeDependentConstantValue timeDependentConstantValue "
				+ "inner join fetch timeDependentConstantValue.timeDependentConstant timeDependentConstant where "
				+ "timeDependentConstant.timeDependentConstantType = :timeDependentConstantType and "
				+ "timeDependentConstantValue.year = :year";
		Optional<TimeDependentConstantValue> timeDependentConstantValueOptional = DaoFunction
				.<TimeDependentConstantValue> querySimpleUniqueFunction("year", year,
						"timeDependentConstantType",
						timeDependentConstantType).apply(queryString);
		if (timeDependentConstantValueOptional.isPresent()) {
			// constant fount for a given year and type
			return timeDependentConstantValueOptional.get();
		} else {
			// if the constant can not be found, it returns the newest constant of the same type
			return getTimeDependentConstantValueByTimeDependentConstantType(timeDependentConstantType);
		}

	}

	/**
	 * @param timeDependentConstantType the type of constant
	 * 
	 * @return the last value for the given constant
	 */
	public static TimeDependentConstantValue getTimeDependentConstantValueByTimeDependentConstantType(
			TimeDependentConstantType timeDependentConstantType) {
		String queryMaxYearString = "select timeDependentConstantValue from "
				+ "TimeDependentConstantValue timeDependentConstantValue "
				+ "inner join fetch timeDependentConstantValue.timeDependentConstant timeDependentConstant where "
				+ "timeDependentConstant.timeDependentConstantType = :timeDependentConstantType "
				+ "order by timeDependentConstantValue.year desc";
		List<TimeDependentConstantValue> timeDependentConstantValues = DaoFunction
				.<TimeDependentConstantValue> querySimpleListFunction("timeDependentConstantType",
						timeDependentConstantType).apply(queryMaxYearString);
		if (timeDependentConstantValues.size() == 0) {
			// there is no value for the given type, major error
			String message = "No TimeDependentConstantValue found for type '"
					+ timeDependentConstantType.name() + "'";
			logger.error(message);
			throw new AssetManagerRuntimeException(message);
		} else {
			return timeDependentConstantValues.get(0);
		}
	}

}
