/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.business;


import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.User;
import org.pau.assetmanager.entities.UserClientAssociation;

import com.google.common.base.Optional;

/**
 * 
 * @author Pau Carré Cardona
 *
 */

public class UserClientAssociationBusiness {
		  
	/**
	 * This method stores a new UserClientAssociation into the database
	 * 
	 * @param userClientAssociation the UserClientAssociation to store
	 * 
	 * @return the stored UserClientAssociation 
	 */
	public static UserClientAssociation createUserClientAssociation (UserClientAssociation userClientAssociation) {
		DaoFunction.persistFunction().apply(userClientAssociation);
		return userClientAssociation;
	}

	/**
	 * @param client of the relationship
	 * @param user of the relationship
	 * @return the relationships between 'client' and 'user'
	 */
	public static UserClientAssociation getUserClientAssociationFromClientAndUser(
			Client client, User user) {
		String query = "select userClientAssociation from UserClientAssociation userClientAssociation where userClientAssociation.user = :user and userClientAssociation.client = :client";
		Optional<UserClientAssociation> userClientAssociationOptional  = DaoFunction.<UserClientAssociation> querySimpleUniqueFunction("client", client, "user", user).apply(query);
		return userClientAssociationOptional.get();
	}

	/**
	 * @param userClientAssociation the relationship (many-to-many) between client and user to delete
	 */
	public static void delete(UserClientAssociation userClientAssociation) {
		DaoFunction.deleteDetachedFunction().apply(userClientAssociation);
	}
}
