/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.pau.assetmanager.business;

import java.util.List;

import org.pau.assetmanager.entities.Group;
import org.pau.assetmanager.entities.User;

import com.google.common.base.Optional;
/**
 * This class contains database methods related to User {@link org.pau.assetmanager.entities.User}
 * 
 * @author Pau Carré Cardona
 */

public class UsersBusiness {
	
	private static final String DEFAULT_GROUP = "USER";

	/**
	 * @param username to be have into account
	 * 
	 * @return the optional user that has the parameter's username
	 */
	public static Optional<User> getUserByUsername(String username){
		String queryString = "select user from User user where user.username = :username";
		Optional<User> optionalUser = DaoFunction.<User>querySimpleUniqueFunction("username", username).apply(queryString);
		return optionalUser;
	}
	
	/**
	 * @return all the users in the system
	 */
	public static List<User> getUsers(){
		String queryString = "select user from User user";
		List<User> users = DaoFunction.<User>querySimpleListFunction().apply(queryString);
		return users;		
	}
	
	/**
	 * @param username of the user to create and persist
	 * @param password of the user to create and persist
	 * @return the user created and persisted with username 'username' and password 'password'
	 */
	public static User persistUserByUsernameAndPassword(String username, String password){
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		persist(user);
		return user;
	}
	
	/**
	 * deletes the user 'user' form the database
	 * @param user the user to delete from the database
	 */
	public static void deleteUser(User user){
		DaoFunction.deleteDetachedFunction().apply(user);
	}

	/**
	 * @param user to persist
	 * @return the persisted user (usually, with an autoincrement)
	 */
	public static User persist(User user) {
		DaoFunction.persistFunction().apply(user);
		Group group = new Group();
		group.setGroupCode(DEFAULT_GROUP);
		group.setUsername(user.getUsername());
		DaoFunction.persistFunction().apply(group);		
		return user;
	}

	/**
	 * @param user to update
	 * @return the user updated
	 */
	public static User update(User user) {
		user = DaoFunction.<User>mergeFunction().apply(user);
		return user;
	}

	/**
	 * @param user form which the groups are retrieved
	 * @return the (security) Groups from the user 'user'
	 */
	public static List<Group> getGroupsFromUser(User user) {
		String queryString = "select group from Group group where group.username = :username";
		List<Group> groups = DaoFunction.<Group>querySimpleListFunction("username", user.getUsername()).apply(queryString);
		return groups;
	}
	
}
