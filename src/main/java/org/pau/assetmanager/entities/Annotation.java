/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.zkoss.bind.annotation.NotifyChange;

/**
 * This class represents an annotation (both incomes and expenses) in a book 
 * This class contains the key information in the application as it is the basic component 
 * in a transaction for all the operations, including transfers and stock market operations.
 * 
 * @author Pau Carré Cardona
 */

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Annotation implements EntityInterface, Serializable {
	private static final long serialVersionUID = 1L;

	public enum AnnotationType {
		INCOME, EXPENSES;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/**
	 * This field contains the positive amount of money involved in the transaction
	 * In case of "expenses" or any other transaction that may imply a negative amount,
	 * this field will still be positive. The subclass of the transaction together with the
	 * 'type method' will identify if the annotation should have a "negative" or "positive" role 
	 * in the application.
	 * 
	 * This field actually represents the amount multiplied by 100, which implies that is able
	 * to store two decimal points. The usage of integer values for economical transactions is
	 * enforced to prevent numerical rounding problems.
	 * The method getAmount will return the amount divided by 100 as a double value which is the
	 * actual encoding of the amount which can have numerical rounding problems.
	 */
	@Column(name="amount")
	private Long twoDecimalLongCodifiedAmount;

	/**
	 * This field identifies the date in which the transaction of the annotation was issued
	 */
	@Temporal(TemporalType.DATE)
	protected Date date;

	/**
	 * @return the type of annotation which can be either 'INCOME' or 'EXPENSES'
	 */
	@Transient
	public abstract AnnotationType getType();

	/**
	 * This field identifies the concept involved in the annotation which is the textual
	 * short description of the annotation
	 */
	private String concept;

	/**
	 * This field is the reference to the book that contains the annotation.
	 * An annotation can only be contained in a single book. 
	 */
	@ManyToOne
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "book")
	private Book book;

	public Annotation() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the amount of money involved in the transaction as a 
	 * long integer multiplied by 100
	 */
	public Long getTwoDecimalLongCodifiedAmount() {
		return this.twoDecimalLongCodifiedAmount;
	}

	@NotifyChange(".")
	public void setTwoDecimalLongCodifiedAmount(Long amount) {
		if (amount == null) {
			amount = 0L;
		}
		if (amount < 0L) {
			amount *= -1L;
		}
		this.twoDecimalLongCodifiedAmount = amount;
	}

	/**
	 * @return the amount involved in the transaction with two 
	 * decimal points of resolution
	 */
	@Transient
	public Double getAmount() {
		return new Double(this.twoDecimalLongCodifiedAmount) / 100.0;
	}

	public abstract Double getSignedAmount();

	
	@NotifyChange(".")
	@Transient
	public void setAmount(Double amount) {
		if (amount == null) {
			amount = 0.0;
		}
		if (amount < 0.0) {
			amount *= -1.0;
		}
		this.twoDecimalLongCodifiedAmount = Math.round(amount * 100.0);
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getConcept() {
		return this.concept;
	}

	/**
	 * WATCH OUT!: do not notify changes on the concept as this will make the UI unstable
	 * @param concept the concept to be stored in for the transaction 
	 */
	public void setConcept(String concept) {
		if (concept != null) {
			this.concept = concept;
		}
	}

	public Book getBook() {
		return this.book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((twoDecimalLongCodifiedAmount == null) ? 0 : twoDecimalLongCodifiedAmount.hashCode());
		result = prime * result + ((concept == null) ? 0 : concept.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Annotation other = (Annotation) obj;
		if (twoDecimalLongCodifiedAmount == null) {
			if (other.twoDecimalLongCodifiedAmount != null)
				return false;
		} else if (!twoDecimalLongCodifiedAmount.equals(other.twoDecimalLongCodifiedAmount))
			return false;
		if (concept == null) {
			if (other.concept != null)
				return false;
		} else if (!concept.equals(other.concept))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Annotation [id=" + id + ", amount=" + twoDecimalLongCodifiedAmount + ", date=" + date
				+ ", concept=" + concept + ", book=" + book + "]";
	}

}