/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.zkoss.bind.annotation.NotifyChange;


/**
 * This class represents a client in the system which can own books. 
 * A client is *not* a user of the system, but a container of books.
 * A user can manage one or more clients in the system.
 * 
 * @author Pau Carré Cardona
 *
 */

@Entity
public class Client implements EntityInterface, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	/**
	 * Additional information one may add for the client
	 */
	private String additionalInformation;

	/**
	 * Name of the client
	 */
	private String name;

	/**
	 * Books owned by the client
	 */
	@OneToMany(mappedBy="client")
	private Set<Book> books;

	/**
	 * Users that manage the accounting of the client
	 */
	@OneToMany(mappedBy="client")
	private Set<UserClientAssociation> userClientAssociations;

    public Client() {
    }

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAdditionalInformation() {
		return this.additionalInformation;
	}

	@NotifyChange(".")
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public String getName() {
		return this.name;
	}

	@NotifyChange(".")
	public void setName(String name) {
		this.name = name;
	}

	public Set<Book> getBooks() {
		return this.books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}
	
	public Set<UserClientAssociation> getUserClientAssociations() {
		return this.userClientAssociations;
	}

	public void setUserClientAssociations(Set<UserClientAssociation> userClientAssociations) {
		this.userClientAssociations = userClientAssociations;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}