/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.entities;

import java.io.Serializable;

import javax.persistence.Entity;

/**
 * A General book is a book that contains generic income and 
 * expenses annotations. This book should not be used for 
 * housing rentals and stock market related annotations as they
 * have their own book subclass
 * 
 * @author Pau Carré Cardona
 *
 */

@Entity
public class GeneralBook extends Book implements EntityInterface, Serializable {
	private static final long serialVersionUID = 1L;

    public GeneralBook() {
    }
	
	
}