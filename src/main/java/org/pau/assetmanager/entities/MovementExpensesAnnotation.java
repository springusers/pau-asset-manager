/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * This annotations is used to represent movements between two books.
 * For each movement transaction there are two annotations involved:
 * -An expense annotation in the source book
 * -An income annotation in the destination book
 * 
 * For this reason, it is necessary to have a synchronization between
 * both annotations so their information is consistent. This implies
 * that both annotations are referenced so they "know each other". 
 * 
 * @author Pau Carré Cardona
 *
 */

@Entity
@DiscriminatorValue("MOVEMENT_EXPENSES")
public class MovementExpensesAnnotation extends ExpensesAnnotation implements
EntityInterface, Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * The income annotation linked to the current expenses annotation and
	 * form together a movement transaction. The income is linked to the 
	 * destination book (the book that receives the amount)
	 */
	@OneToOne
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "movementIncomeAnnotation")
	private MovementIncomeAnnotation movementIncomeAnnotation;

	public MovementExpensesAnnotation() {
		super();
	}

	@Override
	public void setId(Long id) {
		super.setId(id);
		if(id == null){
			movementIncomeAnnotation.setId(null);
		}
	}

	public MovementIncomeAnnotation getMovementIncomeAnnotation() {
		return movementIncomeAnnotation;
	}

	public void setMovementIncomeAnnotation(
			MovementIncomeAnnotation movementIncomeAnnotation) {
		this.movementIncomeAnnotation = movementIncomeAnnotation;
	}

	@Override
	public void setTwoDecimalLongCodifiedAmount(Long amount) {
		movementIncomeAnnotation.setTwoDecimalLongCodifiedAmount(amount);
		super.setTwoDecimalLongCodifiedAmount(amount);
	}
	
	@Override
	public void setAmount(Double amount) {
		movementIncomeAnnotation.setAmount(amount);
		super.setAmount(amount);
	}

	@Override
	public void setDate(Date date) {
		movementIncomeAnnotation.setDate(date);
		super.setDate(date);
	}

	@Override
	public void setConcept(String concept) {
		movementIncomeAnnotation.setConcept(concept);
		super.setConcept(concept);
	}

}