/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

/**
 * This annotation represents the income annotation in the
 * destination book of a movement between two books.
 * 
 * The income annotation takes a passive role in the transaction 
 * and it is the MovementExpensesAnnotation the one that is 
 * used to actually create the annotation and maintain the synchronization
 * between both of them. 
 * 
 * WATCH OUT: As this synchronization architecture the best, 
 * it is very likely to change in a future to have a more elegant way
 * to preserve the data consistency maintaining the two annotations
 * which are necessary to have the same generic treatment for annotations.
 * 
 * @author Pau Carré Cardona
 *
 */

@Entity
@DiscriminatorValue("MOVEMENT_INCOME")
public class MovementIncomeAnnotation extends IncomeAnnotation implements
EntityInterface, Serializable {
	private static final long serialVersionUID = 1L;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "movementIncomeAnnotation", cascade = CascadeType.ALL)
	private MovementExpensesAnnotation movementExpensesAnnotations;

	public MovementIncomeAnnotation() {
		super();
	}

	public MovementExpensesAnnotation getMovementExpensesAnnotations() {
		return movementExpensesAnnotations;
	}

	public void setMovementExpensesAnnotations(
			MovementExpensesAnnotation movementExpensesAnnotations) {
		this.movementExpensesAnnotations = movementExpensesAnnotations;
	}

}