/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.zkoss.bind.annotation.NotifyChange;

/**
 * The concepts form the annoatations are simple strings.
 * But, there is a set of 'Predefined Concepts' in the system 
 * which are used as a default or recommended values for a 
 * variety of books and annotation type.
 * 
 * @author Pau Carré Cardona
 *
 */

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class PredefinedConcept implements EntityInterface, Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/**
	 * The name of the default/recommended concept 
	 */
	private String name;

	/**
	 * The subclass of annotation that this predefined concept
	 * is supposed to apply to
	 */
	@Column(name = "annotation_class_name")
	private String annotationClassName;
	
	/**
	 * Indicates whether this concept is the default one 
	 * (there can only be a single default predefined concept
	 * for each annotation subclass)
	 */
	@Column(name = "is_default", nullable = false, columnDefinition = "TINYINT(1)")
	private Boolean isDefault;
	
	
	public PredefinedConcept() {
	}
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	@NotifyChange(".")
	public void setName(String name) {
		this.name = name;
	}

	public String getAnnotationClassName() {
		return annotationClassName;
	}

	@NotifyChange(".")
	public void setAnnotationClassName(String annotationClassName) {
		this.annotationClassName = annotationClassName;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	@NotifyChange(".")
	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

}