/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pau.assetmanager.viewmodel.type.PropertyType;
import org.zkoss.bind.annotation.NotifyChange;

/**
 * This class represents a household property that is
 * used by a property book. In this system it is possible
 * to have may books for each property (for example 'furniture',
 * 'rentals', 'reparations', 'cleaning'...) in case the user 
 * needs to classify them.
 * 
 * It is possible that this classification mechanism is extended/generalized later
 * on to join all the annotations in the same book and adding some 
 * sort of classification mechanism which at the current time it
 * is not designed nor implemented. This could also be used for other books.
 * But, for now, this is unfortunately the only way to classify annotations.
 * 
 * @author Pau Carré Cardona
 *
 */

@Entity
public class Property implements EntityInterface, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/**
	 * Name of the property (for example, the street address)
	 */
	private String name;

	/**
	 * The client that owns the property 
	 */
	@ManyToOne
	@JoinColumn(name = "client")
	private Client client;

	/**
	 * The type of property: TENMENT or PREMISE
	 * WATCH OUT: this field is not only informative/decorative as the type 
	 * makes huge differences in both the user interface interaction 
	 * and the Spanish fiscal system computations.
	 */
	@Enumerated(EnumType.STRING)
	private PropertyType type;

	/**
	 * The official value is the value that the Government 
	 * states the property has.
	 * This field is important for the Spanish fiscal system.
	 */
	@Column(name = "official_value")
	private Long officialValue;

	/**
	 * The actual value is the value that the owner thinks the property
	 * has at the moment in the market.
	 * Currently this value has only informative value
	 */
	@Column(name = "actual_value")
	private Long actualValue;

	public Property() {
		officialValue = 0L;
		actualValue = 0L;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public PropertyType getType() {
		return this.type;
	}

	@NotifyChange(".")
	public void setType(PropertyType type) {
		this.type = type;
	}

	public Double getOfficialValue() {
		if (this.officialValue == null) {
			return 0.0;
		} else {
			return new Double(this.officialValue) / 100.0;
		}
	}

	@NotifyChange(".")
	public void setOfficialValue(Double officialValue) {
		if (officialValue == null) {
			this.officialValue = 0L;
		} else {
			if (officialValue < 0.0) {
				officialValue *= -1.0;
			}
			this.officialValue = Math.round(officialValue * 100L);
		}
	}

	public Double getActualValue() {
		if (this.actualValue == null) {
			return 0.0;
		} else {
			return new Double(this.actualValue) / 100.0;
		}
	}

	@NotifyChange(".")
	public void setActualValue(Double actualValue) {
		if (actualValue == null) {
			this.actualValue = 0L;
		} else {
			if (actualValue < 0.0) {
				actualValue *= -1.0;
			}
			this.actualValue = Math.round(actualValue * 100L);
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Property other = (Property) obj;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}