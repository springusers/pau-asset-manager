/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.zkoss.bind.annotation.NotifyChange;



/**
 * This class represents stock income for a stocks book.
 * The income annotations in the context of stocks is 
 * a transaction based on selling an amount of stocks 
 * at a given price (the system has only into account
 * the number of stocks and the total cost as amount, 
 * the price per stock is deduced as 'cost of transaction' /'amount of stocks')
 * 
 * 
 * In the context of stocks the annotation represents the
 * textual identifier of the stock. As there are validations
 * and report analysis between bought and sold stocks it is 
 * important to preserve the same concept name for all the 
 * transaction that involve the same value in the stock m
 * market.
 * 
 * 
 * @author Pau Carré Cardona
 *
 */

@Entity
@DiscriminatorValue("STOCK_INCOME")
public class StockIncomeAnnotation extends IncomeAnnotation implements EntityInterface,  Serializable {
	private static final long serialVersionUID = 1L;

	public static Long DEFAULT_NUMBER_OF_STOCKS = 1L;
	
	/**
	 * 
	 */
	@Column(name="number_of_stocks")
	private Long numberOfStocks;

	public StockIncomeAnnotation() {
		super();
		numberOfStocks = DEFAULT_NUMBER_OF_STOCKS;
	}

	public Long getNumberOfStocks() {
		return this.numberOfStocks;
	}

	@NotifyChange(".")
	public void setNumberOfStocks(Long numberOfStocks) {
		this.numberOfStocks = numberOfStocks;
	}

}