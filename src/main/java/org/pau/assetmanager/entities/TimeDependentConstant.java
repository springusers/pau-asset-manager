/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

import org.pau.assetmanager.viewmodel.type.TimeDependentConstantType;

/**
 * In the system, for both, default values and the computation
 * fiscal reports it is necessary to have into account some constants
 * (most of them, percentages). 
 * 
 * As a lot of those constants change along the time 
 * (i.e. the Government decides to change percentages for the 
 * computation of the taxes) it is necessary to have a record
 * of the different constants and apply the ones that are closer
 * in time for default values and to compute the fiscal payments. 
 * 
 * @author Pau Carré Cardona
 *
 */

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class TimeDependentConstant implements EntityInterface, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/**
	 * This enum represents the conceptual constant identifier
	 * fot the constant
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="code")
	private TimeDependentConstantType timeDependentConstantType;

	/**
	 * Textual easy-to-read description of the constant
	 */
	private String name;

	/**
	 * the time-dependent values for the given constant.
	 * As stated above, each constant can have a different 
	 * value depending on the year it applies.
	 */
	@OneToMany(mappedBy = "timeDependentConstant")
	private Set<TimeDependentConstantValue> timeDependentConstantValues;

	public TimeDependentConstant() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TimeDependentConstantType getTimeDependentConstantType() {
		return timeDependentConstantType;
	}

	public void setTimeDependentConstantType(
			TimeDependentConstantType timeDependentConstantType) {
		this.timeDependentConstantType = timeDependentConstantType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<TimeDependentConstantValue> getTimeDependentConstantValues() {
		return timeDependentConstantValues;
	}

	public void setTimeDependentConstantValues(
			Set<TimeDependentConstantValue> timeDependentConstantValues) {
		this.timeDependentConstantValues = timeDependentConstantValues;
	}

}