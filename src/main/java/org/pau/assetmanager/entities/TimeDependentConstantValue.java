/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * This class represents a value that
 * a 'TimeDependentConstant' has for a given year
 * 
 * @author Pau Carré Cardona
 *
 */

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class TimeDependentConstantValue implements EntityInterface, Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/**
	 * Year the value applies
	 */
	private Integer year;

	/**
	 * Numerical value of the constant (for percentages it is multiplied by 100)
	 */
	@Column(name = "numerical_value")
	private Long numericalValue;
	
	/**
	 * The time dependent constant the value is applied to
	 */
	@ManyToOne
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "time_dependent_constant")
	private TimeDependentConstant timeDependentConstant;
	
	public TimeDependentConstantValue() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@Transient
	public Double getNumericalValueAsDouble() {
		return numericalValue / 100.0;
	}

	
	public Long getNumericalValue() {
		return numericalValue;
	}

	public void setNumericalValue(Long numericalValue) {
		this.numericalValue = numericalValue;
	}

	public TimeDependentConstant getTimeDependentConstant() {
		return timeDependentConstant;
	}

	public void setTimeDependentConstant(TimeDependentConstant timeDependentConstant) {
		this.timeDependentConstant = timeDependentConstant;
	}

	

}