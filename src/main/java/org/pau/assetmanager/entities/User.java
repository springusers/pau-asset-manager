/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;


/**
 * This class represents the database entity of a 
 * user of the application. 
 * A user of the application is *not* a client of the 
 * application. A user is someone who can login in the 
 * application and can manage the books for a set 
 * of *clients* which are a separate entity.
 * 
 * @author Pau Carré Cardona
 *
 */

@Entity
public class User implements EntityInterface, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	/**
	 * The clean password of the user.
	 * WATCH OUT: the password is expected to be hashed
	 * in a future
	 */
	private String password;

	/**
	 * Username of the user
	 */
	private String username;

	/**
	 * The user-to-clients relations the user is involved in. 
	 * The association describes the clients' books the user can user can manage .
	 * The explicit entity association is prefered in the system as the main
	 * developer does not like at all the many-to-many relation JPA abstraction ;)
	 */
	@OneToMany(mappedBy="user")
	private Set<UserClientAssociation> userClientAssociations;

    public User() {
    }

    
    @Transient
    public Boolean isAdministrator(){
    	return true;
    }
    
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Set<UserClientAssociation> getUserClientAssociations() {
		return this.userClientAssociations;
	}

	public void setUserClientAssociations(Set<UserClientAssociation> userClientAssociations) {
		this.userClientAssociations = userClientAssociations;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	
}