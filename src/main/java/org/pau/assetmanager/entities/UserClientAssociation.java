/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


/**
 * This entity is represents the many-to-many association 
 * between user and client.
 * As a reminder, the *user* is *not* a *client*.  A user is
 * a user of the application that manages the books from one 
 * or more clients.
 * 
 * In the context of this applications the many-to-many relations
 * are implemented using an intermediate entity instead of using
 * JPA's many-to-many abstraction as the developer does not like 
 * that abstraction ;)  
 * 
 * @author Pau Carré Cardona
 *
 */

@Entity
public class UserClientAssociation implements EntityInterface,  Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	/**
     * The user side of the relation
     */
    @ManyToOne
	@JoinColumn(name="user")
	private User user;

    /**
     * The client side of the relation
     */
    @ManyToOne
	@JoinColumn(name="client")
	private Client client;

    public UserClientAssociation() {
    }

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
}