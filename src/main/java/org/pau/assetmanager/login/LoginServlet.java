/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class is the POST login servlet that takes the credentials
 * following the JEE's form authentication specifications.
 * 
 * WATCH OUT: currently the transmission of the password
 * is not encrypted nor hashed but this is planned to do.
 * 
 * @author Pau Carré Cardona
 *
 */

@WebServlet(name="loginServlet", urlPatterns={"/Login"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static Logger logger =  LogManager.getLogger(LoginServlet.class);

	private static final String USERNAME_PARAMETER = "j_username";
	private static final String PASSWORD_PARAMETER = "j_password";
	
	public static final String MAIN_PAGE = "index.zul";
	
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			request.login(request.getParameter(USERNAME_PARAMETER),
					request.getParameter(PASSWORD_PARAMETER));
			response.sendRedirect(MAIN_PAGE);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.sendError(401);
		}
	}

}
