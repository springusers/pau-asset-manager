/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.utils;

/**
 * @author Pau Carré Cardona
 */

/**
 * This class is the generic runtime exception used in the application.
 * Runtime exceptions are prefered over checked exceptions as we enforce
 * as much as possible that the results of the methods should be correct
 * and in case they are not, the application should raise an error.
 *
 */
public class AssetManagerRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	protected AssetManagerRuntimeException() {
		super();
	}

	public AssetManagerRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public AssetManagerRuntimeException(String message) {
		super(message);
	}		

}
