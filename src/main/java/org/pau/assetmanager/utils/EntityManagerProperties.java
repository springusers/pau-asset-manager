/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This utility class generated the properties map from a properties file
 * to set up the Hibernate configuration.
 * 
 * @author Pau Carré Cardona
 *
 */
public class EntityManagerProperties {

	private static Logger logger = LogManager
			.getLogger(EntityManagerProperties.class);

	private static final String ASSETMANAGER_HOME = "ASSETMANAGER_HOME";
	private static final String HIBERNATE_PROPERTIES = "hibernate.properties";

	private static Map<String, String> propertiesMap;

	/**
	 * Sets ups the defaults properties for the application. 
	 * The application assumes that there exists a valid hibernate properties file in
	 * the following path '$ASSETMANAGER_HOME/hibernate.properties'
	 * 
	 * The properties could *not* be the default ones in case of testing in which the
	 * testing properties are used
	 */
	private static void setUpDefaultPropertiesMap() {
		try {
			Map<String, String> environment = System.getenv();
			String assetManagerHomePath = environment.get(ASSETMANAGER_HOME);
			if (assetManagerHomePath == null
					|| assetManagerHomePath.trim().equals("")) {
				throw new AssetManagerRuntimeException(
						"The environment variable ASSETMANAGER_HOME is not properly set up.");
			}
			Properties properties = new Properties();
			File propertiesFile = new File(assetManagerHomePath
					+ File.separator + HIBERNATE_PROPERTIES);
			if (!propertiesFile.exists()) {
				String message = "It is not possible to find the hibernate.properties file in the ASSETMANAGER_HOME folder";
				logger.error(message);
				throw new AssetManagerRuntimeException(message);
			}
			InputStream hibernatePropertiesInputStream = new FileInputStream(
					propertiesFile);
			properties.load(hibernatePropertiesInputStream);
			setPropertiesMapFromProperties(properties);
		} catch (Throwable t) {
			String message = "It was not possible to create a entity manager.";
			logger.error(message, t);
			throw new AssetManagerRuntimeException(message, t);
		}
	}

	private static void setPropertiesMapFromProperties(Properties properties){
		propertiesMap = new HashMap<String, String>();
		for (Entry<Object, Object> currentEntry : properties.entrySet()) {
			propertiesMap.put((String) currentEntry.getKey(),
					(String) currentEntry.getValue());
		}
	}
	
	/**
	 * This method is used to set up a custom hibernate properties.
	 * This method is called by the testing infrastructure
	 * 
	 * @param customProperties custom hibernate properties 
	 */
	public static void setUpCustomProperties(Properties customProperties){
		setPropertiesMapFromProperties(customProperties);
	}
	
	/**
	 * @return the properites maps which should be the default ones if the method 'setUpCustomProperties'
	 * has not been called
	 */
	public static Map<String, String> getPropertiesMap() {
		if (propertiesMap == null) {
			setUpDefaultPropertiesMap();
		}
		return propertiesMap;
	}

}
