/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.utils;

import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This is a factory class that generates Hibernate entity managers to be used for database operations
 * The entity manager factory assumes that the entity manager name is 'assetmanager'.
 * 
 * @author Pau Carré Cardona
 */

public class PropertiesEntityManagerFactory {

	private static EntityManagerFactory emFactory = null;
	
	private static Logger logger = LogManager
			.getLogger(PropertiesEntityManagerFactory.class);
	
	private static final String ENTITY_MANAGER_NAME = "assetmanager";

	/**
	 * This is the only and main factory method that should be used
	 * in order to retrieve the entity manager.
	 * @return the EntityManager to be used for database operations
	 */
	public static synchronized EntityManager getEntityManager() {
		try {
			if (emFactory == null) {
				Map<String, String> propertiesMap = EntityManagerProperties.getPropertiesMap();
				emFactory = Persistence
						.createEntityManagerFactory(ENTITY_MANAGER_NAME, propertiesMap);
			}
			EntityManager entityManager = emFactory.createEntityManager();
			return entityManager;
		} catch (Throwable t) {
			String message = "It was not possible to create an entity manager.";
			logger.error(
					message,
					t);
			throw new AssetManagerRuntimeException(message, t);
		}
	}

}
