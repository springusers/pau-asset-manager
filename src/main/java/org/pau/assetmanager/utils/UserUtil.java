/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.pau.assetmanager.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pau.assetmanager.business.UsersBusiness;
import org.pau.assetmanager.entities.User;
import org.zkoss.zk.ui.Executions;

import com.google.common.base.Optional;

/**
 * This is a utility class that returns the current user that
 * is logged in in the application. 
 * 
 * @author Pau Carré Cardona
 *
 */

public class UserUtil {

	private static final Logger logger = LogManager.getLogger(UserUtil.class);
	
	/**
	 * @return the current user that is logged in in the application
	 * WATCH OUT: To call the method the thread should be running in a servlet container in a ZK environment.
	 */
	public static User getExecutionUser() {
		String username = "asesor02";// Executions.getCurrent().getUserPrincipal().getName();
		Optional<User> optionalUser = UsersBusiness.getUserByUsername(username);
		if (optionalUser.isPresent()) {
			return optionalUser.get();
		} else {
			String message = "The execution user with name '"
					+ username + "' does not exist in the database";
			logger.error(message);
			throw new AssetManagerRuntimeException(message);
		}
	}
}
