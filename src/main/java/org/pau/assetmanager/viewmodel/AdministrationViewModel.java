/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel;

import java.util.LinkedList;
import java.util.List;

import org.pau.assetmanager.business.ClientsBusiness;
import org.pau.assetmanager.business.UserClientAssociationBusiness;
import org.pau.assetmanager.business.UsersBusiness;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.Group;
import org.pau.assetmanager.entities.User;
import org.pau.assetmanager.entities.UserClientAssociation;
import org.pau.assetmanager.utils.UserUtil;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;

/**
 * This class is the ViewModel for the administration of the application
 * defined by the ZUL 'administration.zul'
 * 
 * @author Pau Carré Cardona
 * 
 */

public class AdministrationViewModel {

	private static final Integer USER_FIEL_MINIMUM_LENGTH = 4;
	private static final String ADMINISTRATOR = "ADMINISTRATOR";
	
	// user selected to manage
	private User selectedUser;
	// user that is logged
	private User executionUser;
	// existing users in the system
	private List<User> users;
	// clients that are linked with 'selectedUser' user
	private List<Client> clientsFromSelectedUser;

	// first password introduced to change the password
	private String newPasswordFirst;
	// second password introduced to change the password
	private String newPasswordSecond;

	// indicates whether the error message for a failed attempt to
	// change the password should be shown
	private Boolean errorUpdatingPassword;
	// indicates whether the confirmation message for a successful attempt to
	// change the password should be shown
	private Boolean isPasswordUpdated;
	// indicates whether the dialog to add a new user
	// should be displayed or not
	private Boolean addUserActive;

	// Fields for user creation
	//
	// username introduced to create a new user	
	private String createUserUsername;
	// first password introduced to create a new user
	private String createUserPasswordFirst;
	// second password introduced to create a new user
	private String createUserPasswordSecond;
	// indicates whether the error message for a failed attempt to
	// create a new user should be shown
	private Boolean errorCreateUser;
	
	// Fields for client association
	//
	/* indicates whether the window to add a new relationship 
	/* between the selected user and another client should 
	/* be open
	*/
	private Boolean relationshipClientActive;
	/* 
	/* list of clients that can be added in the 
	/* relationship between the selected user
	/* and a client
	*/
	private List<Client> relationshipClients;
	/* the selected client to be added in the 
	/* relationship with the selected user 
	*/
	private Client relationshipSelectedClient;
	
	@Init
	public void init() {
		setCreateUserWindowToDefaults();
		setRelationshipClientWindowToDefaults();
		errorUpdatingPassword = false;
		isPasswordUpdated = false;
		executionUser = UserUtil.getExecutionUser();
		users = UsersBusiness.getUsers();
		if (users.size() > 0) {
			selectedUser = executionUser;
			clientsFromSelectedUser = ClientsBusiness
					.getClientsFromUser(selectedUser);
		} else {
			// this should not happen
			selectedUser = null;
			clientsFromSelectedUser = new LinkedList<Client>();
		}
	}

	
	/**
	 * @return whether the executing user is administrator
	 * (this method is called to display or hide administration
	 * functionalities)
	 */
	public Boolean getExecutionUserIsAdministrator(){
		List<Group> groups = UsersBusiness.getGroupsFromUser(executionUser);
		List<String> groupCodes = Lists.transform(groups, new Function<Group, String>() {
			@Override
			public String apply(Group input) {
				return input.getGroupCode();
			}
			
		});
		return groupCodes.contains(ADMINISTRATOR);
	}
	
	@NotifyChange({ "errorUpdatingPassword", "isPasswordUpdated" })
	@Command
	public void changePasswordToExecutionUser() {
		if (Objects.equal(newPasswordFirst, newPasswordSecond)) {
			// check password complexity
			if (checkUserFieldComplexity(newPasswordFirst)) {
				String trimmedPassword = newPasswordFirst.trim();
				executionUser.setPassword(trimmedPassword);
				executionUser = UsersBusiness.update(executionUser);
				errorUpdatingPassword = false;
			} else {
				errorUpdatingPassword = true;
			}
		} else {
			errorUpdatingPassword = true;
		}
		isPasswordUpdated = !errorUpdatingPassword;
	}

	@NotifyChange({"clientsFromSelectedUser", "relationshipClients"})
	@Command
	public void removeClientRelationship(@BindingParam("client") Client client){
		UserClientAssociation userClientAssociation = UserClientAssociationBusiness.getUserClientAssociationFromClientAndUser(client, selectedUser);
		UserClientAssociationBusiness.delete(userClientAssociation);
	}
	
	@NotifyChange("addUserActive")
	@Command
	public void openAddUserWindow() {
		addUserActive = true;
	}
	
	@NotifyChange("relationshipClientActive")
	@Command
	public void openRelationshipClientWindow(){
		relationshipClientActive = true;
	}

	@NotifyChange({ "addUserActive", "createUserUsername",
			"createUserPasswordFirst", "createUserPasswordSecond" })
	@Command
	public void cancelAddUser() {
		setCreateUserWindowToDefaults();
	}
	
	@NotifyChange({ "relationshipClientActive", "relationshipClients",
		"relationshipSelectedClient" })
	@Command
	public void cancelRelationshipUser() {
		setRelationshipClientWindowToDefaults();
	}
	
	@NotifyChange({ "relationshipClientActive", "relationshipClients",
	"relationshipSelectedClient", "clientsFromSelectedUser" })
	@Command
	public void relateClient(){
		UserClientAssociation userClientAssociation = new UserClientAssociation();
		userClientAssociation.setClient(relationshipSelectedClient);
		userClientAssociation.setUser(selectedUser);
		clientsFromSelectedUser = ClientsBusiness.getClientsFromUser(selectedUser);
		UserClientAssociationBusiness.createUserClientAssociation(userClientAssociation);
		setRelationshipClientWindowToDefaults();
	}
	
	private void setRelationshipClientWindowToDefaults() {
		relationshipClientActive = false;
	}


	@NotifyChange({
		"selectedUser", "users" , "selectedUser"})
	@Command
	public void removeUser() {
		UsersBusiness.deleteUser(selectedUser);
		users = UsersBusiness.getUsers();
		if(users.size() > 0){
			selectedUser = users.iterator().next();
		}
	}

	@NotifyChange({ "addUserActive", "createUserUsername",
			"createUserPasswordFirst", "createUserPasswordSecond",
			"errorCreateUser", "selectedUser", "users" })
	@Command
	public void addUser() {
		if (Objects.equal(createUserPasswordFirst, createUserPasswordSecond)) {
			if (checkUserFieldComplexity(createUserUsername)
					&& checkUserFieldComplexity(createUserPasswordFirst)) {
				User user = new User();
				user.setUsername(createUserUsername);
				user.setPassword(createUserPasswordFirst);
				UsersBusiness.persist(user);
				setCreateUserWindowToDefaults();
				users = UsersBusiness.getUsers();
				selectedUser = user;
			} else {
				errorCreateUser = true;
			}
		} else {
			errorCreateUser = true;
		}
	}

	private void setCreateUserWindowToDefaults() {
		createUserUsername = "";
		createUserPasswordFirst = "";
		createUserPasswordSecond = "";
		addUserActive = false;
		errorCreateUser = false;
	}
	
	
	private static Boolean checkUserFieldComplexity(String userField) {
		if (userField != null) {
			userField = userField.trim();
			if (userField.length() >= USER_FIEL_MINIMUM_LENGTH) {
				return true;
			}
		}
		return false;
	}

	
	public Boolean getRelationshipClientActive() {
		return relationshipClientActive;
	}

	public void setRelationshipClientActive(Boolean relationshipClientActive) {
		this.relationshipClientActive = relationshipClientActive;
	}

	@DependsOn("selectedUser")
	public List<Client> getRelationshipClients() {
		relationshipClients = ClientsBusiness.getClientsNotLinkedWithUser(selectedUser);
		return relationshipClients;
	}

	public void setRelationshipClients(List<Client> relationshipClients) {
		this.relationshipClients = relationshipClients;
	}

	@DependsOn("relationshipClients")
	public Client getRelationshipSelectedClient() {
		if(relationshipClients != null && relationshipClients.size() > 0){
			if(!relationshipClients.contains(relationshipSelectedClient)){
				relationshipSelectedClient = relationshipClients.iterator().next();
			}
		}else{
			relationshipSelectedClient = null;
		}
		return relationshipSelectedClient;
	}

	public void setRelationshipSelectedClient(Client relationshipSelectedClient) {
		this.relationshipSelectedClient = relationshipSelectedClient;
	}
	public String getNewPasswordFirst() {
		return newPasswordFirst;
	}

	public void setNewPasswordFirst(String newPasswordFirst) {
		this.newPasswordFirst = newPasswordFirst;
	}

	public String getNewPasswordSecond() {
		return newPasswordSecond;
	}

	public void setNewPasswordSecond(String newPasswordSecond) {
		this.newPasswordSecond = newPasswordSecond;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User getExecutionUser() {
		return executionUser;
	}

	public void setExecutionUser(User executionUser) {
		this.executionUser = executionUser;
	}

	public Boolean getErrorCreateUser() {
		return errorCreateUser;
	}

	public void setErrorCreateUser(Boolean errorCreateUser) {
		this.errorCreateUser = errorCreateUser;
	}

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public Boolean getErrorUpdatingPassword() {
		return errorUpdatingPassword;
	}

	public void setErrorUpdatingPassword(Boolean errorUpdatingPassword) {
		this.errorUpdatingPassword = errorUpdatingPassword;
	}

	public Boolean getIsPasswordUpdated() {
		return isPasswordUpdated;
	}

	public void setIsPasswordUpdated(Boolean isPasswordUpdated) {
		this.isPasswordUpdated = isPasswordUpdated;
	}

	public Boolean getAddUserActive() {
		return addUserActive;
	}

	public void setAddUserActive(Boolean addUserActive) {
		this.addUserActive = addUserActive;
	}

	public String getCreateUserUsername() {
		return createUserUsername;
	}

	public void setCreateUserUsername(String createUserUsername) {
		this.createUserUsername = createUserUsername;
	}

	public String getCreateUserPasswordFirst() {
		return createUserPasswordFirst;
	}

	public void setCreateUserPasswordFirst(String createUserPasswordFirst) {
		this.createUserPasswordFirst = createUserPasswordFirst;
	}

	public String getCreateUserPasswordSecond() {
		return createUserPasswordSecond;
	}

	public void setCreateUserPasswordSecond(String createUserPasswordSecond) {
		this.createUserPasswordSecond = createUserPasswordSecond;
	}

	@DependsOn("selectedUser")
	public List<Client> getClientsFromSelectedUser() {
		clientsFromSelectedUser = ClientsBusiness
				.getClientsFromUser(selectedUser);
		return clientsFromSelectedUser;
	}

	public void setClientsFromSelectedUser(List<Client> clientsFromSelectedUser) {
		this.clientsFromSelectedUser = clientsFromSelectedUser;
	}

}
