/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.pau.assetmanager.business.PropertiesBusiness;
import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.GeneralBook;
import org.pau.assetmanager.entities.Property;
import org.pau.assetmanager.entities.PropertyBook;
import org.pau.assetmanager.entities.StocksBook;
import org.pau.assetmanager.viewmodel.type.BookType;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

/**
 * This class is the ViewModel for the UI interface to add new Books
 * to a client which is defined in the ZUL file 'add_book.zul'
 * 
 * @author Pau Carré Cardona
 *
 */

public class BookCreationViewModel {

	// field that holds whether the windows to add books is visible or not
	private Boolean isVisible;
	// field that holds the client to whom the book should be added
	private Client client;
	// field that holds the state of whether the properly checkbox has been selected
	// in which case  it will display the selection of the property
	private Boolean propertyCheckboxSelected;
	// the property selected which has sense only if 'propertyCheckboxSelected' is true
	private Property selectedProperty;
	// the name of the book
	private String name;
	// the selected book type which is mapped in the radio group for the selection of the book type
	private BookType selectedBookType;

	Collection<BookType> bookTypes = new ArrayList<BookType>(
			Arrays.asList(BookType.values()));

	public Collection<BookType> getBookTypes() {
		return bookTypes;
	}

	@Init
	public void init() {
		isVisible = false;
		client = null;
		propertyCheckboxSelected = false;
		selectedBookType = BookType.GENERIC_BOOK;
	}

	@NotifyChange({ "selectedBookType", "isVisible", "name",
			"propertyCheckboxSelected", "client" })
	@Command
	public void clearComponents(
			@ContextParam(ContextType.TRIGGER_EVENT) Event event) {
		event.stopPropagation();
		isVisible = false;
		client = null;
		name = "";
		propertyCheckboxSelected = false;
		selectedBookType = BookType.GENERIC_BOOK;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BookType getSelectedBookType() {
		return selectedBookType;
	}

	@NotifyChange({ "properties", "selectedProperty" })
	@Command
	public void notifyPropertyAdded(@BindingParam("property") Property property) {
		selectedProperty = property;
	}

	@NotifyChange("propertyCheckboxSelected")
	public void setSelectedBookType(BookType selectedBookType) {
		this.selectedBookType = selectedBookType;
		propertyCheckboxSelected = selectedBookType
				.equals(BookType.PROPERTY_BOOK);
	}

	@NotifyChange("client")
	@Command
	public void updateClient(@BindingParam("client") Client client) {
		this.client = client;
	}

	@NotifyChange({ "name", "isVisible" })
	@Command
	public void createBook(@ContextParam(ContextType.TRIGGER_EVENT) Event event) {
		// enforces the name to be not null and with at least three characters of length
		if (name == null || name.trim().length() < 3) {
			Messagebox.show(
					"El nombre tiene que tener al menos tres caracteres",
					"Error", Messagebox.OK, Messagebox.ERROR,
					new org.zkoss.zk.ui.event.EventListener<Event>() {
						public void onEvent(Event evt)
								throws InterruptedException {
							if (evt.getName().equals("onOK")) {

							}
						}
					});
		} else {
			Book book = null;
			// property book
			if (propertyCheckboxSelected) {
				// if the book is for properties, we check the selection
				// of a property
				if (selectedProperty == null) {
					Messagebox.show("Es necesario seleccionar una propiedad.",
							"Error", Messagebox.OK, Messagebox.ERROR,
							new org.zkoss.zk.ui.event.EventListener<Event>() {
								public void onEvent(Event evt)
										throws InterruptedException {
									if (evt.getName().equals("onOK")) {

									}
								}
							});
					return;
				}
				PropertyBook propertyBook = new PropertyBook();
				propertyBook.setName(name);
				propertyBook.setClient(client);
				propertyBook.setProperty(selectedProperty);
				book = propertyBook;
			} else if (selectedBookType.equals(BookType.STOCKS_BOOK)) {
				// stock book
				StocksBook stocksBook = new StocksBook();
				stocksBook.setName(name);
				stocksBook.setClient(client);
				book = stocksBook;
			} else if (selectedBookType.equals(BookType.GENERIC_BOOK)) {
				// generic book (general expenses and income)
				GeneralBook generalBook = new GeneralBook();
				generalBook.setName(name);
				generalBook.setClient(client);
				book = generalBook;
			}
			isVisible = false;
			name = "";
			// generate the request for the creation of the Book in the database
			Events.postEvent("onCreateBookRequest", event.getTarget(), book);
		}
	}

	//	@NotifyChange("selectedProperty")
	@DependsOn({ "client" })
	public List<Property> getProperties() {
		List<Property> properties = PropertiesBusiness
				.getPropertiesByClient(client);
		return properties;
	}

	@DependsOn({ "client" })
	public Property getSelectedProperty() {
		List<Property> properties = PropertiesBusiness
				.getPropertiesByClient(client);
		if (selectedProperty == null && properties.size() > 0) {
			selectedProperty = properties.get(0);
		}
		if (selectedProperty != null && !properties.contains(selectedProperty)) {
			if (properties.size() > 0) {
				selectedProperty = properties.get(0);
			} else {
				selectedProperty = null;
			}
		}
		return selectedProperty;
	}

	public void setSelectedProperty(Property selectedProperty) {
		this.selectedProperty = selectedProperty;
	}

	public Boolean getIsVisible() {
		return isVisible;
	}

	public void setIsVisible(Boolean isVisible) {
		this.isVisible = isVisible;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Boolean getPropertyCheckboxSelected() {
		return propertyCheckboxSelected;
	}

	public void setPropertyCheckboxSelected(Boolean propertyCheckboxSelected) {
		this.propertyCheckboxSelected = propertyCheckboxSelected;
	}

}
