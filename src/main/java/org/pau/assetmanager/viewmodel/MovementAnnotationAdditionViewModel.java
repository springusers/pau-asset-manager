/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.pau.assetmanager.business.BooksBusiness;
import org.pau.assetmanager.business.ClientsBusiness;
import org.pau.assetmanager.entities.Annotation.AnnotationType;
import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.Client;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
/**
 * This class is the ViewModel used to add Movement annotations
 * used by the ZUL 'add_movement_annotation.zul'
 * 
 * The View allows to add a single new movement from the currently
 * selected book to another book form the same or another client
 * 
 * @author Pau Carré Cardona
 *
 */

public class MovementAnnotationAdditionViewModel {

	// whether the window is visible (active) or invisible (inactive/hidden)
	private Boolean isVisible;
	// book selected from which the movements will be the source of
	private Book sourceBook;
	// selected client who will receive the transference as an income
	private Client selectedClient;
	// selected book that will receive the transference as an income
	private Book selectedBook;
	// list of available books for the selected client
	private List<Book> books;
	// list of all available clients for the user
	private List<Client> clients;

	@Init
	public void init() {
		isVisible = false;
		clients = new LinkedList<Client>();
		books = new LinkedList<Book>();
		selectedClient = null;
		selectedBook = null;
	}

	public Boolean getIsVisible() {
		return isVisible;
	}

	@DependsOn("sourceBook")
  	@NotifyChange("clients")
	public List<Client> getClients() {
		if(sourceBook != null){
			List<Client> clientsForUser = ClientsBusiness.getClients();
			clients = clientsForUser;
		}else{
			clients = new LinkedList<Client>();
		}
		return clients;
	}

	@DependsOn("clients")
  	@NotifyChange("selectedClient")
	public Client getSelectedClient() {
		if(selectedClient == null || !clients.contains(selectedClient)){
			// update selected client;
			if(clients.size() > 0){
				selectedClient = clients.get(0);
			}else{
				selectedClient = null;
			}
		}
		return selectedClient;
	}

	@DependsOn("books")
  	@NotifyChange("selectedBook")
	public Book getSelectedBook() {
		if(selectedBook == null || !books.contains(selectedBook)){
			if(books.size() > 0){
				selectedBook = books.get(0);
			}else{
				selectedBook = null;
			}
		}
		return selectedBook;
	}

	@DependsOn("selectedClient")
  	@NotifyChange("books")
	public List<Book> getBooks() {
		// check books from client
		List<Book> booksFromClient = BooksBusiness.getBooksFromClient(selectedClient);
		// remove the current book (there is no sense to 
		// make a movement from a book to the same book)
		booksFromClient.remove(sourceBook);
		books = booksFromClient;
		return books;
	}


	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public void setIsVisible(Boolean isVisible) {
		this.isVisible = isVisible;
	}

	public Book getSourceBook() {
		return sourceBook;
	}

	public void setSourceBook(Book sourceBook) {
		this.sourceBook = sourceBook;
	}
	

	public void setSelectedClient(Client selectedClient) {
		this.selectedClient = selectedClient;
	}

	public void setSelectedBook(Book selectedBook) {
		this.selectedBook = selectedBook;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

  	@NotifyChange({ "isVisible", "sourceBook" })
	@GlobalCommand
	public void createMovementAnnotationForBook(@BindingParam("sourceBook") Book sourceBook) {
		this.isVisible = true;
		this.sourceBook = sourceBook;
	}

  	@NotifyChange("isVisible")
	@Command
	public void issueMovementAnnotationDatabaseInsertionRequest() {
		this.isVisible = false;
		Map<String, Object> data =new HashMap<String, Object>();
		data.put("destinationBook", selectedBook);
		data.put("type", AnnotationType.EXPENSES);
		BindUtils.postGlobalCommand(null, null, "addMovementAnnotation",
				data);
	}

}
