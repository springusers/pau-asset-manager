/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.pau.assetmanager.business.AnnotationsBusiness;
import org.pau.assetmanager.business.ConceptsBusiness;
import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.Annotation.AnnotationType;
import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.MovementExpensesAnnotation;
import org.pau.assetmanager.entities.MovementIncomeAnnotation;
import org.pau.assetmanager.viewmodel.utils.SortingCriteria;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;

import com.google.common.base.Optional;

/**
 * This is the ViewModel, that extends AnnotationViewModel, is used
 * for the Movement annotations to add the two annotations
 * (income in the 'destination' book and expenses in the 'source' book)
 * that should be synchronized together in order to generate a 
 * consistent movement
 * 
 * 
 * @author Pau Carré Cardona
 *
 */

public class MovementAnnotationViewModel extends AnnotationViewModel {

	@Init
	public void init(@BindingParam("annotationType") String annotationType){
		super.init(annotationType);
	}

	@NotifyChange({ "annotations" })
	@GlobalCommand
	public void addMovementAnnotation(
			@BindingParam("destinationBook") Book destinationBook) {
		if (annotationType.equals(AnnotationType.EXPENSES)) {

			MovementIncomeAnnotation movementIncomeAnnotation = new MovementIncomeAnnotation();
			movementIncomeAnnotation.setDone(true);
			addDefaultValuesToAnnotation(
					movementIncomeAnnotation, destinationBook, currentYear,
					annotationsFilter);
			movementIncomeAnnotation = (MovementIncomeAnnotation) addAnnotation(movementIncomeAnnotation);

			MovementExpensesAnnotation movementExpensesAnnotation = new MovementExpensesAnnotation();
			movementExpensesAnnotation
					.setMovementIncomeAnnotation(movementIncomeAnnotation);
			addDefaultValuesToAnnotation(
					movementExpensesAnnotation, bookSelection.getSelectedBook(), currentYear,
					annotationsFilter);
			addAnnotation(movementExpensesAnnotation);
		}

	}

	@NotifyChange({ "annotations" })
	@Command
	public void deleteMovementAnnotation(
			@ContextParam(ContextType.TRIGGER_EVENT) Event event) {
		MovementExpensesAnnotation movementExpensesAnnotation = (MovementExpensesAnnotation) event
				.getData();
		AnnotationsBusiness.deleteAnnotation(movementExpensesAnnotation);
		AnnotationsBusiness.deleteAnnotation(movementExpensesAnnotation
				.getMovementIncomeAnnotation());
		BindUtils
				.postGlobalCommand(null, null, "updateReportAnnotations", null);
	}

	@NotifyChange({ "annotations" })
	@Command
	public void duplicateMovementAnnotation(
			@BindingParam("annotation") MovementExpensesAnnotation movementExpensesAnnotation,
			@ContextParam(ContextType.TRIGGER_EVENT) Event event) {
		String defaultConcept = ConceptsBusiness.getDefaultConcept(movementExpensesAnnotation.getClass().getSimpleName());
		Date currentDate = new Date();

		MovementIncomeAnnotation movementIncomeAnnotation = movementExpensesAnnotation
				.getMovementIncomeAnnotation();
		movementIncomeAnnotation.setId(null);
		movementIncomeAnnotation.setDate(currentDate);
		movementIncomeAnnotation.setConcept(defaultConcept);
		addAnnotation(movementIncomeAnnotation);

		movementExpensesAnnotation.setId(null);
		movementExpensesAnnotation.setDate(currentDate);
		movementExpensesAnnotation.setConcept(defaultConcept);
		movementExpensesAnnotation
				.setMovementIncomeAnnotation(movementIncomeAnnotation);
		addAnnotation(movementExpensesAnnotation);
	}

	@DependsOn({"selectedBook","bookSelectionType","selectedClient","currentYear","annotationsFilter"})
	@Override
	public List<Annotation> getAnnotations() {
		List<Annotation> annotations = new LinkedList<Annotation>();
		if (bookSelection.getSelectedBook() != null) {
			annotations = AnnotationsBusiness.getMovementExpensesAnnotations(bookSelection, Optional.<Integer>of(currentYear), SortingCriteria.DESCENDING, annotationsFilter);
			if (justCreatedAnnotation != null
					&& annotations.contains(justCreatedAnnotation)) {
				annotations.remove(justCreatedAnnotation);
				annotations.add(0, justCreatedAnnotation);
				justCreatedAnnotation = null;
			}
		}
		Clients.clearBusy();
		return annotations;
	}

	
}
