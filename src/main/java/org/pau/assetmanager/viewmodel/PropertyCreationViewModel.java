/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.pau.assetmanager.business.PropertiesBusiness;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.Property;
import org.pau.assetmanager.viewmodel.type.PropertyType;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
/**
 * This class is a ViewModel that controls the addition 
 * of a property which is described in the ZUL 'add_property.zul'
 * 
 * @author Pau Carré Cardona
 *
 */

public class PropertyCreationViewModel {

	// property type to create
	private PropertyType selectedPropertyType;
	// name of the property
	private String name;
	// field that holds the visibility/enabling state of the window
	private Boolean isVisible;
	// client to which the property will be created
	private Client client;

	private static Collection<PropertyType> propertyBookTypes = new ArrayList<PropertyType>(
			Arrays.asList(PropertyType.values()));

	public Collection<PropertyType> getPropertyBookTypes() {
		return propertyBookTypes;
	}

	@Init
	public void init() {
		selectedPropertyType = PropertyType.TENEMENT;
		name = "";
		isVisible = false;
		client = null;
	}

	public Boolean getIsVisible() {
		return isVisible;
	}

	public void setIsVisible(Boolean isVisible) {
		this.isVisible = isVisible;
	}

	@NotifyChange({ "isVisible", "client" })
	@GlobalCommand
	public void createPropertyForClient(@BindingParam("client") Client client) {
		this.isVisible = true;
		this.client = client;
	}
	
	@NotifyChange({"isVisible", "name", "client"})
	@Command
	public void createProperty() {
		if (name.trim().length() < 3) {
			Messagebox.show(
					"El nombre tiene que tener al menos tres caracteres",
					"Error", Messagebox.OK, Messagebox.ERROR,
					new org.zkoss.zk.ui.event.EventListener<Event>() {
						public void onEvent(Event evt)
								throws InterruptedException {
							if (evt.getName().equals("onOK")) {

							}
						}
					});
		} else {
			Property property = new Property();
			property.setClient(client);
			property.setName(name);
			property.setType(selectedPropertyType);
			PropertiesBusiness.createProperty(property);
			isVisible = false;
			name = "";
			client = null;
			Component add_book_window = Path
					.getComponent("//add_book_page/add_book_window");
			Events.postEvent("onNotifyPropertyAdded", add_book_window, property);
		}
	}

	public PropertyType getSelectedPropertyType() {
		return selectedPropertyType;
	}

	public void setSelectedPropertyType(PropertyType selectedPropertyType) {
		this.selectedPropertyType = selectedPropertyType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
