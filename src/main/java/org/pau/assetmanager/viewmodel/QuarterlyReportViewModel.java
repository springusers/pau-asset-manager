/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.pau.assetmanager.business.AnnotationsBusiness;
import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.utils.TestTolerantClients;
import org.pau.assetmanager.viewmodel.comparator.AnnotationDateQuarterlyComparator;
import org.pau.assetmanager.viewmodel.grouping.AnnotationDateQuarterlyGroupingModel;
import org.pau.assetmanager.viewmodel.type.BookSelectionType;
import org.pau.assetmanager.viewmodel.utils.BookSelection;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.event.Event;

import com.google.common.base.Optional;
/**
 * This class is the ViewModel that controls the 
 * quarterly tax payment which is defined
 * in the ZUL 'grouping_model_view_quarter.zul'
 * 
 * @author Pau Carré Cardona
 *
 */

public class QuarterlyReportViewModel {

	// book selection that contains 
	private BookSelection bookSelection;
	// year selected in the report
	private Integer quarterlyReportYear;

	@Init
	public void init() {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(new Date());
		quarterlyReportYear = calendar.get(Calendar.YEAR);
	}

	public BookSelectionType getBookSelectionType() {
		return bookSelection.getBookSelectionType();
	}

	public Client getSelectedClient() {
		return bookSelection.getSelectedClient();
	}

	@NotifyChange({ "annotationsDateQuarterlyGroupingModel" })
	@GlobalCommand
	public void updateReportAnnotations() {

	}

	@NotifyChange({ "selectedBook", "bookSelectionType", "selectedClient" })
	@GlobalCommand
	public void updateBookSelection(
			@BindingParam("bookSelection") BookSelection bookSelection) {
		this.bookSelection = bookSelection;
	}
	
	public Book getSelectedBook() {
		return bookSelection.getSelectedBook();
	}


	public Integer getQuarterlyReportYear() {
		return quarterlyReportYear;
	}

	@NotifyChange({ "quarterlyReportYear" })
	public void setQuarterlyReportYear(Integer monthlyReportYear) {
		this.quarterlyReportYear = monthlyReportYear;
	}

	@Command
	@NotifyChange({ "quarterlyReportYear" })
	public void refreshQuarterlyReport(
			@ContextParam(ContextType.TRIGGER_EVENT) Event event) {
		String value = (String) event.getData();
		this.quarterlyReportYear = new Integer(value);
	}

	@DependsOn({"quarterlyReportYear","selectedBook"})
	public AnnotationDateQuarterlyGroupingModel getAnnotationsDateQuarterlyGroupingModel() {
		TestTolerantClients.clearBusy();
		return new AnnotationDateQuarterlyGroupingModel(
				getAnnotations(quarterlyReportYear),
				new AnnotationDateQuarterlyComparator(), true);
	}

	
	private List<Annotation> getAnnotations(Integer year) {
		return AnnotationsBusiness.getQuarterlyHaciendaAnnotationsFromDatabase(bookSelection, Optional.<Integer>of(year));
	}

}
