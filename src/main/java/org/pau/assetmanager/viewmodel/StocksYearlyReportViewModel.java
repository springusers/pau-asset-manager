/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import org.pau.assetmanager.business.AnnotationsBusiness;
import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.StockExpensesAnnotation;
import org.pau.assetmanager.entities.StockIncomeAnnotation;
import org.pau.assetmanager.viewmodel.comparator.AnnotationDateStocksYearlyComparator;
import org.pau.assetmanager.viewmodel.grouping.AnnotationDateStocksYearlyGroupingModel;
import org.pau.assetmanager.viewmodel.stocks.StocksUtils;
import org.pau.assetmanager.viewmodel.type.BookSelectionType;
import org.pau.assetmanager.viewmodel.type.ClientDomainType;
import org.pau.assetmanager.viewmodel.utils.BookSelection;
import org.pau.assetmanager.viewmodel.utils.SortingCriteria;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;


/**
 * This is the ViewModel for the yearly stock
 * tax payment which is defined in the ZUL 
 * 'stocks_grouping_model_view_yearly.zul'
 * 
 * @author Pau Carré Cardona
 *
 */

public class StocksYearlyReportViewModel {

	// book selected
	private BookSelection bookSelection = null;
	// year of the report
	private Integer yearlyReportYear;

	@Init
	public void init() {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(new Date());
		yearlyReportYear = calendar.get(Calendar.YEAR);
	}

	public BookSelectionType getBookSelectionType() {
		return bookSelection.getBookSelectionType();
	}

	public Client getSelectedClient() {
		return bookSelection.getSelectedClient();
	}

	@NotifyChange({ "selectedBook", "bookSelectionType", "selectedClient" })
	@GlobalCommand
	public void updateBookSelection(
			@BindingParam("bookSelection") BookSelection bookSelection) {
		this.bookSelection = bookSelection;
	}

	@NotifyChange({ "yearlyReportYear" })
	@Command
	public void refreshYearlyReport(
			@ContextParam(ContextType.TRIGGER_EVENT) Event event) {
		String value = (String) event.getData();
		Integer newValue = new Integer(value);
		if (!newValue.equals(yearlyReportYear)) {
			this.yearlyReportYear = newValue;
		}
	}

	@NotifyChange({ "annotationsDateYearlyGroupingModel" })
	@GlobalCommand
	public void updateReportAnnotations() {

	}

	@DependsOn({ "selectedBook", "yearlyReportYear" })
	public AnnotationDateStocksYearlyGroupingModel getAnnotationsDateYearlyGroupingModel() {
		List<Annotation> annotations = getStocksAnnotationsUntilYear(bookSelection,
				yearlyReportYear);
		return new AnnotationDateStocksYearlyGroupingModel(
				StocksUtils.getStockIncomeDescriptionList(annotations),
				new AnnotationDateStocksYearlyComparator(), true);
	}
	
	public static List<Annotation> getStocksAnnotationsUntilYear(
			BookSelection bookSelection, Integer lastYear) {
		List<Annotation> listOfAnnotations = AnnotationsBusiness
				.getAllStocksAnnotationsFromDatabaseUntilYear(bookSelection, lastYear,
						ClientDomainType.CURRENT_CLIENT,
						SortingCriteria.ASCENDING);
		removeStocksAnnotationsWithoutStocks(listOfAnnotations);
		Clients.clearBusy();
		return listOfAnnotations;
	}


//	public static List<Annotation> getStocksAnnotations(
//			BookSelection bookSelection, Integer year) {
//		List<Annotation> listOfAnnotations = AnnotationsBusiness
//				.getAllStocksAnnotationsFromDatabase(bookSelection, Optional.<Integer>of(year),
//						ClientDomainType.CURRENT_CLIENT,
//						SortingCriteria.DESCENDING);
//		removeStocksAnnotationsWithoutStocks(listOfAnnotations);
//		Clients.clearBusy();
//		return listOfAnnotations;
//	}

	private static void removeStocksAnnotationsWithoutStocks(
			List<Annotation> annotations) {
		List<Annotation> currentList = new LinkedList<Annotation>();
		for (Annotation annotation : annotations) {
			if (!(annotation instanceof StockExpensesAnnotation)
					&& !(annotation instanceof StockIncomeAnnotation)) {
				currentList.add(annotation);
			} else {
				if (annotation instanceof StockExpensesAnnotation) {
					StockExpensesAnnotation stockExpensesAnnotation = (StockExpensesAnnotation) annotation;
					if (stockExpensesAnnotation.getNumberOfStocks() == 0) {
						currentList.add(stockExpensesAnnotation);
					}
				} else if (annotation instanceof StockIncomeAnnotation) {
					StockIncomeAnnotation stockIncomeAnnotation = (StockIncomeAnnotation) annotation;
					if (stockIncomeAnnotation.getNumberOfStocks() == 0) {
						currentList.add(stockIncomeAnnotation);
					}
				}
			}
		}
		annotations.removeAll(currentList);
	}

	public Book getSelectedBook() {
		return bookSelection.getSelectedBook();
	}

	public Integer getYearlyReportYear() {
		return yearlyReportYear;
	}

}
