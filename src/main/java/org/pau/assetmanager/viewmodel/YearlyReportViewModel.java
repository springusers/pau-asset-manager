/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.pau.assetmanager.business.AnnotationsBusiness;
import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.GeneralBook;
import org.pau.assetmanager.utils.TestTolerantClients;
import org.pau.assetmanager.viewmodel.comparator.AnnotationDateYearlyComparator;
import org.pau.assetmanager.viewmodel.grouping.AnnotationDateYearlyGroupingModel;
import org.pau.assetmanager.viewmodel.type.BookSelectionType;
import org.pau.assetmanager.viewmodel.utils.BookSelection;
import org.pau.assetmanager.viewmodel.utils.SortingCriteria;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.event.Event;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;

/**
 * This is the ViewModel for the yearly tax report
 * described in the ZUL 'grouping_model_view_yearly.zul'
 * 
 * @author Pau Carré Cardona
 */

public class YearlyReportViewModel {

	// book selection
	private BookSelection bookSelection = null;
	// year for the report
	private Integer yearlyReportYear;

	@Init
	public void init() {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(new Date());
		yearlyReportYear = calendar.get(Calendar.YEAR);
	}
	
	@NotifyChange({ "selectedBook", "bookSelectionType", "selectedClient" })
	@GlobalCommand
	public void updateBookSelection(
			@BindingParam("bookSelection") BookSelection bookSelection) {
		this.bookSelection = bookSelection;
	}
	
	@NotifyChange({ "yearlyReportYear" })
	@Command
	public void refreshYearlyReport(
			@ContextParam(ContextType.TRIGGER_EVENT) Event event) {
		String value = (String) event.getData();
		Integer newValue = new Integer(value);
		if (!newValue.equals(yearlyReportYear)) {
			this.yearlyReportYear = newValue;
		}
	}

	public void setYearlyReportYear(Integer yearlyReportYear) {
		this.yearlyReportYear = yearlyReportYear;
	}

	public BookSelectionType getBookSelectionType() {
		return bookSelection.getBookSelectionType();
	}

	public Client getSelectedClient() {
		return bookSelection.getSelectedClient();
	}

	@NotifyChange({ "annotationsDateYearlyGroupingModel" })
	@GlobalCommand
	public void updateReportAnnotations() {

	}


	@DependsOn({ "selectedBook", "yearlyReportYear" })
	public AnnotationDateYearlyGroupingModel getAnnotationsDateYearlyGroupingModel() {
		return new AnnotationDateYearlyGroupingModel(getAnnotations(bookSelection, yearlyReportYear),
				new AnnotationDateYearlyComparator(), true);
	}

	private static List<Annotation> getAnnotations(BookSelection bookSelection,
			Integer year) {
		List<Annotation> listOfAnnotations = AnnotationsBusiness.getYearlyReportAnnotationsFromDatabase(bookSelection, year, SortingCriteria.DESCENDING);
		List<Annotation> listOfNonGeneralAnnotations = Lists.newLinkedList(Collections2.filter(listOfAnnotations, 
				new Predicate<Annotation>() {
					@Override
					public boolean apply(Annotation annotation) {						
						return !(annotation.getBook() instanceof GeneralBook);
					}
		}));
		TestTolerantClients.clearBusy();
		return listOfNonGeneralAnnotations;
	}

	public Book getSelectedBook() {
		return bookSelection.getSelectedBook();
	}

	public Integer getYearlyReportYear() {
		return yearlyReportYear;
	}

}
