/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.annotationimport;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.pau.assetmanager.business.BooksBusiness;
import org.pau.assetmanager.business.ClientsBusiness;
import org.pau.assetmanager.business.ConceptsBusiness;
import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.viewmodel.type.BookSelectionType;
import org.pau.assetmanager.viewmodel.utils.BookSelection;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.NotifyChange;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;

/**
 * This class represents the state of one of the annotations
 * to import in the ViewModel 'ImportAnnotationsViewModel',
 * which contains a list 'importedAnnotations' of 'AnnotationImport's
 * 
 * @author Pau Carré Cardona
 */

public class AnnotationImport {

	// concept used in both import annotation (income and expenses
	private String concept;
	// amount of the imported annotation (only positive number)
	private Long twoDecimalLongCodifiedAmount;
	// date of the imported annotation
	private Date date;
	// the book for the imported annotation
	private Book book;
	// the client of the imported annotation (the client of 'book')
	private Client client;
	// the avaliable concepts for the import
	private List<String> availableConcepts;
	// avaliable clients for the user
	private List<Client> availableClients;
	// avaliable books for the selected client ('client' field)
	private List<Book> availableBooks;

	public AnnotationImport(String concept, Double amount, Date date, Book book,
			Client client) {
		super();
		this.concept = concept;
		this.twoDecimalLongCodifiedAmount =  Math.round(amount * 100.0);
		this.date = date;
		this.book = book;
		this.client = client;
	}

	public List<Client> getAvailableClients() {
		availableClients = ClientsBusiness.getClients();
		return availableClients;
	}
	
	@DependsOn("client")
	public List<Book> getAvailableBooks() {
		availableBooks = BooksBusiness.getBooksFromClient(client);
		return availableBooks;
	}

	@DependsOn({"book", "concept"})
	public Collection<String> getAvailableConcepts() {
		Collection<String> coneptsInBook = ConceptsBusiness.getConceptsUsedInBook(new BookSelection(book, BookSelectionType.SINGLE_BOOK, client));
		availableConcepts =  new LinkedList<String>(Collections2.transform(coneptsInBook, 
				new Function<String, String>() {
					@Override
					public String apply(String concept) {
						return concept;
					}
		}));
		availableConcepts.add(concept);
		return availableConcepts;
	}

	public void setAmount(Long amount) {
		this.twoDecimalLongCodifiedAmount = amount;
	}

	@DependsOn("book")
	public String getConcept() {
		return concept;
	}

	@NotifyChange("concept")
	public void setConcept(String concept) {
		// protect againts null sets by the combobox when the model is
		// not synchronized
		if(concept != null){
			this.concept = concept;
		}
	}

	public Double getAmount() {
		return new Double(this.twoDecimalLongCodifiedAmount) / 100.0;
	}

	public void setAmount(Double amount) {
		if (amount == null) {
			amount = 0.0;
		}
		if (amount < 0.0) {
			amount *= -1.0;
		}
		this.twoDecimalLongCodifiedAmount = Math.round(amount * 100.0);
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public Long getTwoDecimalLongCodifiedAmount() {
		return this.twoDecimalLongCodifiedAmount;
	}

	public void setTwoDecimalLongCodifiedAmount(Long amount) {
		if (amount == null) {
			amount = 0L;
		}
		if (amount < 0L) {
			amount *= -1L;
		}
		this.twoDecimalLongCodifiedAmount = amount;
	}
	
	@DependsOn("client")
	public Book getBook() {
		if(!book.getClient().equals(client)){
			// update book
			//TODO: check that there is at least one book
			Book defaultBook = BooksBusiness.getBooksFromClient(client).get(0);
			setBook(defaultBook);
		}
		return book;
	}

	@NotifyChange("book")
	public void setBook(Book book) {
		this.book = book;
	}

	public Client getClient() {
		return client;
	}

	@NotifyChange("client")
	public void setClient(Client client) {
		this.client = client;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((twoDecimalLongCodifiedAmount == null) ? 0 : twoDecimalLongCodifiedAmount.hashCode());
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((concept == null) ? 0 : concept.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnnotationImport other = (AnnotationImport) obj;
		if (twoDecimalLongCodifiedAmount == null) {
			if (other.twoDecimalLongCodifiedAmount != null)
				return false;
		} else if (!twoDecimalLongCodifiedAmount.equals(other.twoDecimalLongCodifiedAmount))
			return false;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (concept == null) {
			if (other.concept != null)
				return false;
		} else if (!concept.equals(other.concept))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		return true;
	}

}
