/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.annotationimport;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import org.pau.assetmanager.business.AnnotationsBusiness;
import org.pau.assetmanager.business.BooksBusiness;
import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.ExpensesAnnotation;
import org.pau.assetmanager.entities.GeneralExpensesAnnotation;
import org.pau.assetmanager.entities.GeneralIncomeAnnotation;
import org.pau.assetmanager.entities.IncomeAnnotation;
import org.pau.assetmanager.entities.PropertyBook;
import org.pau.assetmanager.entities.PropertyExpensesAnnotation;
import org.pau.assetmanager.entities.PropertyIncomeAnnotation;
import org.pau.assetmanager.entities.StockExpensesAnnotation;
import org.pau.assetmanager.entities.StockIncomeAnnotation;
import org.pau.assetmanager.entities.StocksBook;
import org.pau.assetmanager.viewmodel.annotationimport.sanostra.SaNostraAnnotationImport;
import org.pau.assetmanager.viewmodel.annotationimport.sanostra.SaNostraImporter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.util.Clients;

/**
 * This class is the ViewModel for the import of annotations
 * that is defined in the ZUL 'import_annotations.zul'
 * 
 * @author Pau Carré Cardona
 */

public class ImportAnnotationsViewModel {

	// annotations to be imported which their visual state
	private List<AnnotationImport> importedAnnotations;
	// default book to use for the import (current selected book)
	private Book defaultBook;
	// list of available books for the current user
	private List<Book> books;

	@Init
	public void init() {
		books = BooksBusiness.getBooks();
		if(books != null && books.size() > 0){
			defaultBook = books.get(0);
		}
		importedAnnotations = new LinkedList<AnnotationImport>();
	}

	@NotifyChange("importedAnnotations")
	@Command
	public void clearAnnotations(
			@ContextParam(ContextType.TRIGGER_EVENT) Event event) {
		event.stopPropagation();
		importedAnnotations.clear();
		Component targetButton = event.getTarget();
		Events.postEvent(targetButton, new Event("onAnnotationsCleared"));
	}

	@NotifyChange("importedAnnotations")
	@Command
	public void importAnnotations(
			@ContextParam(ContextType.TRIGGER_EVENT) Event event) {
		for (AnnotationImport annotationImport : importedAnnotations) {
			Book annotationImportBook = annotationImport.getBook();
			String concept = annotationImport.getConcept();
			Annotation annotationToCreate = null;
			if (annotationImportBook instanceof PropertyBook) {
				// property book
				if (annotationImport.getTwoDecimalLongCodifiedAmount() >= 0) {
					PropertyIncomeAnnotation propertyIncomeAnnotation = new PropertyIncomeAnnotation();
					propertyIncomeAnnotation.setUpDefaults();
					annotationToCreate = propertyIncomeAnnotation;
				} else {
					PropertyExpensesAnnotation propertyExpensesAnnotation = new PropertyExpensesAnnotation();
					propertyExpensesAnnotation.setUpDefaults();
					annotationToCreate = propertyExpensesAnnotation;
				}
			} else if (annotationImportBook instanceof StocksBook) {
				// stocks book
				if (annotationImport.getTwoDecimalLongCodifiedAmount() >= 0) {
					StockIncomeAnnotation stockIncomeAnnotation = new StockIncomeAnnotation();
					annotationToCreate = stockIncomeAnnotation;
				} else {
					StockExpensesAnnotation stockExpensesAnnotation = new StockExpensesAnnotation();
					annotationToCreate = stockExpensesAnnotation;
				}
			} else {
				// general books
				if (annotationImport.getTwoDecimalLongCodifiedAmount() >= 0) {
					IncomeAnnotation incomeAnnotation = new GeneralIncomeAnnotation();
					annotationToCreate = incomeAnnotation;
				} else {
					ExpensesAnnotation expensesAnnotation = new GeneralExpensesAnnotation();
					annotationToCreate = expensesAnnotation;
				}
			}
			if (annotationToCreate != null) {
				annotationToCreate.setConcept(concept);
				annotationToCreate.setTwoDecimalLongCodifiedAmount(annotationImport
						.getTwoDecimalLongCodifiedAmount());
				annotationToCreate.setBook(annotationImportBook);
				annotationToCreate.setDate(annotationImport.getDate());
				AnnotationsBusiness.createAnnotation(annotationToCreate);
			}
		}
		importedAnnotations.clear();
		Clients.clearBusy();
	}

	@NotifyChange("importedAnnotations")
	@Command
	public void uploadFile(@BindingParam("media") Media media) {
		InputStream inputStream = media.getStreamData();
		List<SaNostraAnnotationImport> saNostraAnnotationImportList = SaNostraImporter
				.getSaNostraAnnotationImportList(inputStream);
		importedAnnotations.clear();
		for (SaNostraAnnotationImport saNostraAnnotationImport : saNostraAnnotationImportList) {
			AnnotationImport annotationImport = new AnnotationImport(
					saNostraAnnotationImport.getConcept(),
					saNostraAnnotationImport.getAmount(),
					saNostraAnnotationImport.getDate(), defaultBook,
					defaultBook.getClient());
			importedAnnotations.add(annotationImport);
		}
		Clients.clearBusy();
	}

	public List<AnnotationImport> getImportedAnnotations() {
		return importedAnnotations;
	}

	public void setImportedAnnotations(
			List<AnnotationImport> importedAnnotations) {
		this.importedAnnotations = importedAnnotations;
	}

	@Command
	public void addConcept(
			@BindingParam("annotationImport") AnnotationImport annotationImport,
			@ContextParam(ContextType.TRIGGER_EVENT) InputEvent event) {
		String newConcept = event.getValue().trim();
		annotationImport.setConcept(newConcept);
	}

	@NotifyChange("importedAnnotations")
	@Command
	public void deleteAnnotationImport(
			@BindingParam("importedAnnotation") AnnotationImport importedAnnotation) {
		importedAnnotations.remove(importedAnnotation);
		Clients.clearBusy();
	}

}
