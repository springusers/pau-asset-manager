/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.annotationimport.sanostra;

import java.util.Date;
/**
 * This class is represents a single annotation to import
 * for the import of annotations from 'Sa Nostra' export files
 * 
 * @author Pau Carré Cardona
 */

public class SaNostraAnnotationImport{

	// concept of the annotation
	private String concept;
	// amount of the annotation
	private Double amount;
	// date of the annotation
	private Date date;
	
	public SaNostraAnnotationImport() {
		super();
	}
	
	public SaNostraAnnotationImport(String concept, Double amount, Date date) {
		super();
		this.concept = concept;
		this.amount = amount;
		this.date = date;
	}
	public String getConcept() {
		return concept;
	}
	public void setConcept(String concept) {
		this.concept = concept;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Boolean allFieldsAreNotNull(){
		return concept != null && amount != null && date != null;
	}
	
	@Override
	public String toString() {
		return "SaNostraAnnotationImport [concept=" + concept + ", amount="
				+ amount + ", date=" + date + "]";
	}
	
}