/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.annotationimport.sanostra;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This class is the SAX parser for SaNostra file exports that 
 * generates a list of 'SaNostraAnnotationImport' to be imported.
 * 
 * Even thought the SaNostra file extension is XLS the actual 
 * internal contents of the file are HTML.
 * 
 * @author Pau Carré Cardona
 */

public class SaNostraDefaultHandler extends DefaultHandler {
// Format example
//<tr>
//	<td align="center" class="contenidocampo">17-08-2011</td>
//	<td align="left" class="contenidocampo">XXXXXX TT GGGG VV ZZZZZ     </td>
//	<td align="right" class="contenidocampo"><font color="red">-65,10</font></td>
//	<td align="right" class="contenidocampo"><font color="blue">+1.431,55</font></td>
//</tr>	
		private static final Logger logger = LogManager.getLogger(SaNostraDefaultHandler.class);

		public SaNostraDefaultHandler(){
			super();
		}
		
		private static Integer DATE_INDEX = 1;
		private static Integer CONCEPT_INDEX = 2;
		private static Integer AMOUNT_INDEX = 3;
		
		private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("###,###.##");
		private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
		
		private static final String TR = "tr";
		boolean inTr = false;

		private static final String TD = "td";
		boolean inTd = false;

		private SaNostraAnnotationImport currentSaNostraAnnotationImport = null;
		private List<SaNostraAnnotationImport> saNostraAnnotationImportList = new LinkedList<>();
		
		private Integer tdIndex = 0;

		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			inTr = inTr || qName.equalsIgnoreCase(TR);
			inTd = inTd || qName.equalsIgnoreCase(TD);
			if (qName.equalsIgnoreCase(TD)) {
				tdIndex++;
			}
		}

		public List<SaNostraAnnotationImport> getSaNostraAnnotationImportList() {
			return saNostraAnnotationImportList;
		}

		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			inTr = inTr && !qName.equalsIgnoreCase(TR);
			inTd = inTd && !qName.equalsIgnoreCase(TD);
			if (!inTr) {
				if(currentSaNostraAnnotationImport != null && currentSaNostraAnnotationImport.allFieldsAreNotNull()){
					saNostraAnnotationImportList.add(currentSaNostraAnnotationImport);
				}
				currentSaNostraAnnotationImport = null;
				tdIndex = 0;
			}
		}

		public void characters(char ch[], int start, int length)
				throws SAXException {
			if(inTd){		
				String value = new String(ch, start, length).trim();
				if(tdIndex.equals(DATE_INDEX)){
					if(currentSaNostraAnnotationImport == null){
						currentSaNostraAnnotationImport = new SaNostraAnnotationImport();
					}
					try{
						Date date = DATE_FORMAT.parse(value);
						currentSaNostraAnnotationImport.setDate(date);
					}catch(Throwable t){
						logger.error("Error trying to fomat the date '" + value + "'", t);
						currentSaNostraAnnotationImport = null;
					}
				}else if(currentSaNostraAnnotationImport != null && tdIndex.equals(CONCEPT_INDEX)){
					currentSaNostraAnnotationImport.setConcept(value);
				}else if(currentSaNostraAnnotationImport != null && tdIndex.equals(AMOUNT_INDEX)){
					try{
						value = value.replace("+", "");
						Number amount = DECIMAL_FORMAT.parse(value);
						currentSaNostraAnnotationImport.setAmount(amount.doubleValue());
					}catch(Throwable t){
						logger.error("Error trying to format date value '"+ value+"'", t);
						currentSaNostraAnnotationImport = null;
					}
				}
			}
		}

	}