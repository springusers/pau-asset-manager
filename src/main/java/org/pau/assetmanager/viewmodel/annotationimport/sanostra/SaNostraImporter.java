/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.annotationimport.sanostra;

import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pau.assetmanager.utils.AssetManagerRuntimeException;
/**
 * This is a singleton class that executes an import of 
 * an HTML export from SaNostra.
 * 
 * @author Pau Carré Cardona
 */

public class SaNostraImporter {
	
	private static Logger logger = LogManager
			.getLogger(SaNostraImporter.class);
	
	public SaNostraImporter() {

	}

	/**
	 * @param inputStream the input stream of the SaNostra exported file
	 * @return the list of SaNostra annotations
	 */
	public static List<SaNostraAnnotationImport> getSaNostraAnnotationImportList(InputStream inputStream) {
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			SaNostraDefaultHandler sanNostraDefaultHandler = new SaNostraDefaultHandler();
			saxParser.parse(inputStream, sanNostraDefaultHandler);
			List<SaNostraAnnotationImport> saNostraAnnotationImportList = sanNostraDefaultHandler.getSaNostraAnnotationImportList();
			return saNostraAnnotationImportList;
		} catch (Exception e) {
			String message = "Error importing XML file from Sa Nostra";
			logger.error("Error importing XML file from Sa Nostra", e);
			throw new AssetManagerRuntimeException(message, e);
		}
	};

}
