/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.text.NumberFormat;
import java.util.List;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryMarker;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.ui.Layer;
import org.jfree.ui.LengthAdjustmentType;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;
import org.pau.assetmanager.entities.PropertyBook;
import org.pau.assetmanager.viewmodel.utils.NumberFomatter;
/**
 * This class is a utility class to add styles to charts
 * 
 * @author Pau Carré Cardona
 */
public class PrepareChart {

	public static void commonPrepareJFreeChart(LineAndShapeRenderer renderer) {
		renderer.setSeriesStroke(0, new BasicStroke(1));
		renderer.setSeriesStroke(1, new BasicStroke(1));
		renderer.setSeriesStroke(2, new BasicStroke(1));
		renderer.setSeriesStroke(3, new BasicStroke(1));
		renderer.setSeriesStroke(4, new BasicStroke(1));
		renderer.setSeriesStroke(5, new BasicStroke(1));
		renderer.setSeriesStroke(6, new BasicStroke(1));

		renderer.setSeriesItemLabelsVisible(0, true);
		renderer.setSeriesItemLabelsVisible(1, true);
		renderer.setSeriesItemLabelsVisible(2, true);
		renderer.setSeriesItemLabelsVisible(3, true);
		renderer.setSeriesItemLabelsVisible(4, true);
		renderer.setSeriesItemLabelsVisible(5, true);
		renderer.setSeriesItemLabelsVisible(6, true);

		renderer.setSeriesLinesVisible(0, true);
		renderer.setSeriesLinesVisible(1, true);
		renderer.setSeriesLinesVisible(2, true);
		renderer.setSeriesLinesVisible(3, true);
		renderer.setSeriesLinesVisible(4, true);
		renderer.setSeriesLinesVisible(5, true);
		renderer.setSeriesLinesVisible(6, true);

		renderer.setSeriesShapesVisible(0, true);
		renderer.setSeriesShapesVisible(1, true);
		renderer.setSeriesShapesVisible(2, true);
		renderer.setSeriesShapesVisible(3, true);
		renderer.setSeriesShapesVisible(4, true);
		renderer.setSeriesShapesVisible(5, true);
		renderer.setSeriesShapesVisible(6, true);
	}

	public static void prepareBalanceChart(JFreeChart jfchart) {

		CategoryPlot categoryPlot = ((CategoryPlot) jfchart.getPlot());
		NumberAxis numberAxis = (NumberAxis) categoryPlot.getRangeAxis();
		numberAxis.setAutoRange(true);
		numberAxis.setAutoRangeIncludesZero(false);
		categoryPlot.setBackgroundPaint(Color.WHITE);
		categoryPlot.setDomainGridlinePaint(Color.WHITE);
		categoryPlot.setRangeMinorGridlinePaint(Color.WHITE);
		categoryPlot.setRangeGridlinePaint(Color.BLACK);
		LineAndShapeRenderer renderer = (LineAndShapeRenderer) categoryPlot
				.getRenderer();

		renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		renderer.setBaseItemLabelsVisible(true);

		commonPrepareJFreeChart(renderer);

		renderer.setSeriesPaint(0, Color.black);

		renderer.setSeriesItemLabelPaint(0, Color.black);

	}

	public static void prepareSimpleJFreeChart(JFreeChart jfchart) {

		CategoryPlot categoryPlot = ((CategoryPlot) jfchart.getPlot());
		categoryPlot.getRangeAxis().resizeRange(1.2);
		categoryPlot.setBackgroundPaint(Color.WHITE);
		categoryPlot.setDomainGridlinePaint(Color.WHITE);
		categoryPlot.setRangeMinorGridlinePaint(Color.WHITE);
		categoryPlot.setRangeGridlinePaint(Color.BLACK);
		LineAndShapeRenderer renderer = (LineAndShapeRenderer) categoryPlot
				.getRenderer();

		renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		renderer.setBaseItemLabelsVisible(true);

		commonPrepareJFreeChart(renderer);

		renderer.setSeriesPaint(0, new Color(0xB3, 0xBE, 0xFF));
		renderer.setSeriesPaint(1, new Color(0xFF, 0xB3, 0xB3));
		renderer.setSeriesPaint(2, new Color(0x61, 0x9E, 0x42));
		renderer.setSeriesPaint(3, Color.black);

		renderer.setSeriesItemLabelPaint(0, new Color(0x21, 0x21, 0xFF));
		renderer.setSeriesItemLabelPaint(1, new Color(0xFF, 0x36, 0x36));
		renderer.setSeriesItemLabelPaint(2, new Color(0x61, 0x9E, 0x42));
		renderer.setSeriesItemLabelPaint(3, Color.black);

	}

	public static void preparePropertiesVsOwnAllPropertiesJFreeChart(
			JFreeChart jfchart, CategoryDataset categoryModel) {

		CategoryPlot categoryPlot = ((CategoryPlot) jfchart.getPlot());
		categoryPlot.getRangeAxis().resizeRange(1.2);
		categoryPlot.setBackgroundPaint(Color.WHITE);
		categoryPlot.setDomainGridlinePaint(Color.WHITE);
		categoryPlot.setRangeMinorGridlinePaint(Color.WHITE);
		categoryPlot.setRangeGridlinePaint(Color.BLACK);
		BarRenderer renderer = (BarRenderer) categoryPlot.getRenderer();

		renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		renderer.setBaseItemLabelsVisible(true);

		renderer.setBaseItemLabelFont(new Font("Serif", Font.PLAIN, 10));
		renderer.setItemLabelAnchorOffset(15);

		renderer.setSeriesStroke(0, new BasicStroke(1));
		renderer.setSeriesStroke(1, new BasicStroke(1));
		renderer.setSeriesStroke(2, new BasicStroke(1));
		renderer.setSeriesStroke(3, new BasicStroke(1));
		renderer.setSeriesStroke(4, new BasicStroke(1));
		renderer.setSeriesStroke(5, new BasicStroke(1));
		renderer.setSeriesPaint(0, new Color(0x77, 0x77, 0xFF));
		renderer.setSeriesPaint(1, new Color(0xCC, 0xCC, 0xFF));
		renderer.setSeriesPaint(2, new Color(0x00, 0x00, 0xFF));
		renderer.setSeriesPaint(3, new Color(0xFF, 0x77, 0x77));
		renderer.setSeriesPaint(4, new Color(0xFF, 0xCC, 0xCC));
		renderer.setSeriesPaint(5, new Color(0xFF, 0x00, 0x00));
		for (int i = 0; i < categoryModel.getColumnKeys().size(); i++) {
			String label = (String) categoryModel.getColumnKey(i);
			CategoryMarker marker = new CategoryMarker(label);
			marker.setLabel("");
			marker.setPaint(Color.cyan);
			marker.setOutlinePaint(Color.cyan);
			marker.setAlpha(0.1f);
			marker.setLabelAnchor(RectangleAnchor.TOP);
			marker.setLabelTextAnchor(TextAnchor.TOP_CENTER);
			marker.setLabelOffsetType(LengthAdjustmentType.CONTRACT);
			categoryPlot.addDomainMarker(marker, Layer.BACKGROUND);
		}
		renderer.setDrawBarOutline(false);
		renderer.setShadowVisible(false);
		renderer.setItemMargin(.1);
		renderer.setBarPainter(new StandardBarPainter());
		renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
				ItemLabelAnchor.OUTSIDE12, TextAnchor.TOP_RIGHT));
		renderer.setBaseNegativeItemLabelPosition(new ItemLabelPosition(
				ItemLabelAnchor.OUTSIDE6, TextAnchor.BOTTOM_RIGHT));
	}

	public static void prepareJFreeBarChartForStocks(JFreeChart jfchart,
			CategoryDataset categoryModel) {

		CategoryPlot categoryPlot = ((CategoryPlot) jfchart.getPlot());
		categoryPlot.getRangeAxis().resizeRange(1.2);
		categoryPlot.setBackgroundPaint(Color.WHITE);
		categoryPlot.setDomainGridlinePaint(Color.WHITE);
		categoryPlot.setRangeMinorGridlinePaint(Color.WHITE);
		categoryPlot.setRangeGridlinePaint(Color.BLACK);
		BarRenderer renderer = (BarRenderer) categoryPlot.getRenderer();

		renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());

		renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator(
				"{2}€ {1} ", NumberFomatter.PERCENTAGE_FORMATTER , NumberFormat.getPercentInstance()));
		renderer.setBaseItemLabelsVisible(true);
		renderer.setBaseItemLabelFont(new Font("Serif", Font.PLAIN, 12));
		renderer.setItemLabelAnchorOffset(15);

		renderer.setDrawBarOutline(false);
		renderer.setShadowVisible(false);
		renderer.setItemMargin(.1);
		renderer.setBarPainter(new StandardBarPainter());

	}

	public static void prepareJFreeBarChart(JFreeChart jfchart,
			List<PropertyBook> listOfPropertties, CategoryDataset categoryModel) {

		CategoryPlot categoryPlot = ((CategoryPlot) jfchart.getPlot());
		categoryPlot.getRangeAxis().resizeRange(1.2);
		categoryPlot.setBackgroundPaint(Color.WHITE);
		categoryPlot.setDomainGridlinePaint(Color.WHITE);
		categoryPlot.setRangeMinorGridlinePaint(Color.WHITE);
		categoryPlot.setRangeGridlinePaint(Color.BLACK);
		BarRenderer renderer = (BarRenderer) categoryPlot.getRenderer();

		renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		renderer.setBaseItemLabelsVisible(false);
		for (int i = 0; i < listOfPropertties.size(); i++) {
			renderer.setSeriesStroke(i, new BasicStroke(1));
		}
		for (int i = 0; i < categoryModel.getColumnKeys().size(); i++) {
			String label = (String) categoryModel.getColumnKey(i);
			CategoryMarker marker = new CategoryMarker(label);
			marker.setLabel("");
			marker.setPaint(Color.cyan);
			marker.setOutlinePaint(Color.cyan);
			marker.setAlpha(0.1f);
			marker.setLabelAnchor(RectangleAnchor.TOP);
			marker.setLabelTextAnchor(TextAnchor.TOP_CENTER);
			marker.setLabelOffsetType(LengthAdjustmentType.CONTRACT);
			categoryPlot.addDomainMarker(marker, Layer.BACKGROUND);
		}
		renderer.setDrawBarOutline(false);
		renderer.setShadowVisible(false);
		renderer.setItemMargin(.1);
		renderer.setBarPainter(new StandardBarPainter());

	}
}
