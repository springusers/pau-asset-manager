/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.chart;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;

import javax.servlet.ServletContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;

import com.google.common.base.Function;
/**
 * This class is a factory of Functions that can generate temporal
 * resources as files in a temp folder, that are cleaned (removed)
 * when they are more than one day old 
 * 
 * @author Pau Carré Cardona
 */
public class ResourceImageGenerator implements Function<JFreeChart, String> {

	static Logger logger = LogManager.getLogger(ResourceImageGenerator.class);

	protected ResourceImageGenerator() {
	}

	@Override
	public String apply(JFreeChart jfchart) {
		String tempFileName = "";
		try {
			ServletContext servletContext = Sessions.getCurrent().getWebApp()
					.getServletContext();
			String tempDirPath = servletContext.getRealPath("")
					+ File.separator + "TMP";
			File tempDir = new File(tempDirPath);
			if (!tempDir.exists()) {
				tempDir.mkdir();
			} else {
				cleanTempFolder(tempDir);
			}
			File tempFile = File.createTempFile("tmp", ".png", tempDir);
			tempFileName = tempFile.getName();
			FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
			ChartUtilities
					.writeChartAsPNG(fileOutputStream, jfchart, 1200, 500);
		} catch (Throwable t) {
			logger.error("Error generating chart.", t);
		}
		Clients.clearBusy();
		return "TMP"+ File.separator  + tempFileName;
	}

	public static ResourceImageGenerator getFunction() {
		return new ResourceImageGenerator();
	}

	private static void cleanTempFolder(File directory) {
		Calendar cal = Calendar.getInstance();
		int daysBack = 1;
		cal.add(Calendar.DAY_OF_MONTH, daysBack * -1);
		long purgeTime = cal.getTimeInMillis();
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.lastModified() < purgeTime) {
				file.delete();
			}
		}
	}
}