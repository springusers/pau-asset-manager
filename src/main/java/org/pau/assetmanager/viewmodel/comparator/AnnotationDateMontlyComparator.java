/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.comparator;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Comparator;
import java.util.GregorianCalendar;

import org.pau.assetmanager.entities.Annotation;
import org.zkoss.zul.GroupComparator;
/**
 * This class is a comparator for annotations that uses
 * the month of the year as criteria
 * 
 * @author Pau Carré Cardona
 *
 */

public class AnnotationDateMontlyComparator implements Comparator<Annotation>,
		GroupComparator<Annotation>, Serializable {
	private static final long serialVersionUID = 1L;

	public int compare(Annotation annotationA, Annotation annotationB) {
		Calendar calendarA = GregorianCalendar.getInstance();
		calendarA.setTime(annotationA.getDate());
		Calendar calendarB = GregorianCalendar.getInstance();
		calendarB.setTime(annotationB.getDate());
		if (calendarA.get(Calendar.MONTH) == calendarB.get(Calendar.MONTH)
				&& calendarA.get(Calendar.YEAR) == calendarB.get(Calendar.YEAR)) {
			return 0;
		} else {
			return annotationA.getDate().compareTo(annotationB.getDate());
		}
	}

	public int compareGroup(Annotation o1, Annotation o2) {
		return compare(o1, o2);
	}
}
