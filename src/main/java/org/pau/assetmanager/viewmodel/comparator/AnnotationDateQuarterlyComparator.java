/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.comparator;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Comparator;
import java.util.GregorianCalendar;

import org.pau.assetmanager.entities.Annotation;
import org.zkoss.zul.GroupComparator;
/**
 * This class is a comparator for annotations that uses
 * the quarter (Q1, Q2, Q3 and Q4) as a comparison criteria.
 * 
 * @author Pau Carré Cardona
 *
 */

public class AnnotationDateQuarterlyComparator implements
		Comparator<Annotation>, GroupComparator<Annotation>, Serializable {
	private static final long serialVersionUID = 1L;

	public int compare(Annotation annotationA, Annotation annotationB) {
		Calendar calendarA = new GregorianCalendar();
		calendarA.setTime(annotationA.getDate());
		Calendar calendarB = new GregorianCalendar();
		calendarB.setTime(annotationB.getDate());
		Integer calendarAnumber = calendarA.get(Calendar.MONTH);
		Integer calendarBnumber = calendarB.get(Calendar.MONTH);
		if (calendarA.get(Calendar.YEAR) == calendarB.get(Calendar.YEAR)) {
			if (calendarAnumber >= 0 && calendarAnumber < 3
					&& calendarBnumber >= 0 && calendarBnumber < 3) {
				return 0;
			} else if (calendarAnumber >= 3 && calendarAnumber < 6
					&& calendarBnumber >= 3 && calendarBnumber < 6) {
				return 0;
			} else if (calendarAnumber >= 6 && calendarAnumber < 9
					&& calendarBnumber >= 6 && calendarBnumber < 9) {
				return 0;
			} else if (calendarAnumber >= 9 && calendarAnumber < 12
					&& calendarBnumber >= 9 && calendarBnumber < 12) {
				return 0;
			} else {
				return annotationA.getDate().compareTo(annotationB.getDate());
			}
		} else {
			return annotationA.getDate().compareTo(annotationB.getDate());
		}
	}

	public int compareGroup(Annotation o1, Annotation o2) {
		return compare(o1, o2);
	}
}
