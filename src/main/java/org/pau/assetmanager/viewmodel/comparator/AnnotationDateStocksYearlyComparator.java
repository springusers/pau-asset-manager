/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.comparator;

import java.io.Serializable;
import java.util.Comparator;

import org.pau.assetmanager.viewmodel.stocks.StockIncomeDescription;
import org.zkoss.zul.GroupComparator;
/**
 * This class is a comparator for stock annotations that uses the concept
 * (stock value) as criteria
 * 
 * @author Pau Carré Cardona
 *
 */

public class AnnotationDateStocksYearlyComparator implements Comparator<StockIncomeDescription>,
		GroupComparator<StockIncomeDescription>, Serializable {
	private static final long serialVersionUID = 1L;

	public int compare(StockIncomeDescription annotationA, StockIncomeDescription annotationB) {
		String bookAString = annotationA.getStockIncomeAnnotation().getConcept();
		String bookBString = annotationB.getStockIncomeAnnotation().getConcept();		
		return bookAString.compareTo(bookBString);
	}



	public int compareGroup(StockIncomeDescription o1, StockIncomeDescription o2) {
		return compare(o1, o2);
	}
}
