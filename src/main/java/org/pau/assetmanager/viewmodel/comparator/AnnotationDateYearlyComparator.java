/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.comparator;

import java.io.Serializable;
import java.util.Comparator;

import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.PropertyExpensesAnnotation;
import org.pau.assetmanager.entities.PropertyIncomeAnnotation;
import org.zkoss.zul.GroupComparator;
/**
 * This class is a comparator of annotations that uses the class 
 * name of the book concatenated with the property name or the 
 * book name in case it is not a property book
 * 
 * This comparator is used by the yearly tax report to group the conceptual
 * assets
 * 
 * @author Pau Carré Cardona
 *
 */

public class AnnotationDateYearlyComparator implements Comparator<Annotation>,
		GroupComparator<Annotation>, Serializable {
	private static final long serialVersionUID = 1L;

	public int compare(Annotation annotationA, Annotation annotationB) {
		String bookAString = getStringCodeForAnnotation(annotationA);
		String bookBString = getStringCodeForAnnotation(annotationB);		
		return bookAString.compareTo(bookBString);
	}

	private String getStringCodeForAnnotation(Annotation annotation){
		String bookString = annotation.getBook().getClass().getSimpleName();
		if (annotation instanceof PropertyExpensesAnnotation) {
			PropertyExpensesAnnotation expensesannotation = (PropertyExpensesAnnotation) annotation;
			bookString += "@"+expensesannotation.getPropertyBook().getProperty()
					.getName();
		}else if (annotation instanceof PropertyIncomeAnnotation) {
			PropertyIncomeAnnotation incomeAnnotation = (PropertyIncomeAnnotation) annotation;
			bookString += "@"
					+ incomeAnnotation.getPropertyBook().getProperty()
							.getName();
		} else {
			bookString += "@" + annotation.getBook().getName();
		}
		return bookString;
	}

	public int compareGroup(Annotation o1, Annotation o2) {
		return compare(o1, o2);
	}
}
