/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.converter;

import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.PropertyExpensesAnnotation;
import org.pau.assetmanager.entities.PropertyIncomeAnnotation;
import org.pau.assetmanager.viewmodel.utils.NumberFomatter;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zul.Label;
/**
 * This class is a read-only converter for the tax base which adds 
 * the VAT and the IRPF information
 * 
 * @author Pau Carré Cardona
 *
 */

public class BaseImponibleConverterQuarterly implements Converter<String, Annotation, Label> {


	@Override
	public String coerceToUi(Annotation annotation, Label label,
			BindContext bindContext) {
		if (annotation instanceof PropertyExpensesAnnotation) {
			PropertyExpensesAnnotation expensesAnnotation = (PropertyExpensesAnnotation) annotation;
			if (expensesAnnotation.getAppliesVAT()) {
				return NumberFomatter.formatMoney(expensesAnnotation
						.getBaseImponibleQuarterly())
						+ " ("
						+ NumberFomatter.formatPercentage(expensesAnnotation
								.getVat()) + "%  IVA )";
			} else {
				if (expensesAnnotation.getCommunity()) {
					return NumberFomatter.formatMoney(expensesAnnotation
							.getBaseImponibleQuarterly())+" ( Comunidad )";
				} else {
					return NumberFomatter.formatMoney(expensesAnnotation
							.getBaseImponibleQuarterly());
				}
			}
		} else if (annotation instanceof PropertyIncomeAnnotation) {
			PropertyIncomeAnnotation incomeAnnotation = (PropertyIncomeAnnotation) annotation;
			if (incomeAnnotation.getAppliesRetentionAndVAT()) {
				return NumberFomatter.formatMoney(incomeAnnotation
						.getBaseImponibleQuarterly())
						+ " ("
						+ NumberFomatter.formatPercentage(incomeAnnotation.getVat())
						+ "%  IVA, "
						+ NumberFomatter.formatPercentage(incomeAnnotation
								.getRetention()) + "% IRPF )";
			} else {
				return NumberFomatter.formatMoney(incomeAnnotation
						.getBaseImponibleQuarterly());
			}
		}
		return "";
	}

	@Override
	public Annotation coerceToBean(String arg0, Label arg1, BindContext arg2) {
		return null;
	}

}
