/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.converter;

import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.PropertyExpensesAnnotation;
import org.pau.assetmanager.entities.PropertyIncomeAnnotation;
import org.pau.assetmanager.viewmodel.utils.NumberFomatter;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zul.Label;
/**
 * This is a read-only converter for property-related annotations
 * that add detailed information for the yearly tax report
 * 
 * @author Pau Carré Cardona
 *
 */

public class DeclaredYearlyConverter implements
		Converter<String, Annotation, Label> {

	@Override
	public String coerceToUi(Annotation annotation, Label label,
			BindContext bindContext) {
		if (annotation instanceof PropertyExpensesAnnotation) {
			PropertyExpensesAnnotation expensesAnnotation = (PropertyExpensesAnnotation) annotation;
			String mainString = NumberFomatter.formatMoney(expensesAnnotation
					.getDeclaredYearly());
			mainString += " ("
					+ NumberFomatter.formatPercentage(expensesAnnotation
							.getDeductiblePercentage())
					+ "% de "
					+ NumberFomatter.formatMoney(expensesAnnotation
							.getBaseImponibleYearly()) + " )";
			return mainString;
		}else if (annotation instanceof PropertyIncomeAnnotation) {
			PropertyIncomeAnnotation propertyIncomeAnnotation = (PropertyIncomeAnnotation) annotation;
			String mainString = NumberFomatter.formatMoney(propertyIncomeAnnotation
					.getDeclaredYearly());
			return mainString;
		}
		return "";
	}

	@Override
	public Annotation coerceToBean(String arg0, Label arg1, BindContext arg2) {
		
		return null;
	}

}
