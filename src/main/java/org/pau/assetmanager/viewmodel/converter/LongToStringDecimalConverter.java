/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.converter;

import java.math.BigDecimal;

import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zul.Decimalbox;
/**
 * This class is read-write converter that transform long amounts
 * into double amounts by dividing by 100 and converting to double
 * the long amounts
 * 
 * @author Pau Carré Cardona
 *
 */

public class LongToStringDecimalConverter implements
		Converter<BigDecimal, Long, Decimalbox> {

	@Override
	public BigDecimal coerceToUi(Long decimalAsLong, Decimalbox label,
			BindContext bindContext) {
		if (decimalAsLong == null) {
			return new BigDecimal(0L);
		}
		BigDecimal bigDecimal = new BigDecimal(decimalAsLong);
		BigDecimal oneHundred = new BigDecimal(100L);
		BigDecimal finalValue = bigDecimal.divide(oneHundred);
		return finalValue;
	}

	@Override
	public Long coerceToBean(BigDecimal bigDecimal, Decimalbox decimalbox, BindContext arg2) {
		if (bigDecimal == null) {
			return 0L;
		}
		BigDecimal oneHundred = new BigDecimal(100L);
		BigDecimal finalValue = bigDecimal.multiply(oneHundred);
		return finalValue.longValue();
	}

}
