/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.converter;

import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.StockExpensesAnnotation;
import org.pau.assetmanager.entities.StockIncomeAnnotation;
import org.pau.assetmanager.viewmodel.utils.NumberFomatter;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zul.Label;
/**
 * This class is a read-only converter for stocks-related annotations
 * which generate a price per stock value
 * 
 * @author Pau Carré Cardona
 *
 */

public class PricePerStockConverter implements Converter<String, Annotation, Label> {


	@Override
	public String coerceToUi(Annotation annotation, Label label,
			BindContext bindContext) {
		Double price = 0.0;
		Double amount = 0.0;
		Double numberOfStocks = null;
		if (annotation instanceof StockIncomeAnnotation) {
			StockIncomeAnnotation stockIncomeAnnotation = (StockIncomeAnnotation) annotation;
			amount= stockIncomeAnnotation.getAmount();
			if(stockIncomeAnnotation.getNumberOfStocks() > 0){
				numberOfStocks = stockIncomeAnnotation.getNumberOfStocks().doubleValue();
			}
		} else if (annotation instanceof StockExpensesAnnotation) {
			StockExpensesAnnotation stockExpensesAnnotation = (StockExpensesAnnotation) annotation;
			amount= stockExpensesAnnotation.getAmount();
			if(stockExpensesAnnotation.getNumberOfStocks() > 0){
				numberOfStocks = stockExpensesAnnotation.getNumberOfStocks().doubleValue();
			}
		}
		if(numberOfStocks != null){
			price = amount / numberOfStocks;
		}
		return NumberFomatter.formatMoney(price);
	}

	@Override
	public Annotation coerceToBean(String arg0, Label arg1, BindContext arg2) {
		return null;
	}

}
