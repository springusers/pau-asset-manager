/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.converter;

import org.pau.assetmanager.viewmodel.type.PropertyType;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zk.ui.Component;

/**
 * This class is a converter for the radio panel to select the
 * property type
 * 
 * @author Pau Carré Cardona
 *
 */

public class PropertyTypeNameConverter implements Converter<String, PropertyType, Component> {

	public PropertyTypeNameConverter() {
	}

	@Override
	public PropertyType coerceToBean(String propertyLabel, Component component, BindContext bindContext) {
		return PropertyType.getPropertyTypeByLabel(propertyLabel);
	}

	@Override
	public String coerceToUi(PropertyType propertyType, Component component, BindContext bindContext) {
		return propertyType.getLabel();
	}

}
