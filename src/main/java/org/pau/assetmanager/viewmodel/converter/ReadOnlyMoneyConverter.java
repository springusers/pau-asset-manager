/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.converter;

import java.math.BigDecimal;
import java.text.NumberFormat;

import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.ExpensesAnnotation;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zul.Label;
/**
 * This class is a read-only converter to convert amounts from annotations
 * to formatted double money values
 * 
 * @author Pau Carré Cardona
 *
 */

public class ReadOnlyMoneyConverter implements
		Converter<String, Annotation, Label> {

	@Override
	public String coerceToUi(Annotation annotation, Label label,
			BindContext bindContext) {
		Long decimalAsLong = annotation.getTwoDecimalLongCodifiedAmount();
		if (decimalAsLong == null) {
			decimalAsLong = 0L;
		}
		BigDecimal bigDecimal = new BigDecimal(decimalAsLong);
		BigDecimal oneHundred = new BigDecimal(100L);
		BigDecimal finalValue = bigDecimal.divide(oneHundred);
		if(annotation instanceof ExpensesAnnotation){
			BigDecimal minusOne = new BigDecimal(-1L);
			finalValue = finalValue.multiply(minusOne);
		}
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		String finalValueFormatted = formatter.format(finalValue);
		return finalValueFormatted;
	}

	@Override
	public Annotation coerceToBean(String arg0, Label arg1, BindContext arg2) {
		
		return null;
	}

}
