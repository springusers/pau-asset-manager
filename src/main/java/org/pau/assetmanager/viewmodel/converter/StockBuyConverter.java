/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.converter;

import java.util.Iterator;
import java.util.Set;

import org.pau.assetmanager.viewmodel.stocks.StockIncomeDescription;
import org.pau.assetmanager.viewmodel.stocks.StocksBuySubtransaction;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zul.Label;
/**
 * @author Pau Carré Cardona
 *
 */

public class StockBuyConverter implements
		Converter<String, StockIncomeDescription, Label> {

	@Override
	public String coerceToUi(StockIncomeDescription stockIncomeDescription,
			Label label, BindContext bindContext) {
		StringBuffer stringBuffer = new StringBuffer();
		Set<StocksBuySubtransaction> stocksBuySubtransactionSet = stockIncomeDescription
				.getStocksBuySubtransactionSet();
		stringBuffer.append("(");
		for (Iterator<StocksBuySubtransaction> stocksBuySubtransactionIterator = stocksBuySubtransactionSet
				.iterator(); stocksBuySubtransactionIterator.hasNext();) {
			StocksBuySubtransaction stocksBuySubtransaction = stocksBuySubtransactionIterator.next();
			stringBuffer.append(stocksBuySubtransaction);
			if(stocksBuySubtransactionIterator.hasNext()){
				stringBuffer.append("; ");
			}
		}
		stringBuffer.append(")");
		return stringBuffer.toString();
	}

	@Override
	public StockIncomeDescription coerceToBean(String arg0, Label arg1,
			BindContext arg2) {
		
		return null;
	}

}
