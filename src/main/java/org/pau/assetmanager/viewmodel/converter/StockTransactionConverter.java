/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.converter;

import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.StockExpensesAnnotation;
import org.pau.assetmanager.entities.StockIncomeAnnotation;
import org.pau.assetmanager.viewmodel.stocks.StockIncomeDescription;
import org.pau.assetmanager.viewmodel.utils.NumberFomatter;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zul.Label;
/**
 * @author Pau Carré Cardona
 *
 */

public class StockTransactionConverter implements
		Converter<String, StockIncomeDescription, Label> {

	@Override
	public String coerceToUi(StockIncomeDescription stockIncomeDescription, Label label,
			BindContext bindContext) {
		Annotation annotation = stockIncomeDescription.getStockIncomeAnnotation();
		if (annotation instanceof StockExpensesAnnotation) {
			StockExpensesAnnotation expensesAnnotation = (StockExpensesAnnotation) annotation;
			return "Comprados " + expensesAnnotation.getNumberOfStocks()
					+ " valores por "
					+ NumberFomatter.formatMoney(expensesAnnotation.getAmount());
		} else if (annotation instanceof StockIncomeAnnotation) {
			StockIncomeAnnotation incomeAnnotation = (StockIncomeAnnotation) annotation;
			return "Vendidos " + incomeAnnotation.getNumberOfStocks()
					+ " valores por "
					+ NumberFomatter.formatMoney(incomeAnnotation.getAmount());
		}
		return "";
	}

	@Override
	public StockIncomeDescription coerceToBean(String arg0, Label arg1, BindContext arg2) {
		
		return null;
	}

}
