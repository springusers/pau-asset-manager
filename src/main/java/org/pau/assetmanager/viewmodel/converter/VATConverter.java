/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.converter;

import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.PropertyExpensesAnnotation;
import org.pau.assetmanager.entities.PropertyIncomeAnnotation;
import org.pau.assetmanager.viewmodel.utils.NumberFomatter;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zul.Label;
/**
 * @author Pau Carré Cardona
 *
 */

public class VATConverter implements Converter<String, Annotation, Label> {

	@Override
	public String coerceToUi(Annotation annotation, Label label,
			BindContext bindContext) {
		Boolean hasVAT = false;
		Double vat = 0.0;
		Double baseImponible = 0.0;
		if (annotation instanceof PropertyIncomeAnnotation) {
			PropertyIncomeAnnotation incomeAnnotation = (PropertyIncomeAnnotation) annotation;
			hasVAT = incomeAnnotation.getAppliesRetentionAndVAT();
			vat = incomeAnnotation.getVat();
			baseImponible = incomeAnnotation.getBaseImponibleQuarterly();
		} else if (annotation instanceof PropertyExpensesAnnotation) {
			PropertyExpensesAnnotation propertyExpensesAnnotation = (PropertyExpensesAnnotation) annotation;
			hasVAT = true;
			vat = propertyExpensesAnnotation.getVat();
			baseImponible = propertyExpensesAnnotation.getBaseImponibleQuarterly();
		}
		if (hasVAT) {
			return NumberFomatter.formatMoney(baseImponible * vat / 100.0) + " ("
					+ NumberFomatter.formatPercentage(vat) + "%)";
		} else {
			return "";
		}
	}

	@Override
	public Annotation coerceToBean(String arg0, Label arg1, BindContext arg2) {
		
		return null;
	}
}
