/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.grouping;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.ExpensesAnnotation;
import org.pau.assetmanager.entities.GeneralExpensesAnnotation;
import org.pau.assetmanager.entities.IncomeAnnotation;
import org.pau.assetmanager.entities.PropertyExpensesAnnotation;
import org.pau.assetmanager.entities.PropertyIncomeAnnotation;
import org.pau.assetmanager.entities.StockExpensesAnnotation;
import org.pau.assetmanager.entities.StockIncomeAnnotation;
import org.pau.assetmanager.viewmodel.utils.NumberFomatter;
import org.zkoss.zul.GroupsModelArray;
import org.zkoss.zul.ext.GroupsSortableModel;
/**
 * @author Pau Carré Cardona
 *
 */

public class AnnotationDateMonthlyGroupingModel extends
		GroupsModelArray<Annotation, String, String, Object> implements
		GroupsSortableModel<Annotation> {
	private static final long serialVersionUID = 1L;

	private boolean showGroup;

	public AnnotationDateMonthlyGroupingModel(List<Annotation> data,
			Comparator<Annotation> cmpr, boolean showGroup) {
		super(data.toArray(new Annotation[0]), cmpr);

		this.showGroup = showGroup;
	}

	protected String createGroupHead(Annotation[] groupdata, int index, int col) {
		String ret = "";
		if (groupdata.length > 0) {
			Calendar calendar = GregorianCalendar.getInstance();
			calendar.setTime(groupdata[0].getDate());
			ret = new SimpleDateFormat("MMMM").format(groupdata[0].getDate())
					+ " - " + calendar.get(Calendar.YEAR);
		}

		return ret;
	}

	protected String createGroupFoot(Annotation[] groupdata, int index, int col) {
		String result = "";
		Double total = 0.0;
		Double ingresosPropiedades = 0.0;
		Double ingresosGenerales = 0.0;
		Double ingresosValores = 0.0;
		Double gastosPropiedades = 0.0;
		Double gastosGenerales = 0.0;
		Double gastosValores = 0.0;
		for (Annotation annotation : groupdata) {
			total += annotation.getSignedAmount();
			if (annotation instanceof IncomeAnnotation) {
				if (annotation instanceof PropertyIncomeAnnotation) {
					ingresosPropiedades += annotation.getSignedAmount();
				} else if (annotation instanceof StockIncomeAnnotation) {
					ingresosValores += annotation.getSignedAmount(); 
				}else  {
					ingresosGenerales += annotation.getSignedAmount(); 
				}
			} else if (annotation instanceof ExpensesAnnotation) {
				if (annotation instanceof PropertyExpensesAnnotation) {
					gastosPropiedades += annotation.getSignedAmount();
				} else if (annotation instanceof StockExpensesAnnotation) {
					gastosValores += annotation.getSignedAmount(); 
				}else if (annotation instanceof GeneralExpensesAnnotation) {
					gastosGenerales += annotation.getSignedAmount(); 
				}
			}
		}
		if (gastosPropiedades != 0.0f || ingresosPropiedades != 0.0f) {
			result += "Propiedades\n";
			result += "\tTotal ingresos de propiedades : "
					+ NumberFomatter.formatMoney(ingresosPropiedades) + "\n";
			result += "\tTotal gastos de propiedades: "
					+ NumberFomatter.formatMoney(gastosPropiedades) + "\n";
			result += "\tTotal propiedades: "
					+ NumberFomatter.formatMoney(gastosPropiedades + ingresosPropiedades)
					+ "\n";
		}
		if (ingresosGenerales != 0.0f || gastosGenerales != 0.0f) {
			result += "Generales\n";
			result += "\tTotal ingresos generales : "
					+ NumberFomatter.formatMoney(ingresosGenerales) + "\n";
			result += "\tTotal gastos generales: "
					+ NumberFomatter.formatMoney(gastosGenerales) + "\n";
			result += "\tTotal generales: "
					+ NumberFomatter.formatMoney(gastosGenerales + ingresosGenerales)
					+ "\n";
		}
		if (ingresosValores != 0.0f || gastosValores != 0.0f) {
			result += "Valores\n";
			result += "\tTotal ingresos de valores : "
					+ NumberFomatter.formatMoney(ingresosValores) + "\n";
			result += "\tTotal gastos de valores: "
					+ NumberFomatter.formatMoney(gastosValores) + "\n";
			result += "\tTotal de valores: "
					+ NumberFomatter.formatMoney(gastosValores + ingresosValores)
					+ "\n";
		}
		result += "Total Mensual: " + NumberFomatter.formatMoney(total);
		return result;
	}

	public boolean hasGroupfoot(int groupIndex) {
		boolean retBool = false;

		if (showGroup) {
			retBool = super.hasGroupfoot(groupIndex);
		}

		return retBool;
	}
}
