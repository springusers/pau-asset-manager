/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.grouping;

import java.util.Calendar;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.viewmodel.utils.NumberFomatter;
import org.zkoss.zul.GroupsModelArray;
import org.zkoss.zul.ext.GroupsSortableModel;
/**
 * @author Pau Carré Cardona
 *
 */

public class AnnotationDateQuarterlyGroupingModel extends
		GroupsModelArray<Annotation, String, String, Object> implements
		GroupsSortableModel<Annotation> {
	private static final long serialVersionUID = 1L;

	private boolean showGroup;

	public AnnotationDateQuarterlyGroupingModel(List<Annotation> data,
			Comparator<Annotation> cmpr, boolean showGroup) {
		super(data.toArray(new Annotation[0]), cmpr);

		this.showGroup = showGroup;
	}

	protected String createGroupHead(Annotation[] groupdata, int index, int col) {
		if (groupdata.length > 0) {
			Calendar calendar = GregorianCalendar.getInstance();
			calendar.setTime(groupdata[0].getDate());
			Integer calendarAnumber = calendar.get(Calendar.MONTH);
			if (calendarAnumber >= 0 && calendarAnumber < 3) {
				return "Primer trimestre del " + calendar.get(Calendar.YEAR);
			} else if (calendarAnumber >= 3 && calendarAnumber < 6) {
				return "Segundo trimestre del " + calendar.get(Calendar.YEAR);
			} else if (calendarAnumber >= 6 && calendarAnumber < 9) {
				return "Tercer trimestre del " + calendar.get(Calendar.YEAR);
			} else if (calendarAnumber >= 9 && calendarAnumber < 12) {
				return "Cuarto trimestre del " + calendar.get(Calendar.YEAR);
			}
		}
		return "";
	}

	protected String createGroupFoot(Annotation[] groupdata, int index, int col) {
		QuarterlyHaciendaResults haciendaResults = QuarterlyHaciendaResults.compute(groupdata);
		String stringToReturn = "";
		stringToReturn += "Base Imponible de IVA soportado: "
				+ NumberFomatter.formatMoney(haciendaResults.getBaseImponibleSoportado()) + "\n";
		stringToReturn += "IVA soportado: "
				+ NumberFomatter.formatMoney(haciendaResults.getIvaSoportado()) + "\n";
		stringToReturn += "Base Imponible de IVA repercutido: "
				+ NumberFomatter.formatMoney(haciendaResults.getBaseImponibleRepercutido()) + "\n";
		stringToReturn += "IVA repercutido: "
				+ NumberFomatter.formatMoney(haciendaResults.getIvaRepercutido()) + "\n";
		stringToReturn += "Total IVA: "
				+ NumberFomatter.formatMoney(haciendaResults.getTotal());
		return stringToReturn;
	}


	public boolean hasGroupfoot(int groupIndex) {
		boolean retBool = false;

		if (showGroup) {
			retBool = super.hasGroupfoot(groupIndex);
		}

		return retBool;
	}
}
