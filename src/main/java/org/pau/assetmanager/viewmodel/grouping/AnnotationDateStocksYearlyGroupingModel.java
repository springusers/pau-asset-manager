/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.grouping;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import org.pau.assetmanager.business.TimeDependentConstantsBusiness;
import org.pau.assetmanager.viewmodel.stocks.StockConceptPerformance;
import org.pau.assetmanager.viewmodel.stocks.StockIncomeDescription;
import org.pau.assetmanager.viewmodel.stocks.StocksUtils;
import org.pau.assetmanager.viewmodel.type.TimeDependentConstantType;
import org.pau.assetmanager.viewmodel.utils.NumberFomatter;
import org.zkoss.zul.GroupsModelArray;
import org.zkoss.zul.ext.GroupsSortableModel;
/**
 * @author Pau Carré Cardona
 *
 */

public class AnnotationDateStocksYearlyGroupingModel extends
		GroupsModelArray<StockIncomeDescription, String, String, Object>
		implements GroupsSortableModel<StockIncomeDescription> {
	private static final long serialVersionUID = 1L;

	private boolean showGroup;

	public AnnotationDateStocksYearlyGroupingModel(
			List<StockIncomeDescription> data,
			Comparator<StockIncomeDescription> cmpr, boolean showGroup) {
		super(data.toArray(new StockIncomeDescription[0]), cmpr);
		this.showGroup = showGroup;
	}

	protected String createGroupHead(StockIncomeDescription[] groupdata,
			int index, int col) {
		if (groupdata.length > 0) {
			return groupdata[0].getStockIncomeAnnotation().getBook().getName();
		}
		return "";
	}

	protected String createGroupFoot(StockIncomeDescription[] groupdata,
			int index, int col) {
		
		
		
		StringBuffer resumen = new StringBuffer();
		StockConceptPerformance stockConceptPerformance = StocksUtils
				.getStockConceptPerformance(Arrays.asList(groupdata));
		
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(stockConceptPerformance.getLastDate());
		Integer year = calendar.get(Calendar.YEAR);
		
		Double percentageBelow6000 = TimeDependentConstantsBusiness.getTimeDependentConstantValueByYearAndTimeDependentConstantType(year, TimeDependentConstantType.PERCENTAGE_BELOW_6000).getNumericalValueAsDouble();
		Double percentageBetween6000and30000 = TimeDependentConstantsBusiness.getTimeDependentConstantValueByYearAndTimeDependentConstantType(year, TimeDependentConstantType.PERCENTAGE_BETWEEN_6000_AND_30000).getNumericalValueAsDouble();
		Double percentageAbove30000 = TimeDependentConstantsBusiness.getTimeDependentConstantValueByYearAndTimeDependentConstantType(year, TimeDependentConstantType.PERCENTAGE_ABOVE_30000).getNumericalValueAsDouble();
		
		Double percentageUntilJune = TimeDependentConstantsBusiness.getTimeDependentConstantValueByYearAndTimeDependentConstantType(year, TimeDependentConstantType.PERCENTAGE_FOR_JUNE).getNumericalValueAsDouble();
		Double percentageUntilNovember = TimeDependentConstantsBusiness.getTimeDependentConstantValueByYearAndTimeDependentConstantType(year, TimeDependentConstantType.PERCENTAGE_FOR_NOVEMBER).getNumericalValueAsDouble();
		
		Double gananciasEnBolsa = 0.0;
		Double porcentajeAplicado = 0.0;
		Double aTributar = 0.0;
		if (stockConceptPerformance.getProfit() > 0) {
			gananciasEnBolsa = stockConceptPerformance.getProfit();
			if (gananciasEnBolsa < 6000) {
				porcentajeAplicado = percentageBelow6000;
			} else if (gananciasEnBolsa < 30000) {
				porcentajeAplicado = percentageBetween6000and30000;
			} else {				
				porcentajeAplicado = percentageAbove30000;
			}
			aTributar = gananciasEnBolsa * porcentajeAplicado / 100.0;
		}
		resumen.append("Número de valores vendidos: "
				+ stockConceptPerformance.getNumberOfStocks() + "\n"
				+ "Beneficios en Bolsa: " + NumberFomatter.formatMoney(gananciasEnBolsa)
				+ "€\n" + "Porcentaje Aplicado: "
				+ NumberFomatter.formatPercentage(porcentajeAplicado) + "%\n"
				+ "Total A Tributar: " + NumberFomatter.formatMoney(aTributar) + "€\n"
				+ "\t A Tributar en Junio: " + NumberFomatter.formatMoney(aTributar * percentageUntilJune / 100.0)
				+ "€\n" + "\t A Tributar en Noviembre: "
				+ NumberFomatter.formatMoney(aTributar * percentageUntilNovember / 100.0) + "€\n");
		return resumen.toString();
	}

	public boolean hasGroupfoot(int groupIndex) {
		boolean retBool = false;

		if (showGroup) {
			retBool = super.hasGroupfoot(groupIndex);
		}

		return retBool;
	}
}
