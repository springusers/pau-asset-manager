/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.grouping;

import java.util.Comparator;
import java.util.List;

import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.viewmodel.utils.NumberFomatter;
import org.zkoss.zul.GroupsModelArray;
import org.zkoss.zul.ext.GroupsSortableModel;
/**
 * @author Pau Carré Cardona
 *
 */

public class AnnotationDateYearlyGroupingModel extends
		GroupsModelArray<Annotation, String, String, Object> implements
		GroupsSortableModel<Annotation> {
	private static final long serialVersionUID = 1L;

	private boolean showGroup;
	
	public AnnotationDateYearlyGroupingModel(List<Annotation> data,
			Comparator<Annotation> cmpr, boolean showGroup) {
		super(data.toArray(new Annotation[0]), cmpr);

		this.showGroup = showGroup;
	}

	protected String createGroupHead(Annotation[] groupdata, int index, int col) {
		if (groupdata.length > 0) {
			return groupdata[0].getBook().getName();
		}
		return "";
	}

	protected String createGroupFoot(Annotation[] groupdata, int index, int col) {
		
		YearlyHaciendaResults yearlyHaciendaResults = YearlyHaciendaResults.compute(groupdata);
		
		return "Gastos varios: " + NumberFomatter.formatMoney(yearlyHaciendaResults.getGastosVarios()) + "\n"
				+ "Ingresos: " + NumberFomatter.formatMoney(yearlyHaciendaResults.getIngresos()) + "\n"
				+ "Amortización: " + NumberFomatter.formatMoney(yearlyHaciendaResults.getAmortizacion()) + "\n"
				+ "Retenciones: " + NumberFomatter.formatMoney(yearlyHaciendaResults.getRetenciones()) + "\n"
				+ "Comunidad: " + NumberFomatter.formatMoney(yearlyHaciendaResults.getComunidad());

	}

	public boolean hasGroupfoot(int groupIndex) {
		boolean retBool = false;

		if (showGroup) {
			retBool = super.hasGroupfoot(groupIndex);
		}

		return retBool;
	}
}
