/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.grouping;

import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.PropertyExpensesAnnotation;
import org.pau.assetmanager.entities.PropertyIncomeAnnotation;
/**
 * @author Pau Carré Cardona
 *
 */

public class QuarterlyHaciendaResults {
	private Double baseImponibleSoportado = 0.0;
	private Double ivaSoportado = 0.0;
	private Double baseImponibleRepercutido = 0.0;
	private Double ivaRepercutido = 0.0;
	private Double total = 0.0;

	public QuarterlyHaciendaResults() {

	}

	public Double getBaseImponibleSoportado() {
		return baseImponibleSoportado;
	}

	public Double getBaseImponibleRepercutido() {
		return baseImponibleRepercutido;
	}

	public Double getIvaSoportado() {
		return ivaSoportado;
	}

	public Double getIvaRepercutido() {
		return ivaRepercutido;
	}

	public Double getTotal() {
		return total;
	}

	public static QuarterlyHaciendaResults compute(Annotation[] groupdata) {
		QuarterlyHaciendaResults haciendaResults = new QuarterlyHaciendaResults();
		for (Annotation annotation : groupdata) {
			if (annotation instanceof PropertyIncomeAnnotation) {
				PropertyIncomeAnnotation propertyIncomeAnnotation = (PropertyIncomeAnnotation) annotation;
				haciendaResults.baseImponibleRepercutido += propertyIncomeAnnotation.getBaseImponibleQuarterly();
				haciendaResults.ivaRepercutido += propertyIncomeAnnotation.getBaseImponibleQuarterly()
						* propertyIncomeAnnotation.getVat() / 100.0;
			} else if (annotation instanceof PropertyExpensesAnnotation) {
				PropertyExpensesAnnotation propertyExpensesAnnotation = (PropertyExpensesAnnotation) annotation;
				haciendaResults.baseImponibleSoportado += propertyExpensesAnnotation.getBaseImponibleQuarterly();
				haciendaResults.ivaSoportado += propertyExpensesAnnotation.getBaseImponibleQuarterly()
						* propertyExpensesAnnotation.getVat() / 100.0;
			}
		}
		haciendaResults.total = haciendaResults.ivaRepercutido
				- haciendaResults.ivaSoportado;
		return haciendaResults;
	}

}