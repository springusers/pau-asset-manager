/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.grouping;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.pau.assetmanager.business.TimeDependentConstantsBusiness;
import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.PropertyBook;
import org.pau.assetmanager.entities.PropertyExpensesAnnotation;
import org.pau.assetmanager.entities.PropertyIncomeAnnotation;
import org.pau.assetmanager.viewmodel.type.TimeDependentConstantType;

/**
 * @author Pau Carré Cardona
 * 
 */

public class YearlyHaciendaResults {
	private Double gastosVarios = 0.0;
	private Double ingresos = 0.0;
	private Double retenciones = 0.0;
	private Double amortizacion = 0.0;
	private Double comunidad = 0.0;

	public YearlyHaciendaResults() {

	}

	public YearlyHaciendaResults(Double gastosVarios, Double ingresos,
			Double retenciones, Double amortizacion, Double comunidad) {
		super();
		this.gastosVarios = gastosVarios;
		this.ingresos = ingresos;
		this.retenciones = retenciones;
		this.amortizacion = amortizacion;
		this.comunidad = comunidad;
	}

	public static YearlyHaciendaResults compute(Annotation[] groupdata) {
		Double gastosVarios = 0.0;
		Double ingresos = 0.0;
		Double retenciones = 0.0;
		Double amortizacion = 0.0;
		Double comunidad = 0.0;
		for (Annotation annotation : groupdata) {
			Date annotationDate = annotation.getDate();
			Calendar calendar = GregorianCalendar.getInstance();
			calendar.setTime(annotationDate);
			Integer year = calendar.get(Calendar.YEAR);
			Double percentageDeductible = TimeDependentConstantsBusiness
					.getTimeDependentConstantValueByYearAndTimeDependentConstantType(
							year,
							TimeDependentConstantType.PERCENTAGE_DEDUCTIBLE_FOR_PROPERTY_VALUE)
					.getNumericalValueAsDouble();

			if (amortizacion == 0.0f) {
				if (annotation.getBook() instanceof PropertyBook) {
					PropertyBook propertyBook = (PropertyBook) annotation
							.getBook();
					amortizacion = propertyBook.getProperty()
							.getOfficialValue() * percentageDeductible / 100.0;
				}
			}
			if (annotation instanceof PropertyIncomeAnnotation) {
				PropertyIncomeAnnotation incomeAnnotation = (PropertyIncomeAnnotation) annotation;
				ingresos += incomeAnnotation.getDeclaredYearly();
				retenciones += incomeAnnotation.getRetentionAmountYearly();
			} else if (annotation instanceof PropertyExpensesAnnotation) {
				PropertyExpensesAnnotation expensesAnnotation = (PropertyExpensesAnnotation) annotation;
				if (expensesAnnotation.getCommunity()) {
					comunidad += expensesAnnotation.getDeclaredYearly();
				} else {
					gastosVarios += expensesAnnotation.getDeclaredYearly();
				}

			}

		}

		return new YearlyHaciendaResults(gastosVarios, ingresos, retenciones,
				amortizacion, comunidad);
	}

	public Double getGastosVarios() {
		return gastosVarios;
	}

	public Double getIngresos() {
		return ingresos;
	}

	public Double getRetenciones() {
		return retenciones;
	}

	public Double getAmortizacion() {
		return amortizacion;
	}

	public Double getComunidad() {
		return comunidad;
	}

}