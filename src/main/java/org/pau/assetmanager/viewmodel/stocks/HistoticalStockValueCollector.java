/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.stocks;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Application Lifecycle Listener implementation class
 * HistoticalStockValueCollectior
 * 
 */
public class HistoticalStockValueCollector implements ServletContextListener {

	// collect historical stock values data every hour
	private static final Integer MILLISECONDS_COLLECTION_FREQUENCY_TIME = 1000 * 60 * 60;

	/**
	 * Default constructor.
	 */
	public HistoticalStockValueCollector() {
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent sce) {
		HistoticalStockValueCollectorRunner histoticalStockValueCollectorRunner = new HistoticalStockValueCollectorRunner();
		Thread thread = new Thread(histoticalStockValueCollectorRunner);
		thread.start();
	}

	private static class HistoticalStockValueCollectorRunner implements
			Runnable {
		@Override
		public void run() {
			while (true) {
//				HistoricalStocksValuesDownloader.updateStocksHistoricalValues();
				try {
					Thread.sleep(MILLISECONDS_COLLECTION_FREQUENCY_TIME);
				} catch (InterruptedException e) {
					// TODO: log errors
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent sce) {
	}

}
