/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.stocks;

import java.util.Date;

import org.apache.commons.lang.Validate;
import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.viewmodel.utils.NumberFomatter;

/**
 * This class contains the performance values results
 * that is used to make stock-related reports.
 * 
 * @author Pau Carré Cardona
 *
 */

public class StockConceptPerformance implements Comparable<StockConceptPerformance> {
	Double profit = 0.0;
	Double percentageOfStocks = 0.0;
	String concept = null;
	Long numberOfStocks = 0L;
	Date lastDate = null;
	Book stocksBook = null;

	public StockConceptPerformance(Double profit,
			Double percentageOfStocks, String concept,
			Long numberOfStocks, Date lastDate, Book stocksBook) {
		super();
		Validate.notNull(concept, "Concept cannot be null.");
		Validate.notNull(profit, "Profit cannot be null.");
		Validate.notNull(percentageOfStocks, "PercentageOfStocks cannot be null.");
		Validate.notNull(numberOfStocks, "NumberOfStocks cannot be null.");
		Validate.notNull(lastDate, "LastDate cannot be null.");
		Validate.notNull(stocksBook, "StocksBook cannot be null.");
		this.profit = profit;
		this.percentageOfStocks = percentageOfStocks;
		this.concept = concept;
		this.numberOfStocks = numberOfStocks;
		this.lastDate = lastDate;
		this.stocksBook = stocksBook;
	}

	public Double getProfit() {
		return profit;
	}

	public Double getPercentageOfStocks() {
		return percentageOfStocks;
	}

	public String getConcept() {
		return concept;
	}

	public Long getNumberOfStocks() {
		return numberOfStocks;
	}

	public Date getLastDate() {
		return lastDate;
	}

	public Book getStocksBook() {
		return stocksBook;
	}

	@Override
	public int compareTo(StockConceptPerformance stockConceptPerformance) {
		return concept.compareTo(
				stockConceptPerformance.getConcept());
	}

	@Override
	public String toString() {
		return concept + " ( "
				+ NumberFomatter.formatPercentage(percentageOfStocks) + "%  vendido )";
	}

}