/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.stocks;

import java.util.Set;

import org.pau.assetmanager.entities.StockIncomeAnnotation;
/**
 * This class has the sell stock transaction together
 * with the related subtransactions from different 
 * purchase transactions.
 * 
 * @author Pau Carré Cardona
 *
 */

public class StockIncomeDescription {

	private StockIncomeAnnotation stockIncomeAnnotation;
	private Set<StocksBuySubtransaction> stocksBuySubtransactionSet;

	public StockIncomeDescription(
			StockIncomeAnnotation stockIncomeAnnotation,
			Set<StocksBuySubtransaction> stocksBuySubtransactionSet) {
		super();
		this.stockIncomeAnnotation = stockIncomeAnnotation;
		this.stocksBuySubtransactionSet = stocksBuySubtransactionSet;
	}

	public StockIncomeAnnotation getStockIncomeAnnotation() {
		return stockIncomeAnnotation;
	}

	public Set<StocksBuySubtransaction> getStocksBuySubtransactionSet() {
		return stocksBuySubtransactionSet;
	}

}