/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.stocks;

import org.pau.assetmanager.business.HistoricalStockValuesBusiness;
import org.pau.assetmanager.entities.HistoricalStockValue;
import org.pau.assetmanager.utils.AssetManagerRuntimeException;

import com.google.common.base.Optional;

/**
 * @author pcarre
 * 
 */
public class StockState {

	private String symbol;
	private Double balance;
	private Long numberOfPurchasedStocks;
	private Integer month;
	private Integer year;

	public StockState(String symbol, Double balance,
			Long numberOfPurchasedStocks, Integer month, Integer year) {
		this.symbol = symbol;
		this.balance = balance;
		this.numberOfPurchasedStocks = numberOfPurchasedStocks;
		this.month = month;
		this.year = year;
		if (numberOfPurchasedStocks < 0) {
			throw new AssetManagerRuntimeException(
					"There are not enought stocks to sell");
		}
	}

	public String getSymbol() {
		return symbol;
	}

	public Double getBalance() {
		return balance;
	}

	public Double getVirtualBalanceForForcedYearAndMonth(Integer forcedYear, Integer forcedMonth) {
		// get the historical value for the current month and years
		Optional<HistoricalStockValue> optionalHistoricalStockValue = HistoricalStockValuesBusiness
				.getHistoricalValueForSymbolMonthAndYear(symbol, forcedMonth, forcedYear);
		if (!optionalHistoricalStockValue.isPresent()) {
			// try to get the last value stored in the database in case it is not found for the date
			optionalHistoricalStockValue = HistoricalStockValuesBusiness 
					.getHistoricalLastValueForSymbol(symbol);
		}
		if (optionalHistoricalStockValue.isPresent()) {
			Optional<Double> conversion = HistoricalStockValuesBusiness
					.converSymbolToEuro(symbol, optionalHistoricalStockValue
							.get().getValue());
			if (conversion.isPresent()) {
				return (numberOfPurchasedStocks * conversion.get());
			} else {
				return (numberOfPurchasedStocks * optionalHistoricalStockValue
						.get().getValue());
			}
		}
		return 0.0;
	}
	
	public Double getVirtualBalance() {
		// get the historical value for the current month and years
		Optional<HistoricalStockValue> optionalHistoricalStockValue = HistoricalStockValuesBusiness
				.getHistoricalValueForSymbolMonthAndYear(symbol, month, year);
		if (!optionalHistoricalStockValue.isPresent()) {
			// try to get the last value stored in the database in case it is not found for the date
			optionalHistoricalStockValue = HistoricalStockValuesBusiness 
					.getHistoricalLastValueForSymbol(symbol);
		}
		if (optionalHistoricalStockValue.isPresent()) {
			Optional<Double> conversion = HistoricalStockValuesBusiness
					.converSymbolToEuro(symbol, optionalHistoricalStockValue
							.get().getValue());
			if (conversion.isPresent()) {
				return (numberOfPurchasedStocks * conversion.get());
			} else {
				return (numberOfPurchasedStocks * optionalHistoricalStockValue
						.get().getValue());
			}
		}
		return 0.0;
	}

	public Long getNumberOfPurchasedStocks() {
		return numberOfPurchasedStocks;
	}

	public Integer getMonth() {
		return month;
	}

	public Integer getYear() {
		return year;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockState other = (StockState) obj;
		if (symbol == null) {
			if (other.symbol != null)
				return false;
		} else if (!symbol.equals(other.symbol))
			return false;
		return true;
	}

}