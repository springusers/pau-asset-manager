/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.stocks;

import java.text.SimpleDateFormat;

import org.pau.assetmanager.entities.StockExpensesAnnotation;
/**
 * This class defines a stock buy subtransaction with means
 * the amount of stocks in a purchase stock that is used to 
 * sell the stocks of the same value later on.
 * 
 * @author Pau Carré Cardona
 *
 */

public class StocksBuySubtransaction {
	StockExpensesAnnotation stockExpensesAnnotation;
	Long numberOfStocks;

	public StocksBuySubtransaction(
			StockExpensesAnnotation stockExpensesAnnotation,
			Long numberOfStocks) {
		super();
		this.stockExpensesAnnotation = stockExpensesAnnotation;
		this.numberOfStocks = numberOfStocks;
	}

	public Double getPricePerStock() {
		return stockExpensesAnnotation.getAmount()
				/ stockExpensesAnnotation.getNumberOfStocks().doubleValue();
	}

	public StocksBuySubtransaction(
			StockExpensesAnnotation stockExpensesAnnotation) {
		super();
		this.stockExpensesAnnotation = stockExpensesAnnotation;
		this.numberOfStocks = stockExpensesAnnotation.getNumberOfStocks();
	}

	public StockExpensesAnnotation getStockExpensesAnnotation() {
		return stockExpensesAnnotation;
	}

	public Long getNumberOfStocks() {
		return numberOfStocks;
	}

	@Override
	public String toString() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"dd/MM/yyyy");
		return numberOfStocks
				+ " acciones de la compra de "
				+ stockExpensesAnnotation.getNumberOfStocks()
				+ " acciones el "
				+ simpleDateFormat
						.format(stockExpensesAnnotation.getDate())
				+ " con un precio por acción de " + getPricePerStock()+"€";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((numberOfStocks == null) ? 0 : numberOfStocks.hashCode());
		result = prime
				* result
				+ ((stockExpensesAnnotation == null) ? 0
						: stockExpensesAnnotation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StocksBuySubtransaction other = (StocksBuySubtransaction) obj;
		if (numberOfStocks == null) {
			if (other.numberOfStocks != null)
				return false;
		} else if (!numberOfStocks.equals(other.numberOfStocks))
			return false;
		if (stockExpensesAnnotation == null) {
			if (other.stockExpensesAnnotation != null)
				return false;
		} else if (!stockExpensesAnnotation
				.equals(other.stockExpensesAnnotation))
			return false;
		return true;
	}

}