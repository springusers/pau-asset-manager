/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.stocks;

import java.util.Map;
import java.util.Set;


/**
 * This class represents the history of stock transactions
 * which contains the sold stocks and its related purchases
 * of stocks with the amount of stocks necessary to make the sell
 * transaction.
 * 
 * It also includes the inconsistencies that may happen when 
 * at a given time there are not enough purchased stocks to 
 * make the sell transaction
 * 
 * @author Pau Carré Cardona
 *
 */

public class StocksHistory {
	private Map<String, Set<StockIncomeDescription>> stocksHistoryMap;
	private Map<String, StocksIncomeError> stocksErrorsHistoryMap;

	public StocksHistory(
			Map<String, Set<StockIncomeDescription>> stocksHistoryMap,
			Map<String, StocksIncomeError> stocksErrorsHistoryMap) {
		super();
		this.stocksHistoryMap = stocksHistoryMap;
		this.stocksErrorsHistoryMap = stocksErrorsHistoryMap;
	}

	public Map<String, Set<StockIncomeDescription>> getStocksHistoryMap() {
		return stocksHistoryMap;
	}

	public Map<String, StocksIncomeError> getStocksErrorsHistoryMap() {
		return stocksErrorsHistoryMap;
	}

}