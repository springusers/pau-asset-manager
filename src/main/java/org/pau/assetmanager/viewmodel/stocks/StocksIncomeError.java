/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.stocks;

import java.text.SimpleDateFormat;
import java.util.Map;

import org.pau.assetmanager.entities.StockIncomeAnnotation;
/**
 * This class is a definition of an inconsistency in the sequence or
 * purchases and sells of stocks.
 * The error is when there is  a sell of a stock and at that point there
 * has not been enough purchases of stocks.
 * 
 * @author Pau Carré Cardona
 *
 */

public class StocksIncomeError {
	private StockIncomeAnnotation stockIncomeAnnotation;
	private Map<StocksBuySubtransaction, Long> stocksBuySubtransactionToStocksNeededMap;
	private Long neededStocks;

	public StocksIncomeError(
			StockIncomeAnnotation stockIncomeAnnotation,
			Map<StocksBuySubtransaction, Long> stocksBuySubtransactionToStocksNeededMap,
			Long neededStocks) {
		super();
		this.stockIncomeAnnotation = stockIncomeAnnotation;
		this.stocksBuySubtransactionToStocksNeededMap = stocksBuySubtransactionToStocksNeededMap;
		this.neededStocks = neededStocks;
	}

	public StockIncomeAnnotation getStockIncomeAnnotation() {
		return stockIncomeAnnotation;
	}

	public Map<StocksBuySubtransaction, Long> getStocksBuySubtransactionToStocksNeededMap() {
		return stocksBuySubtransactionToStocksNeededMap;
	}

	public Long getNeededStocks() {
		return neededStocks;
	}

	@Override
	public String toString() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return "La venta de " + stockIncomeAnnotation.getNumberOfStocks()
				+ " acciones el "
				+ simpleDateFormat.format(stockIncomeAnnotation.getDate())
				+ " no se puede realizar dado que faltan " + neededStocks
				+ " acciones en compras anteriores a la venta.";
	}

}