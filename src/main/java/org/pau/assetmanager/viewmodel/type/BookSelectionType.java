/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.type;

/**
 * This enum represents the types of book selection available:
 * -- ALL_BOOKS: all the books from a client
 * -- SINGLE_BOOK: a specific book from a client
 * 
 * @author Pau Carré Cardona
 */
public enum BookSelectionType {
	ALL_BOOKS("Todos los libros"), SINGLE_BOOK("Un sólo libro");

	String label;

	BookSelectionType(String label) {
		this.label = label;
	}

	public Boolean getIsAllBooks() {
		return this.equals(BookSelectionType.ALL_BOOKS);
	}

	public String getLabel() {
		return this.label;
	}

	public static BookSelectionType getBookSelectionTypeByLabel(String label) {
		for (BookSelectionType bookSelectionType : BookSelectionType
				.values()) {
			if (bookSelectionType.getLabel().equals(label)) {
				return bookSelectionType;
			}
		}
		return null;
	}
}