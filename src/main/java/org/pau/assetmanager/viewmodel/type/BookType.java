/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.type;

/**
 * This enum defines the available types of books
 * -- PROPERTY_BOOK: book with property-related incomes and expenses
 * -- GENERIC_BOOK: book with generic incomes and expenses
 * -- STOCKS_BOOK: book with stock-related incomes and expenses
 * 
 * @author Pau Carré Cardona
 */
public enum BookType {
	PROPERTY_BOOK("Propiedad"), GENERIC_BOOK("General"), STOCKS_BOOK(
			"Cartera de valores");

	String label;

	BookType(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return label;
	}

}