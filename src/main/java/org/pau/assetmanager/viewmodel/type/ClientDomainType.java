/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.type;

/**
 * This enum specifies the domain of the
 * annotations to be displayed in the monthly report.
 * The annotations can be all from a specific client
 * or from all the clients
 * 
 * @author Pau Carré Cardona
 */

public enum ClientDomainType {
	CURRENT_CLIENT("Cliente seleccionado"), ALL_CLIENTS("Todos los clientes");

	private String label;
	
	ClientDomainType(String label){
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
	
}