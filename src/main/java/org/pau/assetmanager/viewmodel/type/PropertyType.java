/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.pau.assetmanager.viewmodel.type;

/**
 * This enum represents the two types of household 
 * properties supported by the system.
 * 
 * The selection of the property type has huge implications
 * in the system as the Spanish fiscal system has different
 * treatment for both of them.
 * 
 * @author pcarre
 *
 */
public enum PropertyType {
	TENEMENT("Vivienda"), PREMISE("Local");

	/**
	 * textual definition in Spanish used in the user interface
	 */
	String label;

	PropertyType(String label) {
		this.label = label;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public static  PropertyType getPropertyTypeByLabel(String label){
		for(PropertyType propertyType : PropertyType.values()){
			if(propertyType.getLabel().equals(label)){
				return propertyType;
			}
		}
		return null;
	}

}