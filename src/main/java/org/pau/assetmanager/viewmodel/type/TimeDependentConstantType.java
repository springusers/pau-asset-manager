/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.type;

/**
 * This enum specifies the contants used that are defined in 
 * 'TimeDependentConstant'
 * 
 * @author Pau Carré Cardona
 */
public enum TimeDependentConstantType {
	PERCENTAGE_DEDUCTIBLE_FOR_PROPERTY_VALUE, 
	DEFAULT_VAT, 
	DEFAULT_RETENTION,
	PERCENTAGE_BELOW_6000,
	PERCENTAGE_BETWEEN_6000_AND_30000,
	PERCENTAGE_ABOVE_30000,
	PERCENTAGE_FOR_JUNE,
	PERCENTAGE_FOR_NOVEMBER;
}