/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.utils;

import java.util.Date;

/**
 * This class specifies the state of the grid filters for
 * annotations
 * 
 * @author Pau Carré Cardona
 *
 */

public class AnnotationsFilter {

	private String concept;
	private Date dateFrom;
	private Date dateTo;
	private Double costFrom;
	private Double costTo;
	private Double vatFrom;
	private Double vatTo;
	private Double retentionFrom;
	private Double retentionTo;
	private Boolean useQuarterly;
	private Boolean forCompany;
	private Boolean useYearly;
	private Boolean done;
	private Double deductiblePercentageFrom;
	private Double deductiblePercentageTo;
	private Boolean community;

	public static String ALL_CONCEPTS = "Todos";

	public static AnnotationsFilter emptyAnnotationsFilter(){
		return new AnnotationsFilter();
	}
	
	public Double getRetentionFrom() {
		return retentionFrom;
	}

	public void setRetentionFrom(Double retentionFrom) {
		this.retentionFrom = retentionFrom;
	}

	public Double getRetentionTo() {
		return retentionTo;
	}

	public void setRetentionTo(Double retentionTo) {
		this.retentionTo = retentionTo;
	}

	public Boolean getUseQuarterly() {
		return useQuarterly;
	}

	public void setUseQuarterly(Boolean useQuarterly) {
		this.useQuarterly = useQuarterly;
	}

	public AnnotationsFilter() {
		super();
		clear();
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public Double getCostFrom() {
		return costFrom;
	}

	public void setCostFrom(Double costFrom) {
		this.costFrom = costFrom;
	}

	public Double getCostTo() {
		return costTo;
	}

	public void setCostTo(Double costTo) {
		this.costTo = costTo;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public String getConcept() {
		return concept;
	}

	public void setConcept(String concept) {
		this.concept = concept;
	}

	public void clearDateAndCostFilter() {
		dateFrom = null;
		dateTo = null;
		costFrom = null;
		costTo = null;
		vatFrom = null;
		vatTo = null;
		useQuarterly = null;
		retentionFrom = null;
		retentionTo = null;
		forCompany = null;
		useYearly = null;
		done = null;
		deductiblePercentageFrom = null;
		deductiblePercentageTo = null;
		community = null;
	}

	public Boolean getCommunity() {
		return community;
	}

	public void setCommunity(Boolean community) {
		this.community = community;
	}

	public Boolean getForCompany() {
		return forCompany;
	}

	public void setForCompany(Boolean forCompany) {
		this.forCompany = forCompany;
	}

	public Double getVatFrom() {
		return vatFrom;
	}

	public void setVatFrom(Double vatFrom) {
		this.vatFrom = vatFrom;
	}

	public Double getVatTo() {
		return vatTo;
	}

	public void setVatTo(Double vatTo) {
		this.vatTo = vatTo;
	}

	public Boolean getUseYearly() {
		return useYearly;
	}

	public void setUseYearly(Boolean useYearly) {
		this.useYearly = useYearly;
	}

	public Boolean getDone() {
		return done;
	}

	public void setDone(Boolean done) {
		this.done = done;
	}

	public Double getDeductiblePercentageFrom() {
		return deductiblePercentageFrom;
	}

	public void setDeductiblePercentageFrom(Double deductiblePecentageFrom) {
		this.deductiblePercentageFrom = deductiblePecentageFrom;
	}

	public Double getDeductiblePercentageTo() {
		return deductiblePercentageTo;
	}

	public void setDeductiblePercentageTo(Double deductiblePecentageTo) {
		this.deductiblePercentageTo = deductiblePecentageTo;
	}

	public void clear() {
		concept = ALL_CONCEPTS;
		clearDateAndCostFilter();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((community == null) ? 0 : community.hashCode());
		result = prime * result + ((concept == null) ? 0 : concept.hashCode());
		result = prime * result
				+ ((costFrom == null) ? 0 : costFrom.hashCode());
		result = prime * result + ((costTo == null) ? 0 : costTo.hashCode());
		result = prime * result
				+ ((dateFrom == null) ? 0 : dateFrom.hashCode());
		result = prime * result + ((dateTo == null) ? 0 : dateTo.hashCode());
		result = prime
				* result
				+ ((deductiblePercentageFrom == null) ? 0
						: deductiblePercentageFrom.hashCode());
		result = prime
				* result
				+ ((deductiblePercentageTo == null) ? 0 : deductiblePercentageTo
						.hashCode());
		result = prime * result + ((done == null) ? 0 : done.hashCode());
		result = prime * result
				+ ((forCompany == null) ? 0 : forCompany.hashCode());
		result = prime * result
				+ ((retentionFrom == null) ? 0 : retentionFrom.hashCode());
		result = prime * result
				+ ((retentionTo == null) ? 0 : retentionTo.hashCode());
		result = prime * result
				+ ((useQuarterly == null) ? 0 : useQuarterly.hashCode());
		result = prime * result
				+ ((useYearly == null) ? 0 : useYearly.hashCode());
		result = prime * result + ((vatFrom == null) ? 0 : vatFrom.hashCode());
		result = prime * result + ((vatTo == null) ? 0 : vatTo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnnotationsFilter other = (AnnotationsFilter) obj;
		if (community == null) {
			if (other.community != null)
				return false;
		} else if (!community.equals(other.community))
			return false;
		if (concept == null) {
			if (other.concept != null)
				return false;
		} else if (!concept.equals(other.concept))
			return false;
		if (costFrom == null) {
			if (other.costFrom != null)
				return false;
		} else if (!costFrom.equals(other.costFrom))
			return false;
		if (costTo == null) {
			if (other.costTo != null)
				return false;
		} else if (!costTo.equals(other.costTo))
			return false;
		if (dateFrom == null) {
			if (other.dateFrom != null)
				return false;
		} else if (!dateFrom.equals(other.dateFrom))
			return false;
		if (dateTo == null) {
			if (other.dateTo != null)
				return false;
		} else if (!dateTo.equals(other.dateTo))
			return false;
		if (deductiblePercentageFrom == null) {
			if (other.deductiblePercentageFrom != null)
				return false;
		} else if (!deductiblePercentageFrom
				.equals(other.deductiblePercentageFrom))
			return false;
		if (deductiblePercentageTo == null) {
			if (other.deductiblePercentageTo != null)
				return false;
		} else if (!deductiblePercentageTo.equals(other.deductiblePercentageTo))
			return false;
		if (done == null) {
			if (other.done != null)
				return false;
		} else if (!done.equals(other.done))
			return false;
		if (forCompany == null) {
			if (other.forCompany != null)
				return false;
		} else if (!forCompany.equals(other.forCompany))
			return false;
		if (retentionFrom == null) {
			if (other.retentionFrom != null)
				return false;
		} else if (!retentionFrom.equals(other.retentionFrom))
			return false;
		if (retentionTo == null) {
			if (other.retentionTo != null)
				return false;
		} else if (!retentionTo.equals(other.retentionTo))
			return false;
		if (useQuarterly == null) {
			if (other.useQuarterly != null)
				return false;
		} else if (!useQuarterly.equals(other.useQuarterly))
			return false;
		if (useYearly == null) {
			if (other.useYearly != null)
				return false;
		} else if (!useYearly.equals(other.useYearly))
			return false;
		if (vatFrom == null) {
			if (other.vatFrom != null)
				return false;
		} else if (!vatFrom.equals(other.vatFrom))
			return false;
		if (vatTo == null) {
			if (other.vatTo != null)
				return false;
		} else if (!vatTo.equals(other.vatTo))
			return false;
		return true;
	}


}
