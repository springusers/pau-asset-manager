/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.utils;

import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.viewmodel.type.BookSelectionType;

/**
 * This class represents a book selection
 * A selection can be a book of a client 
 * of all the books from a client
 * 
 * @author Pau Carré Cardona
 */

public class BookSelection {
	private Book selectedBook = null;
	private BookSelectionType bookSelectionType = null;
	private Client selectedClient = null;

	
	public static BookSelection fromBook(Book selectedBook){
		return new BookSelection(selectedBook, BookSelectionType.SINGLE_BOOK, selectedBook.getClient());
	}
	
	public static BookSelection fromClient(Client client){
		return new BookSelection(null, BookSelectionType.ALL_BOOKS, client);
	}
	
	public BookSelection(Book selectedBook,
			BookSelectionType bookSelectionType, Client selectedClient) {
		super();
		this.selectedBook = selectedBook;
		this.bookSelectionType = bookSelectionType;
		this.selectedClient = selectedClient;
	}

	
	public Boolean isAllBooks(){
		return bookSelectionType.equals(BookSelectionType.ALL_BOOKS);
	}
	
	public Book getSelectedBook() {
		return selectedBook;
	}

	public BookSelectionType getBookSelectionType() {
		return bookSelectionType;
	}

	public Client getSelectedClient() {
		return selectedClient;
	}

}
