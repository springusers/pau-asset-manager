/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.utils;

import java.text.DecimalFormat;

/**
 * This factory class contains the constants and the 
 * methods to deal with money and percentage formats
 * for numbers
 * 
 * @author Pau Carré Cardona
 */

public class NumberFomatter {
	
	public static final DecimalFormat MONEY_FORMATTER = new DecimalFormat("###,###.##€");
	public static final DecimalFormat PERCENTAGE_FORMATTER = new DecimalFormat("###.##");
	
	public static String formatMoney(Double number) {
		return MONEY_FORMATTER.format(number);
	}

	public static String formatPercentage(Double number) {
		return PERCENTAGE_FORMATTER.format(number);
	}

	
}
