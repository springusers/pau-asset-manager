/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.viewmodel.utils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Helper class that holds the map: 'annotation type label' --> 'time label' --> 'amount'
 * 
 * @author pcarre
 *
 */
public class ProperitesChartDataModel {
	private Map<String, Map<String, Double>> chartData = new LinkedHashMap<String, Map<String, Double>>();

	public ProperitesChartDataModel() {

	}

	public Map<String, Map<String, Double>> getChartData() {
		return chartData;
	}

}