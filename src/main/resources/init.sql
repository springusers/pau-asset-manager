-- This file is part of Pau's Asset Manager Project.

-- Pau's Asset Manager Project is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- Pau's Asset Manager Project is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.

-- @author Pau Carré Cardona	


insert into User(id, username, password) values (1, 'admin', 'admin');
insert into Groups(group_code, username) values ('USER','admin');
insert into Groups(group_code, username) values ('ADMINISTRATOR','admin');

insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Otros','GeneralExpensesAnnotation', true);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Otros','PropertyExpensesAnnotation', true);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Movimiento','MovementExpensesAnnotation', true);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Desconocido','StockExpensesAnnotation', true);

insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Otros','GeneralIncomeAnnotation', true);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Otros','PropertyIncomeAnnotation', true);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Movimiento','MovementIncomeAnnotation', true);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Desconocido','StockIncomeAnnotation', true);

insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Reparaciones','PropertyExpensesAnnotation', false);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Electricidad','PropertyExpensesAnnotation', false);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Alquiler','PropertyIncomeAnnotation', false);

insert into TimeDependentConstant(id, name, code) values (1, "Porcentaje deducible por valor de la propiedad", "PERCENTAGE_DEDUCTIBLE_FOR_PROPERTY_VALUE");
insert into TimeDependentConstant(id, name, code) values (2, "Porcentaje de I.V.A. por defecto", "DEFAULT_VAT");
insert into TimeDependentConstant(id, name, code) values (3, "Porcentaje de I.R.P.F. por defecto", "DEFAULT_RETENTION");

insert into TimeDependentConstant(id, name, code) values (4, "Porcentaje aplicado a los beneficios en bolsa por debajo de 6.000€", "PERCENTAGE_BELOW_6000");
insert into TimeDependentConstant(id, name, code) values (5, "Porcentaje aplicado a los beneficios en bolsa entre 6.000€ y 30.000€", "PERCENTAGE_BETWEEN_6000_AND_30000");
insert into TimeDependentConstant(id, name, code) values (6, "Porcentaje aplicado a los beneficios en bolsa superiores a 30.000€", "PERCENTAGE_ABOVE_30000");
insert into TimeDependentConstant(id, name, code) values (7, "Porcentaje a tributar en Junio para beneficios en bolsa", "PERCENTAGE_FOR_JUNE");
insert into TimeDependentConstant(id, name, code) values (8, "Porcentaje a tributar en Noviembre para beneficios en bolsa", "PERCENTAGE_FOR_NOVEMBER");
	

insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (1, 2012,  300, 1);
insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (2, 2012, 2100, 2);
insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (3, 2012, 2100, 3);


insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (4, 2012, 2100, 4);
insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (5, 2012, 2500, 5);
insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (6, 2012, 2700, 6);
insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (7, 2012, 6000, 7);
insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (8, 2012, 4000, 8);
