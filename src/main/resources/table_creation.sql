-- This file is part of Pau's Asset Manager Project.

-- Pau's Asset Manager Project is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- Pau's Asset Manager Project is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.

-- @author Pau Carré Cardona	

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- CREATE SCHEMA IF NOT EXISTS `XXXXX` DEFAULT CHARACTER SET utf8 ;
-- USE `XXXX` ;

-- -----------------------------------------------------
-- Table `PredefinedConcept`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PredefinedConcept` ;

CREATE  TABLE IF NOT EXISTS `PredefinedConcept` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(100)  NOT NULL ,
  `annotation_class_name` VARCHAR(45)  NOT NULL , 
  `is_default` TINYINT(1) NOT NULL ,  
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `Client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Client` ;

CREATE  TABLE IF NOT EXISTS `Client` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `additionalInformation` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `Book`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Book` ;

CREATE  TABLE IF NOT EXISTS `Book` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255)  NOT NULL ,
  `description` VARCHAR(1024)  NULL DEFAULT NULL ,
  `client` BIGINT(32) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Book_Client1` (`client` ASC) ,
  CONSTRAINT `fk_Book_Client1`
    FOREIGN KEY (`client` )
    REFERENCES `Client` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `Annotation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Annotation` ;

CREATE  TABLE IF NOT EXISTS `Annotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `book` BIGINT(32) NOT NULL ,
  `amount` BIGINT(32) NOT NULL ,
  `date` DATE NOT NULL ,
  `concept` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Annotation_Property1_idx` (`book` ASC) ,
  CONSTRAINT `fk_Annotation_Property1`
    FOREIGN KEY (`book` )
    REFERENCES `Book` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `Groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Groups` ;

CREATE  TABLE IF NOT EXISTS `Groups` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `group_code` VARCHAR(45)  NOT NULL ,
  `username` VARCHAR(45)  NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `User` ;

CREATE  TABLE IF NOT EXISTS `User` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(45)  NOT NULL ,
  `password` VARCHAR(45)  NOT NULL ,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_username` (`username`)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `ExpensesAnnotation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ExpensesAnnotation` ;

CREATE  TABLE IF NOT EXISTS `ExpensesAnnotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_ExpensesAnnotation_Annotation1`
    FOREIGN KEY (`id` )
    REFERENCES `Annotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `IncomeAnnotation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IncomeAnnotation` ;

CREATE  TABLE IF NOT EXISTS `IncomeAnnotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `done` TINYINT(1) NOT NULL ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_IncomeAnnotation_Annotation1`
    FOREIGN KEY (`id` )
    REFERENCES `Annotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `StockExpensesAnnotation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StockExpensesAnnotation` ;

CREATE  TABLE IF NOT EXISTS `StockExpensesAnnotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `number_of_stocks` BIGINT(32) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_StockExpensesAnnotation_ExpensesAnnotation1` (`id` ASC) ,
  CONSTRAINT `fk_StockExpensesAnnotation_ExpensesAnnotation1`
    FOREIGN KEY (`id` )
    REFERENCES `ExpensesAnnotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `StockIncomeAnnotation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StockIncomeAnnotation` ;

CREATE  TABLE IF NOT EXISTS `StockIncomeAnnotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `number_of_stocks` BIGINT(32) NOT NULL ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_StockIncomeAnnotation_IncomeAnnotation1`
    FOREIGN KEY (`id` )
    REFERENCES `IncomeAnnotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `PropertyIncomeAnnotation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PropertyIncomeAnnotation` ;

CREATE  TABLE IF NOT EXISTS `PropertyIncomeAnnotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `retention` BIGINT(32) NOT NULL ,
  `for_company` TINYINT(1) NULL ,
  `vat` BIGINT(32) NOT NULL ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_PropertyIncomeAnnotation_IncomeAnnotation1`
    FOREIGN KEY (`id` )
    REFERENCES `IncomeAnnotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `PropertyExpensesAnnotation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PropertyExpensesAnnotation` ;

CREATE  TABLE IF NOT EXISTS `PropertyExpensesAnnotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `use_quarterly` TINYINT(1) NOT NULL ,
  `community` TINYINT(1) NOT NULL ,
  `deductible_percentage` BIGINT(32) NOT NULL ,
  `vat` BIGINT(32) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_PropertyExpensesAnnotation_ExpensesAnnotation2` (`id` ASC) ,
  CONSTRAINT `fk_PropertyExpensesAnnotation_ExpensesAnnotation2`
    FOREIGN KEY (`id` )
    REFERENCES `ExpensesAnnotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `Property`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Property` ;

CREATE  TABLE IF NOT EXISTS `Property` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(128) NOT NULL ,
  `client` BIGINT(32) NOT NULL ,
  `type` VARCHAR(45) NOT NULL ,
  `official_value` BIGINT(32) NOT NULL ,
  `actual_value` BIGINT(32) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE KEY `unique_name_client` (`name`, `client`),
  INDEX `fk_Property_Client1` (`client` ASC) ,
  CONSTRAINT `fk_Property_Client1`
    FOREIGN KEY (`client` )
    REFERENCES `Client` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `PropertyBook`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PropertyBook` ;

CREATE  TABLE IF NOT EXISTS `PropertyBook` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `property` BIGINT(32) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_PropertyBook_Property1` (`property` ASC) ,
  CONSTRAINT `fk_PropertyBook_Book1`
    FOREIGN KEY (`id` )
    REFERENCES `Book` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PropertyBook_Property1`
    FOREIGN KEY (`property` )
    REFERENCES `Property` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `StocksBook`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `StocksBook` ;

CREATE  TABLE IF NOT EXISTS `StocksBook` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_StocksBook_Book1`
    FOREIGN KEY (`id` )
    REFERENCES `Book` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `UserClientAssociation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UserClientAssociation` ;

CREATE  TABLE IF NOT EXISTS `UserClientAssociation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `user` BIGINT(32) NOT NULL ,
  `client` BIGINT(32) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE KEY `unique_user_client` (`user`,`client`),
  INDEX `fk_UserClientAssociation_User1` (`user` ASC) ,
  INDEX `fk_UserClientAssociation_Client1` (`client` ASC) ,
  CONSTRAINT `fk_UserClientAssociation_User1`
    FOREIGN KEY (`user` )
    REFERENCES `User` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_UserClientAssociation_Client1`
    FOREIGN KEY (`client` )
    REFERENCES `Client` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `GroupUserAssociation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `GroupUserAssociation` ;

CREATE  TABLE IF NOT EXISTS `GroupUserAssociation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `group` BIGINT(32) NOT NULL ,
  `user` BIGINT(32) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE KEY `unique_group_user` (`group`,`user`),
  INDEX `fk_table1_Groups1` (`group` ASC) ,
  INDEX `fk_table1_User1` (`user` ASC) ,
  CONSTRAINT `fk_table1_Groups1`
    FOREIGN KEY (`group` )
    REFERENCES `Groups` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_table1_User1`
    FOREIGN KEY (`user` )
    REFERENCES `User` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `MovementIncomeAnnotation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MovementIncomeAnnotation` ;

CREATE  TABLE IF NOT EXISTS `MovementIncomeAnnotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_StocksDeposit_IncomeAnnotation1`
    FOREIGN KEY (`id` )
    REFERENCES `IncomeAnnotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `MovementExpensesAnnotation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MovementExpensesAnnotation` ;

CREATE  TABLE IF NOT EXISTS `MovementExpensesAnnotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `movementIncomeAnnotation` BIGINT(32) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_MovementExpensesAnnotation_MovementIncomeAnnotation1` (`movementIncomeAnnotation` ASC) ,
  CONSTRAINT `fk_MovementExpensesAnnotation_ExpensesAnnotation1`
    FOREIGN KEY (`id` )
    REFERENCES `ExpensesAnnotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_MovementExpensesAnnotation_MovementIncomeAnnotation1`
    FOREIGN KEY (`movementIncomeAnnotation` )
    REFERENCES `MovementIncomeAnnotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `GeneralExpensesAnnotation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `GeneralExpensesAnnotation` ;

CREATE  TABLE IF NOT EXISTS `GeneralExpensesAnnotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_GeneralExpensesAnnotation_ExpensesAnnotation1` (`id` ASC) ,
  CONSTRAINT `fk_GeneralExpensesAnnotation_ExpensesAnnotation1`
    FOREIGN KEY (`id` )
    REFERENCES `ExpensesAnnotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `GeneralIncomeAnnotation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `GeneralIncomeAnnotation` ;

CREATE  TABLE IF NOT EXISTS `GeneralIncomeAnnotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_GeneralIncomeAnnotation_IncomeAnnotation1`
    FOREIGN KEY (`id` )
    REFERENCES `IncomeAnnotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `GeneralBook`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `GeneralBook` ;

CREATE  TABLE IF NOT EXISTS `GeneralBook` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_GeneralBook_Book1`
    FOREIGN KEY (`id` )
    REFERENCES `Book` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `TimeDependentConstant`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `TimeDependentConstant` ;

CREATE  TABLE IF NOT EXISTS `TimeDependentConstant` (
  `id`  BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `code` VARCHAR(128) NOT NULL ,
  `name` VARCHAR(128) NOT NULL ,
  PRIMARY KEY (`id`),
   UNIQUE KEY `unique_code` (`code`),
   UNIQUE KEY `unique_name` (`name`)
   )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `TimeDependentConstantValue`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `TimeDependentConstantValue` ;

CREATE  TABLE IF NOT EXISTS `TimeDependentConstantValue` (
  `id`  BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `year` INT NULL default NULL,
  `numerical_value` BIGINT(32) NOT NULL , 
  `time_dependent_constant` BIGINT(32) NOT NULL , 
  PRIMARY KEY (`id`),
  INDEX `fk_TimeDependentConstants_TimeDependentConstant1` (`time_dependent_constant` ASC) ,
  UNIQUE KEY `unique_year_time_dependent_constant` (`year`,`time_dependent_constant`),
  CONSTRAINT `fk_TimeDependentConstants_TimeDependentConstant1`
    FOREIGN KEY (`time_dependent_constant` )
    REFERENCES `TimeDependentConstant` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

-- -----------------------------------------------------
-- Table `HistoricalStockValue`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `HistoricalStockValue` ;

CREATE  TABLE IF NOT EXISTS `HistoricalStockValue` (
  `id`  BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `year` INT NOT NULL,
  `month` INT NOT NULL,
  `symbol` VARCHAR(128) NOT NULL ,
  `numerical_value` BIGINT(32) NOT NULL , 
  `collection_date` DATE NOT NULL , 
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_HistoricalStockValue` (`year`, `month`, `symbol`)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
