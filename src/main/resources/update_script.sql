-- ALTER SCHEMA `XXXX`  DEFAULT CHARACTER SET utf8  DEFAULT COLLATE utf8_general_ci ;
update PropertyExpensesAnnotation set community = 0 where community is null;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';
ALTER TABLE `Annotation` DROP FOREIGN KEY `fk_Annotation_Property1` , DROP FOREIGN KEY `fk_Annotation_Concept1` ;
ALTER TABLE `Book` DROP FOREIGN KEY `fk_Book_Client1` ;
ALTER TABLE `ExpensesAnnotation` DROP FOREIGN KEY `fk_ExpensesAnnotation_Annotation1` ;
ALTER TABLE `GroupUserAssociation` DROP FOREIGN KEY `fk_table1_Groups1` , DROP FOREIGN KEY `fk_table1_User1` ;
ALTER TABLE `IncomeAnnotation` DROP FOREIGN KEY `fk_IncomeAnnotation_Annotation1` ;
ALTER TABLE `MovementExpensesAnnotation` DROP FOREIGN KEY `fk_MovementExpensesAnnotation_ExpensesAnnotation1` , DROP FOREIGN KEY `fk_MovementExpensesAnnotation_MovementIncomeAnnotation1` ;
ALTER TABLE `MovementIncomeAnnotation` DROP FOREIGN KEY `fk_StocksDeposit_IncomeAnnotation1` ;
ALTER TABLE `Property` DROP FOREIGN KEY `fk_Property_Client1` ;
ALTER TABLE `PropertyBook` DROP FOREIGN KEY `fk_PropertyBook_Book1` , DROP FOREIGN KEY `fk_PropertyBook_Property1` ;
ALTER TABLE `PropertyExpensesAnnotation` DROP FOREIGN KEY `fk_PropertyExpensesAnnotation_ExpensesAnnotation2` ;
ALTER TABLE `PropertyIncomeAnnotation` DROP FOREIGN KEY `fk_PropertyIncomeAnnotation_IncomeAnnotation1` ;
ALTER TABLE `StockExpensesAnnotation` DROP FOREIGN KEY `fk_StockExpensesAnnotation_ExpensesAnnotation1` ;
ALTER TABLE `StockIncomeAnnotation` DROP FOREIGN KEY `fk_StockIncomeAnnotation_IncomeAnnotation1` ;
ALTER TABLE `StocksBook` DROP FOREIGN KEY `fk_StocksBook_Book1` ;
ALTER TABLE `UserClientAssociation` DROP FOREIGN KEY `fk_UserClientAssociation_User1` , DROP FOREIGN KEY `fk_UserClientAssociation_Client1` ;
ALTER TABLE `Annotation` CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `book` `book` BIGINT(32) NOT NULL  , CHANGE COLUMN `amount` `amount` BIGINT(32) NOT NULL ;
ALTER TABLE `Book` CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `client` `client` BIGINT(32) NOT NULL;
ALTER TABLE `Client` CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci , CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  ;
ALTER TABLE `ExpensesAnnotation` CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT;
ALTER TABLE `GroupUserAssociation` CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci , CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `group` `group` BIGINT(32) NOT NULL  , CHANGE COLUMN `user` `user` BIGINT(32) NOT NULL;
ALTER TABLE `Groups` CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  ;
ALTER TABLE `IncomeAnnotation` CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT;
ALTER TABLE `MovementExpensesAnnotation` CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci , CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `movementIncomeAnnotation` `movementIncomeAnnotation` BIGINT(32) NOT NULL;
ALTER TABLE `MovementIncomeAnnotation` CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci , CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Property` CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci , CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `client` `client` BIGINT(32) NOT NULL  , CHANGE COLUMN `official_value` `official_value` BIGINT(32) NOT NULL  , CHANGE COLUMN `actual_value` `actual_value` BIGINT(32) NOT NULL;
ALTER TABLE `PropertyBook` CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci , CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `property` `property` BIGINT(32) NOT NULL;
ALTER TABLE `PropertyExpensesAnnotation` CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `use_quarterly` `use_quarterly` TINYINT(1) NOT NULL  , CHANGE COLUMN `community` `community` TINYINT(1) NOT NULL  , CHANGE COLUMN `deductible_percentage` `deductible_percentage` BIGINT(32) NOT NULL  , CHANGE COLUMN `vat` `vat` BIGINT(32) NOT NULL;
ALTER TABLE `PropertyIncomeAnnotation` CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `retention` `retention` BIGINT(32) NOT NULL  , CHANGE COLUMN `vat` `vat` BIGINT(32) NOT NULL;
ALTER TABLE `StockExpensesAnnotation` CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci , CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `number_of_stocks` `number_of_stocks` BIGINT(32) NOT NULL;
ALTER TABLE `StockIncomeAnnotation` CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci , CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `number_of_stocks` `number_of_stocks` BIGINT(32) NOT NULL;
ALTER TABLE `StocksBook` CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci , CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `value` `value` BIGINT(32) NOT NULL ; 
ALTER TABLE `User` CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT , ADD UNIQUE INDEX `unique_username` (`username` ASC) ;
ALTER TABLE `UserClientAssociation` CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci , CHANGE COLUMN `id` `id` BIGINT(32) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `user` `user` BIGINT(32) NOT NULL  , CHANGE COLUMN `client` `client` BIGINT(32) NOT NULL;
ALTER TABLE `Client` CHANGE COLUMN `name` `name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL  , CHANGE COLUMN `additionalInformation` `additionalInformation` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ;
ALTER TABLE `Property` CHANGE COLUMN `name` `name` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL  , CHANGE COLUMN `type` `type` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL  ;
ALTER TABLE `Annotation`  ADD CONSTRAINT `fk_Annotation_Property1`
  FOREIGN KEY (`book` )
  REFERENCES `Book` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, DROP INDEX `fk_Annotation_Concept1_idx` ;
ALTER TABLE `Book`   ADD CONSTRAINT `fk_Book_Client1`
  FOREIGN KEY (`client` )
  REFERENCES `Client` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;
ALTER TABLE `ExpensesAnnotation`  ADD CONSTRAINT `fk_ExpensesAnnotation_Annotation1`
  FOREIGN KEY (`id` )
  REFERENCES `Annotation` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;
ALTER TABLE `GroupUserAssociation`  ADD CONSTRAINT `fk_table1_Groups1`
  FOREIGN KEY (`group` )
  REFERENCES `Groups` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE, 
  ADD CONSTRAINT `fk_table1_User1`
  FOREIGN KEY (`user` )
  REFERENCES `User` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, ADD UNIQUE INDEX `unique_group_user` (`group` ASC, `user` ASC) ;
ALTER TABLE `IncomeAnnotation`  ADD CONSTRAINT `fk_IncomeAnnotation_Annotation1`
  FOREIGN KEY (`id` )
  REFERENCES `Annotation` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;
ALTER TABLE `MovementExpensesAnnotation`  ADD CONSTRAINT `fk_MovementExpensesAnnotation_ExpensesAnnotation1`
  FOREIGN KEY (`id` )
  REFERENCES `ExpensesAnnotation` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE, 
  ADD CONSTRAINT `fk_MovementExpensesAnnotation_MovementIncomeAnnotation1`
  FOREIGN KEY (`movementIncomeAnnotation` )
  REFERENCES `MovementIncomeAnnotation` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;
ALTER TABLE `MovementIncomeAnnotation`  ADD CONSTRAINT `fk_StocksDeposit_IncomeAnnotation1`
  FOREIGN KEY (`id` )
  REFERENCES `IncomeAnnotation` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;
ALTER TABLE `Property`  ADD CONSTRAINT `fk_Property_Client1`
  FOREIGN KEY (`client` )
  REFERENCES `Client` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, ADD UNIQUE INDEX `unique_name_client` (`name` ASC, `client` ASC) ;
ALTER TABLE `PropertyBook`  ADD CONSTRAINT `fk_PropertyBook_Book1`
  FOREIGN KEY (`id` )
  REFERENCES `Book` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE, 
  ADD CONSTRAINT `fk_PropertyBook_Property1`
  FOREIGN KEY (`property` )
  REFERENCES `Property` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, DROP PRIMARY KEY 
, ADD PRIMARY KEY (`id`) ;
ALTER TABLE `PropertyExpensesAnnotation`  ADD CONSTRAINT `fk_PropertyExpensesAnnotation_ExpensesAnnotation2`
  FOREIGN KEY (`id` )
  REFERENCES `ExpensesAnnotation` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;
ALTER TABLE `PropertyIncomeAnnotation`  ADD CONSTRAINT `fk_PropertyIncomeAnnotation_IncomeAnnotation1`
  FOREIGN KEY (`id` )
  REFERENCES `IncomeAnnotation` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;
ALTER TABLE `StockExpensesAnnotation`  ADD CONSTRAINT `fk_StockExpensesAnnotation_ExpensesAnnotation1`
  FOREIGN KEY (`id` )
  REFERENCES `ExpensesAnnotation` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;
ALTER TABLE `StockIncomeAnnotation`  ADD CONSTRAINT `fk_StockIncomeAnnotation_IncomeAnnotation1`
  FOREIGN KEY (`id` )
  REFERENCES `IncomeAnnotation` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;
ALTER TABLE `StocksBook`  ADD CONSTRAINT `fk_StocksBook_Book1`
  FOREIGN KEY (`id` )
  REFERENCES `Book` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;
ALTER TABLE `UserClientAssociation`  ADD CONSTRAINT `fk_UserClientAssociation_User1`
  FOREIGN KEY (`user` )
  REFERENCES `User` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE, 
  ADD CONSTRAINT `fk_UserClientAssociation_Client1`
  FOREIGN KEY (`client` )
  REFERENCES `Client` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, ADD UNIQUE INDEX `unique_user_client` (`user` ASC, `client` ASC) ;


ALTER TABLE `Annotation` add COLUMN `concept_tmp` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL;
update Annotation annotation set annotation.concept_tmp = (select concept.name from Concept concept where concept.id = annotation.concept);
ALTER TABLE `Annotation` CHANGE COLUMN `concept` `concept` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL;
update Annotation annotation set annotation.concept = annotation.concept_tmp;
ALTER TABLE Annotation drop column concept_tmp;



DROP TABLE IF EXISTS `Concept` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
CREATE  TABLE IF NOT EXISTS `GeneralBook` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_GeneralBook_Book1`
    FOREIGN KEY (`id` )
    REFERENCES `Book` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

insert into GeneralBook(id) (select book.id from Book book where book.id not in ( select id from PropertyBook union select id from StocksBook ));


CREATE  TABLE IF NOT EXISTS `GeneralExpensesAnnotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_GeneralExpensesAnnotation_ExpensesAnnotation1` (`id` ASC) ,
  CONSTRAINT `fk_GeneralExpensesAnnotation_ExpensesAnnotation1`
    FOREIGN KEY (`id` )
    REFERENCES `ExpensesAnnotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

insert into GeneralExpensesAnnotation(id) (select expensesAnnotation.id from ExpensesAnnotation expensesAnnotation  where expensesAnnotation.id not in ( select id from PropertyExpensesAnnotation union select id from StockExpensesAnnotation ));

CREATE  TABLE IF NOT EXISTS `GeneralIncomeAnnotation` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_GeneralIncomeAnnotation_IncomeAnnotation1`
    FOREIGN KEY (`id` )
    REFERENCES `IncomeAnnotation` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

insert into GeneralIncomeAnnotation(id) (select incomeAnnotation.id from IncomeAnnotation incomeAnnotation  where incomeAnnotation.id not in ( select id from PropertyIncomeAnnotation union select id from StockIncomeAnnotation ));


CREATE  TABLE IF NOT EXISTS `PredefinedConcept` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `annotation_class_name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `is_default` TINYINT(1) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;
CREATE  TABLE IF NOT EXISTS `TimeDependentConstant` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `code` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `name` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `unique_code` (`code` ASC) ,
  UNIQUE INDEX `unique_name` (`name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE  TABLE IF NOT EXISTS `TimeDependentConstantValue` (
  `id` BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `year` INT(11) NULL DEFAULT NULL ,
  `numerical_value` BIGINT(32) NOT NULL ,
  `time_dependent_constant` BIGINT(32) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `unique_year_time_dependent_constant` (`year` ASC, `time_dependent_constant` ASC) ,
  INDEX `fk_TimeDependentConstants_TimeDependentConstant1` (`time_dependent_constant` ASC) ,
  CONSTRAINT `fk_TimeDependentConstants_TimeDependentConstant1`
    FOREIGN KEY (`time_dependent_constant` )
    REFERENCES `TimeDependentConstant` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE  TABLE IF NOT EXISTS `HistoricalStockValue` (
  `id`  BIGINT(32) NOT NULL AUTO_INCREMENT ,
  `year` INT NOT NULL,
  `month` INT NOT NULL,
  `symbol` VARCHAR(128) NOT NULL ,
  `numerical_value` BIGINT(32) NOT NULL , 
  `collection_date` DATE NOT NULL , 
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_HistoricalStockValue` (`year`, `month`, `symbol`)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Otros','GeneralExpensesAnnotation', true);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Otros','PropertyExpensesAnnotation', true);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Movimiento','MovementExpensesAnnotation', true);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Desconocido','StockExpensesAnnotation', true);

insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Otros','GeneralIncomeAnnotation', true);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Otros','PropertyIncomeAnnotation', true);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Movimiento','MovementIncomeAnnotation', true);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Desconocido','StockIncomeAnnotation', true);

insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Reparaciones','PropertyExpensesAnnotation', false);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Electricidad','PropertyExpensesAnnotation', false);
insert into PredefinedConcept(name, annotation_class_name, is_default) values ('Alquiler','PropertyIncomeAnnotation', false);

insert into TimeDependentConstant(id, name, code) values (1, "Porcentaje deducible por valor de la propiedad", "PERCENTAGE_DEDUCTIBLE_FOR_PROPERTY_VALUE");
insert into TimeDependentConstant(id, name, code) values (2, "Porcentaje de I.V.A. por defecto", "DEFAULT_VAT");
insert into TimeDependentConstant(id, name, code) values (3, "Porcentaje de I.R.P.F. por defecto", "DEFAULT_RETENTION");

insert into TimeDependentConstant(id, name, code) values (4, "Porcentaje aplicado a los beneficios en bolsa por debajo de 6.000€", "PERCENTAGE_BELOW_6000");
insert into TimeDependentConstant(id, name, code) values (5, "Porcentaje aplicado a los beneficios en bolsa entre 6.000€ y 30.000€", "PERCENTAGE_BETWEEN_6000_AND_30000");
insert into TimeDependentConstant(id, name, code) values (6, "Porcentaje aplicado a los beneficios en bolsa superiores a 30.000€", "PERCENTAGE_ABOVE_30000");
insert into TimeDependentConstant(id, name, code) values (7, "Porcentaje a tributar en Junio para beneficios en bolsa", "PERCENTAGE_FOR_JUNE");
insert into TimeDependentConstant(id, name, code) values (8, "Porcentaje a tributar en Noviembre para beneficios en bolsa", "PERCENTAGE_FOR_NOVEMBER");
	

insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (1, 2012,  300, 1);
insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (2, 2012, 2100, 2);
insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (3, 2012, 2100, 3);


insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (4, 2012, 2100, 4);
insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (5, 2012, 2500, 5);
insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (6, 2012, 2700, 6);
insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (7, 2012, 6000, 7);
insert into TimeDependentConstantValue(id, year, numerical_value, time_dependent_constant) values (8, 2012, 4000, 8);

alter table `StocksBook` drop column  `value`;
