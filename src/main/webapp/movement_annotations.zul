
<!--

    This file is part of Pau's Asset Manager Project.

    Pau's Asset Manager Project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pau's Asset Manager Project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.

	@author Pau Carré Cardona	

-->
<zk xmlns:c="client">
	<zscript>
	</zscript>
	<space /> 
	<grid model="@load(movement_annotations_vm.annotations)"
		width="1270px" mold="paging" pageSize="20"
		id="expenses_annotations_grid">
		<auxhead sclass="category-center">
			<auxheader colspan="13" rowspan="1">
				<label value="Año"></label>
				<spinner xmlns:w="client" constraint="no negative,no zero"
					value="@bind(movement_annotations_vm.currentYear)"
					onUpdateCurrentYear="@command('updateCurrentYear')" readonly="true" width="70px">
					<attribute w:name="onBind" >	
						if(btn = this.$n("btn"))
						        this.domListen_(document.body, "onZMouseUp", "_btnOut");
					</attribute>
					<attribute w:name="onUnbind">
						this.domUnlisten_(document.body, "onZMouseUp", "_btnOut");
					</attribute>
					<attribute name="onChanging">
						Clients.showBusy("Cargando datos...");
						Events.echoEvent("onUpdateCurrentYear", self, event.getValue());
					</attribute>
				</spinner>
			</auxheader>
		</auxhead>
		<auxhead>
			<auxheader colspan="1">
				<image src="funnel.png" />
				<combobox
					model="@load(movement_annotations_vm.getConceptsForFiltering('MovementExpensesAnnotation'))"
					selectedItem="@bind(movement_annotations_vm.annotationsFilter.concept)"
					onRefreshFilter="@command('refreshFilter')" readonly="true">
					<attribute name="onChange">
						Clients.showBusy("Cargando datos...");
						Events.echoEvent("onRefreshFilter", self, event.getValue());
					</attribute>
					<template name="model">
						<comboitem label="@load(each)">
						
							<attribute name="onCreate">
<![CDATA[
	if (each.getValue(null).equals(org.pau.assetmanager.viewmodel.utils.AnnotationsFilter.ALL_CONCEPTS)) {
		self.setStyle("font-weight: bold;");
	}
]]>
								</attribute>
						</comboitem>
					</template>
				</combobox>
			</auxheader>
			<auxheader colspan="1">
				<image src="funnel.png" />
				<checkbox label="Filtrar" id="date_filter_checkbox_id"
					onDateFilterDisabled="@command('disableDateFilter')">
					<attribute name="onCheck" >
<![CDATA[
	date_filter_listbox_id.setVisible(!date_filter_listbox_id.isVisible());
	if (!date_filter_listbox_id.isVisible()) {
		Clients.showBusy("Cargando datos...");
		Events.echoEvent("onDateFilterDisabled", self, null);
	}
]]>
								</attribute>
				</checkbox>
				<listbox visible="false" id="date_filter_listbox_id"
					width="140px">
					<listhead>
						<listheader label="" sort="auto" width="30px" />
						<listheader label="" sort="auto" align="right" />
					</listhead>
					<listitem>
						<listcell label="De" />
						<listcell label="">
							<datebox id="date_filter_from"
								format="dd/MM/yyyy"
								value="@bind(movement_annotations_vm.annotationsFilter.dateFrom)"
								onRefreshFilter="@command('refreshFilter')" width="100px" style="text-align:right;">
									<attribute name="onChange">
										Clients.showBusy("Cargando datos...");
										Events.echoEvent("onRefreshFilter", self, event.getValue());
									</attribute>
								</datebox>
						</listcell>
					</listitem>
					<listitem>
						<listcell label="A" />
						<listcell label="">
							<datebox id="date_filter_to"
								format="dd/MM/yyyy"
								value="@bind(movement_annotations_vm.annotationsFilter.dateTo)"
								onRefreshFilter="@command('refreshFilter')" width="100px" style="text-align:right;">
									<attribute name="onChange">
										Clients.showBusy("Cargando datos...");
										Events.echoEvent("onRefreshFilter", self, event.getValue());
									</attribute>
								</datebox>
						</listcell>
					</listitem>
				</listbox>
			</auxheader>
			<auxheader colspan="1">
				<image src="funnel.png" />
				<checkbox label="Filtrar" id="cost_filter_checkbox_id"
					onCostFilterDisabled="@command('disableCostFilter')">
					<attribute name="onCheck" >
<![CDATA[
	cost_filter_listbox_id.setVisible(!cost_filter_listbox_id.isVisible());
	if (!cost_filter_listbox_id.isVisible()) {
		Clients.showBusy("Cargando datos...");
		Events.echoEvent("onCostFilterDisabled", self, null);
	}
]]>
								</attribute>
				</checkbox>
				<listbox visible="false" id="cost_filter_listbox_id">
					<listhead>
						<listheader label="" sort="auto" width="30px" />
						<listheader label="" sort="auto" align="right" />
					</listhead>
					<listitem>
						<listcell label="De" />
						<listcell label="">
							<decimalbox id="cost_filter_from" width="50px"
								value="@bind(movement_annotations_vm.annotationsFilter.costFrom)"
								onRefreshFilter="@command('refreshFilter')" format="###.##" style="text-align:right;">
									<attribute name="onChange">
										Clients.showBusy("Cargando datos...");
										Events.echoEvent("onRefreshFilter", self, event.getValue());
									</attribute>
							</decimalbox>
						</listcell>
					</listitem>
					<listitem>
						<listcell label="A" />
						<listcell label="">
							<decimalbox id="cost_filter_to" width="50px"
								value="@bind(movement_annotations_vm.annotationsFilter.costTo)"
								onRefreshFilter="@command('refreshFilter')" format="###.##" style="text-align:right;">
									<attribute name="onChange">
										Clients.showBusy("Cargando datos...");
										Events.echoEvent("onRefreshFilter", self, event.getValue());
									</attribute>
							</decimalbox>
						</listcell>
					</listitem>
				</listbox>
			</auxheader>
			<auxheader colspan="6"></auxheader>
		</auxhead>
		<columns height="22px">
			<column width="200px" label="Concepto"
				sort="auto(concept)">
			</column>
			<column width="150px" label="Fecha" align="center"
				sort="auto(date)">
			</column>
			<column width="120px" label="Cantidad"
				align="center" sort="auto(amount)">
			</column>
			<column width="150px" label="Cliente Origen" align="center"
				sort="auto(date)">
			</column>
			<column width="150px" label="Libro Origen" align="center"
				sort="auto(date)">
			</column>
			<column width="150px" label="Cliente Destino" align="center"
				sort="auto(date)">
			</column>
			<column width="150px" label="Libro Destino" align="center"
				sort="auto(date)">
			</column>
			<column width="100px" label="Eliminar" align="center" />
			<column width="100px" label="Duplicar" align="center" />
		</columns>
		<template name="model">
			<row>
				<combobox
					model="@load(movement_annotations_vm.getConcepts('MovementExpensesAnnotation'))"
					selectedItem="@bind(each.concept)" hflex="1"
					autocomplete="false"
					
					onChange="@command('saveAnnotationWithConcept', annotation=each)"
					inplace="true" maxlength="80">
					<attribute name="onCreate">
<![CDATA[
	if (movement_annotations_vm.getLastestCreatedAnnotation() != null) {
		if (movement_annotations_vm.getLastestCreatedAnnotation().equals(
				each.getValue(null))) {
			self.setStyle("font-weight: bold;");
		}
	}
]]>
								</attribute>
					<template name="model">
						<comboitem label="@load(each)" />
					</template>
				</combobox>
				<div class='text-center'>
					<datebox inplace="true" value="@bind(each.date)"
					onChange="@command('saveAnnotation', annotation=each)"
						width="70%" format="dd/MM/yyyy" style="text-align:right;">
						<attribute name="onCreate">
<![CDATA[
	if (movement_annotations_vm.getLastestCreatedAnnotation() != null) {
		if (movement_annotations_vm.getLastestCreatedAnnotation().equals(
				each.getValue(null))) {
			self.setStyle("text-align:right;font-weight: bold;");
		}
	}
]]>
								</attribute>
					</datebox>
				</div>
				<div class='text-center'>
					<decimalbox value="@bind(each.twoDecimalLongCodifiedAmount) @converter('org.pau.assetmanager.viewmodel.converter.LongToStringDecimalConverter')"   
						onChange="@command('saveAnnotation', annotation=each)"
						inplace="true" format="###.##" style="text-align:right;" maxlength="9"
						width="100px">
						<attribute name="onCreate">
<![CDATA[
	if (movement_annotations_vm.getLastestCreatedAnnotation() != null) {
		if (movement_annotations_vm.getLastestCreatedAnnotation().equals(
				each.getValue(null))) {
			self.setStyle("text-align:right;font-weight: bold;");
		}
	}
]]>
								</attribute>
					</decimalbox>
				</div>
				<div class='text-center'>
					<label value="@load(each.book.client.name)">
					<attribute name="onCreate">
<![CDATA[
	if (movement_annotations_vm.getLastestCreatedAnnotation() != null) {
		if (movement_annotations_vm.getLastestCreatedAnnotation().equals(
				each.getValue(null))) {
			self.setStyle("text-align:right;font-weight: bold;");
		}
	}
]]>
								</attribute>
					</label>
				</div>
				<div class='text-center'>
					<label value="@load(each.book.name)">
					<attribute name="onCreate">
<![CDATA[
	if (movement_annotations_vm.getLastestCreatedAnnotation() != null) {
		if (movement_annotations_vm.getLastestCreatedAnnotation().equals(
				each.getValue(null))) {
			self.setStyle("text-align:right;font-weight: bold;");
		}
	}
]]>
								</attribute>
					</label>
				</div>
				<div class='text-center'>
					<label value="@load(each.movementIncomeAnnotation.book.client.name)">
					<attribute name="onCreate">
<![CDATA[
	if (movement_annotations_vm.getLastestCreatedAnnotation() != null) {
		if (movement_annotations_vm.getLastestCreatedAnnotation().equals(
				each.getValue(null))) {
			self.setStyle("text-align:right;font-weight: bold;");
		}
	}
]]>
								</attribute>
					</label>
				</div>
				<div class='text-center'>
					<label value="@load(each.movementIncomeAnnotation.book.name)">
					<attribute name="onCreate">
<![CDATA[
	if (movement_annotations_vm.getLastestCreatedAnnotation() != null) {
		if (movement_annotations_vm.getLastestCreatedAnnotation().equals(
				each.getValue(null))) {
			self.setStyle("text-align:right;font-weight: bold;");
		}
	}
]]>
								</attribute>
					</label>
				</div>
				<button label="Eliminar"
					onRemoveAnnotation="@command('deleteMovementAnnotation', annotation=each)">
					<attribute name="onCreate">
<![CDATA[
	if (movement_annotations_vm.getLastestCreatedAnnotation() != null) {
		if (movement_annotations_vm.getLastestCreatedAnnotation().equals(
				each.getValue(null))) {
			self.setStyle("font-weight: bold;");
		}
	}
]]>
								</attribute>
					<attribute name="onClick">
<![CDATA[
	Component thisSelf = self;
	org.pau.assetmanager.entities.Annotation annotation = each.getValue(null);
Messagebox.show("Está seguro que desea eliminar la anotación?", "Confirmación", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
	public void onEvent(Event evt) throws InterruptedException {
		if (evt.getName().equals("onOK")) {
			Clients.showBusy("Cargando datos...");
			Events.echoEvent("onRemoveAnnotation", thisSelf, annotation);
		}
	}
});
]]>
						</attribute>
				</button>
				<button label="Duplicar"
					onDuplicate="@command('duplicateMovementAnnotation', annotation=each)">
					<attribute name="onClick">
						Clients.showBusy("Cargando datos...");
						Events.echoEvent("onDuplicate", self, null);
					</attribute>
					<attribute name="onCreate">
<![CDATA[
	if (movement_annotations_vm.getLastestCreatedAnnotation() != null) {
		if (movement_annotations_vm.getLastestCreatedAnnotation().equals(
				each.getValue(null))) {
			self.setStyle("font-weight: bold;");
		}
	}
]]>
								</attribute>
				</button>
			</row>
		</template>
	</grid>
	<button label="Añadir Anotación"
			onClick="@global-command('createMovementAnnotationForBook', sourceBook=movement_annotations_vm.selectedBook)">
	</button>
	
	 
	<include id="add_movement_annotation" mode="defer" src="add_movement_annotation.zul" />

</zk>