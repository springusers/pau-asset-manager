/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.pau.assetmanager.business.AnnotationsBusiness;
import org.pau.assetmanager.business.BooksBusiness;
import org.pau.assetmanager.business.ClientsBusiness;
import org.pau.assetmanager.business.UsersBusiness;
import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.Annotation.AnnotationType;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.GeneralBook;
import org.pau.assetmanager.entities.GeneralIncomeAnnotation;
import org.pau.assetmanager.entities.User;
import org.pau.assetmanager.viewmodel.type.ClientDomainType;
import org.pau.assetmanager.viewmodel.utils.AnnotationsFilter;
import org.pau.assetmanager.viewmodel.utils.BookSelection;
import org.pau.assetmanager.viewmodel.utils.SortingCriteria;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;

/**
 * @author Pau Carré Cardona
 *
 */
public class AnnotationTest extends GenericTest {

	@Test
	public void annotationCRUDTests() throws ParseException {
		// create a user
		User user = UsersBusiness.persistUserByUsernameAndPassword("new_user",
				"fake_password");
		Assert.assertArrayEquals(new Object[] { "new_user", "fake_password" },
				new Object[] { user.getUsername(), user.getPassword() });

		// create a client
		Client client = ClientsBusiness.addClientByClientName(user,
				"new_client");
		Assert.assertEquals("new_client", client.getName());

		// create general book
		GeneralBook generalBook = new GeneralBook();
		generalBook.setClient(client);
		generalBook.setDescription("general_book_description");
		generalBook.setName("general_book");
		generalBook = BooksBusiness.addBook(generalBook);

		// create a generic annotation
		Annotation annotation = new GeneralIncomeAnnotation();
		annotation.setBook(generalBook);
		annotation.setTwoDecimalLongCodifiedAmount(1000L * 100L); // 1000.00
		annotation.setConcept("test");
		Date date = new SimpleDateFormat("dd-MM-yyyy").parse("20-10-2013");
		annotation.setDate(date);
		annotation = AnnotationsBusiness.createAnnotation(annotation);
		Assert.assertEquals(generalBook, annotation.getBook());
		Assert.assertEquals(new Long(1000L * 100L),
				annotation.getTwoDecimalLongCodifiedAmount());
		Assert.assertEquals("test", annotation.getConcept());
		Assert.assertEquals(date, annotation.getDate());

		// find annotation
		List<Annotation> annotations = AnnotationsBusiness
				.getAnnotationsWithFilter(Optional.<AnnotationType>absent(),
						BookSelection.fromBook(generalBook),
						Optional.<Integer>of(2013), ClientDomainType.CURRENT_CLIENT, SortingCriteria.ASCENDING, 
						AnnotationsFilter.emptyAnnotationsFilter());
		Assert.assertEquals(new Integer(1), new Integer(annotations.size()));
		Assert.assertEquals(annotation, annotations.iterator().next());

		// delete annotation
		AnnotationsBusiness.deleteAnnotation(annotation);
		annotations = AnnotationsBusiness.getAnnotationsWithFilter(Optional.<AnnotationType>absent(),
				BookSelection.fromBook(generalBook),
				Optional.<Integer>of(2013), ClientDomainType.CURRENT_CLIENT, SortingCriteria.ASCENDING, 
				AnnotationsFilter.emptyAnnotationsFilter());
		Assert.assertEquals(new Integer(0), new Integer(annotations.size()));

	}

	@Test
	public void annotationFindersTests() throws ParseException {
		// create a user
		User user = UsersBusiness.persistUserByUsernameAndPassword("new_user",
				"fake_password");
		Assert.assertArrayEquals(new Object[] { "new_user", "fake_password" },
				new Object[] { user.getUsername(), user.getPassword() });

		// create a client
		Client client = ClientsBusiness.addClientByClientName(user,
				"new_client");
		Assert.assertEquals("new_client", client.getName());

		// create general book
		GeneralBook generalBook = new GeneralBook();
		generalBook.setClient(client);
		generalBook.setDescription("general_book_description");
		generalBook.setName("general_book");
		generalBook = BooksBusiness.addBook(generalBook);

		// create a generic annotation
		Annotation annotation = new GeneralIncomeAnnotation();
		annotation.setBook(generalBook);
		annotation.setTwoDecimalLongCodifiedAmount(1000L * 100L); // 1000.00
		annotation.setConcept("test");
		Date date = new SimpleDateFormat("dd-MM-yyyy").parse("11-11-2013");
		annotation.setDate(date);
		annotation = AnnotationsBusiness.createAnnotation(annotation);
		Assert.assertEquals(generalBook, annotation.getBook());
		Assert.assertEquals(new Long(1000L * 100L),
				annotation.getTwoDecimalLongCodifiedAmount());
		Assert.assertEquals("test", annotation.getConcept());
		Assert.assertEquals(date, annotation.getDate());

		// find annotation for current client
		List<Annotation> annotations = AnnotationsBusiness
				.getAnnotationsWithFilter(Optional.<AnnotationType>absent(),
						BookSelection.fromBook(generalBook),
						Optional.<Integer>of(2013), ClientDomainType.CURRENT_CLIENT, SortingCriteria.ASCENDING, 
						AnnotationsFilter.emptyAnnotationsFilter());
		Assert.assertEquals(new Integer(1), new Integer(annotations.size()));
		Assert.assertEquals(annotation, annotations.iterator().next());

		// find annotation all clients
		annotations = AnnotationsBusiness.getAnnotationsWithFilter(Optional.<AnnotationType>absent(),
				BookSelection.fromBook(generalBook),
				Optional.<Integer>of(2013), ClientDomainType.ALL_CLIENTS, SortingCriteria.ASCENDING, 
				AnnotationsFilter.emptyAnnotationsFilter());
		Assert.assertEquals(new Integer(1), new Integer(annotations.size()));
		Assert.assertEquals(annotation, annotations.iterator().next());

		// find annotation all clients
		annotations = AnnotationsBusiness.getAnnotationsWithFilter(Optional.<AnnotationType>absent(),
				BookSelection.fromBook(generalBook),
				Optional.<Integer>of(2011), ClientDomainType.ALL_CLIENTS, SortingCriteria.ASCENDING, 
				AnnotationsFilter.emptyAnnotationsFilter());
		Assert.assertEquals(new Integer(0), new Integer(annotations.size()));

		// create another user
		User secondUser = UsersBusiness.persistUserByUsernameAndPassword(
				"new_user_2", "fake_password_2");
		Assert.assertArrayEquals(
				new Object[] { "new_user_2", "fake_password_2" }, new Object[] {
						secondUser.getUsername(), secondUser.getPassword() });

		// create another client
		Client secondClient = ClientsBusiness.addClientByClientName(secondUser,
				"new_client_2");
		Assert.assertEquals("new_client_2", secondClient.getName());

		// create another general book
		GeneralBook secondGeneralBook = new GeneralBook();
		secondGeneralBook.setClient(secondClient);
		secondGeneralBook.setDescription("general_book_description_2");
		secondGeneralBook.setName("general_book_2");
		secondGeneralBook = BooksBusiness.addBook(secondGeneralBook);

		// create another generic annotation
		Annotation secondAnnotation = new GeneralIncomeAnnotation();
		secondAnnotation.setBook(secondGeneralBook);
		secondAnnotation.setTwoDecimalLongCodifiedAmount(1000L * 100L * 2L); // 2000.00
		secondAnnotation.setConcept("test_2");
		Date secondDate = new SimpleDateFormat("dd-MM-yyyy")
				.parse("22-12-2013");
		secondAnnotation.setDate(secondDate);
		secondAnnotation = AnnotationsBusiness
				.createAnnotation(secondAnnotation);
		Assert.assertEquals(secondGeneralBook, secondAnnotation.getBook());
		Assert.assertEquals(new Long(1000L * 100L * 2L),
				secondAnnotation.getTwoDecimalLongCodifiedAmount());
		Assert.assertEquals("test_2", secondAnnotation.getConcept());
		Assert.assertEquals(secondDate, secondAnnotation.getDate());

		// find annotation for current client
		annotations = AnnotationsBusiness.getAnnotationsWithFilter(Optional.<AnnotationType>absent(),
				BookSelection.fromBook(secondGeneralBook),
				Optional.<Integer>of(2013), ClientDomainType.CURRENT_CLIENT, SortingCriteria.ASCENDING, 
				AnnotationsFilter.emptyAnnotationsFilter());
		Assert.assertEquals(new Integer(1), new Integer(annotations.size()));
		Assert.assertEquals(secondAnnotation, annotations.iterator().next());

		// find annotation all clients
		annotations = AnnotationsBusiness.getAnnotationsWithFilter(Optional.<AnnotationType>absent(),
				BookSelection.fromBook(secondGeneralBook),
				Optional.<Integer>of(2013), ClientDomainType.ALL_CLIENTS, SortingCriteria.ASCENDING, 
				AnnotationsFilter.emptyAnnotationsFilter());
		Assert.assertEquals(new Integer(2), new Integer(annotations.size()));
		Set<Annotation> annotationsFromDatabase = ImmutableSet.copyOf(annotations);
		Set<Annotation> expectedAnnotations = ImmutableSet.of(annotation, secondAnnotation);
		Assert.assertEquals(expectedAnnotations, annotationsFromDatabase);
		
		// find annotation all clients
		annotations = AnnotationsBusiness.getAnnotationsWithFilter(Optional.<AnnotationType>absent(),
				BookSelection.fromBook(secondGeneralBook),
				Optional.<Integer>of(2011), ClientDomainType.ALL_CLIENTS, SortingCriteria.ASCENDING, 
				AnnotationsFilter.emptyAnnotationsFilter());
		Assert.assertEquals(new Integer(0), new Integer(annotations.size()));

	}

}
