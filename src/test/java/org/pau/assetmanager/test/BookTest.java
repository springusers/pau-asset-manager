/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.pau.assetmanager.business.BooksBusiness;
import org.pau.assetmanager.business.ClientsBusiness;
import org.pau.assetmanager.business.UsersBusiness;
import org.pau.assetmanager.entities.Book;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.GeneralBook;
import org.pau.assetmanager.entities.User;

/**
 * @author Pau Carré Cardona
 *
 */
public class BookTest extends GenericTest {

	@Test
	public void bookCRUDTests() {
		// create a user
		User user = UsersBusiness.persistUserByUsernameAndPassword("new_user",
				"fake_password");
		Assert.assertArrayEquals(new Object[] { "new_user", "fake_password" },
				new Object[] { user.getUsername(), user.getPassword() });

		// create a client
		Client client = ClientsBusiness.addClientByClientName(user,
				"new_client");
		Assert.assertEquals("new_client", client.getName());

		// create a general book
		GeneralBook generalBook = new GeneralBook();
		generalBook.setClient(client);
		generalBook.setDescription("general_book_description");
		generalBook.setName("general_book");
		generalBook = BooksBusiness.addBook(generalBook);

		// find book
		List<Book> books = BooksBusiness.getBooksFromClient(client);
		Assert.assertEquals(new Integer(1), new Integer(books.size()));
		Assert.assertEquals(generalBook, books.iterator().next());

		// delete book
		BooksBusiness.removeBook(generalBook);
		books = BooksBusiness.getBooksFromClient(client);
		Assert.assertEquals(new Integer(0), new Integer(books.size()));

	}

}
