/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.test;

import java.text.ParseException;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.pau.assetmanager.business.BooksBusiness;
import org.pau.assetmanager.business.ClientsBusiness;
import org.pau.assetmanager.business.UsersBusiness;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.GeneralBook;
import org.pau.assetmanager.entities.GeneralExpensesAnnotation;
import org.pau.assetmanager.entities.GeneralIncomeAnnotation;
import org.pau.assetmanager.entities.User;

/**
 * @author Pau Carré Cardona
 * 
 */
public class GeneralBookTest extends GenericTest {

	@Test
	public void propertyPremiseTests() throws ParseException {
		// create a user
		User user = UsersBusiness.persistUserByUsernameAndPassword("new_user",
				"fake_password");
		Assert.assertArrayEquals(new Object[] { "new_user", "fake_password" },
				new Object[] { user.getUsername(), user.getPassword() });

		// create a client
		Client client = ClientsBusiness.addClientByClientName(user,
				"new_client");
		Assert.assertEquals("new_client", client.getName());

		// create property book
		GeneralBook generalBook = new GeneralBook();
		generalBook.setClient(client);
		generalBook.setDescription("general_book_description");
		generalBook.setName("general_book");
		generalBook = BooksBusiness.addBook(generalBook);
		
		
		generalIncomeAnnotation_done(generalBook);
		generalExpensesAnnotation(generalBook);

	}

	private void generalIncomeAnnotation_done(GeneralBook generalBook){
		GeneralIncomeAnnotation generalIncomeAnnotation_done = new GeneralIncomeAnnotation();
		generalIncomeAnnotation_done.setAmount(1000.0);
		generalIncomeAnnotation_done.setBook(generalBook);
		generalIncomeAnnotation_done.setConcept("test_concept");
		Date date = new Date();
		generalIncomeAnnotation_done.setDate(date);
		generalIncomeAnnotation_done.setDone(true);
		
		Assert.assertEquals(new Double(1000.0),
				generalIncomeAnnotation_done
						.getSignedAmount());
		Assert.assertEquals(new Long(100000L),
				generalIncomeAnnotation_done
						.getTwoDecimalLongCodifiedAmount());
	}

	private void generalExpensesAnnotation(GeneralBook generalBook){
		GeneralExpensesAnnotation generalExpensesAnnotation_done = new GeneralExpensesAnnotation();
		generalExpensesAnnotation_done.setAmount(1000.0);
		generalExpensesAnnotation_done.setBook(generalBook);
		generalExpensesAnnotation_done.setConcept("test_concept");
		Date date = new Date();
		generalExpensesAnnotation_done.setDate(date);
		
		Assert.assertEquals(new Double(-1000.0),
				generalExpensesAnnotation_done
						.getSignedAmount());
		Assert.assertEquals(new Long(100000L),
				generalExpensesAnnotation_done
						.getTwoDecimalLongCodifiedAmount());
	}

}
