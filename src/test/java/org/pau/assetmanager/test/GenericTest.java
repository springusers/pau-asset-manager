/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.Before;
import org.pau.assetmanager.utils.AssetManagerRuntimeException;
import org.pau.assetmanager.utils.EntityManagerProperties;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

/**
 * @author Pau Carré Cardona
 *
 */
public class GenericTest {

	@Before
	public void setUpDatabase() throws IOException {
		InputStream propertiesResource = getInputStreamFromFile("hibernate.properties");
		Properties properties = new Properties();
		properties.load(propertiesResource);
		propertiesResource.close();
		// create test database
		createDatabase(properties);
		// create test connection
		EntityManagerProperties.setUpCustomProperties(properties);
	}
	
	
	
	public static Date getDateFromString(String dateAsString) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date date = simpleDateFormat.parse(dateAsString);
			return date;
		} catch (ParseException e) {
			// throw uncatched exception --> this should crash the test
			throw new AssetManagerRuntimeException("Wrong data format 'dd/MM/yyyy' for string " + dateAsString );
		}
	}
	
	public static Integer getYearFromDate(Date date){
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		Integer year = calendar.get(Calendar.YEAR);
		return year;
	}
	

	private static void createDatabase(Properties properties) {
		try {
			Connection connection = getJDBCConnection(properties);
			ScriptRunner scriptRunner = new ScriptRunner(connection);
			InputStreamReader inputStreamReader = new InputStreamReader(
					getInputStreamFromFile("table_creation.sql"));
			scriptRunner.runScript(inputStreamReader);
			inputStreamReader.close();
			InputStreamReader inputStreamReaderInit = new InputStreamReader(
					getInputStreamFromFile("init.sql"));
			scriptRunner.runScript(inputStreamReaderInit);
			inputStreamReaderInit.close();
			scriptRunner.closeConnection();
		} catch (IOException e) {
			throw new RuntimeException("Error initalizing database", e);
		}
	}

	private static Connection getJDBCConnection(Properties properties) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection(
					properties.getProperty("hibernate.connection.url"),
					properties.getProperty("hibernate.connection.username"),
					properties.getProperty("hibernate.connection.password"));
			return connection;
		} catch (Exception e) {
			throw new RuntimeException("Error getting JDBC connection", e);
		}
	}

	private static InputStream getInputStreamFromFile(String file) {
		Reflections reflections = new Reflections(new ConfigurationBuilder()
				.addUrls(ClasspathHelper.forPackage("org.pau.assetmanager"))
				.setScanners(new ResourcesScanner()));
		Set<String> resources = reflections.getResources(Pattern.compile(file));
		if (resources.size() > 0) {
			String resourceFound = resources.iterator().next().toString();
			InputStream inputStream = GenericTest.class.getClassLoader()
					.getResourceAsStream(resourceFound);
			return inputStream;
		}
		throw new RuntimeException("Error getting the file '" + file
				+ "' from the class path.");
	}

}
