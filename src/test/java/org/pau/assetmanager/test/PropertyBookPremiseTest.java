/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.test;

import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.pau.assetmanager.business.AnnotationsBusiness;
import org.pau.assetmanager.business.BooksBusiness;
import org.pau.assetmanager.business.ClientsBusiness;
import org.pau.assetmanager.business.PropertiesBusiness;
import org.pau.assetmanager.business.UsersBusiness;
import org.pau.assetmanager.entities.Annotation;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.Property;
import org.pau.assetmanager.entities.PropertyBook;
import org.pau.assetmanager.entities.PropertyExpensesAnnotation;
import org.pau.assetmanager.entities.PropertyIncomeAnnotation;
import org.pau.assetmanager.entities.User;
import org.pau.assetmanager.viewmodel.QuarterlyReportViewModel;
import org.pau.assetmanager.viewmodel.YearlyReportViewModel;
import org.pau.assetmanager.viewmodel.grouping.AnnotationDateQuarterlyGroupingModel;
import org.pau.assetmanager.viewmodel.grouping.AnnotationDateYearlyGroupingModel;
import org.pau.assetmanager.viewmodel.grouping.QuarterlyHaciendaResults;
import org.pau.assetmanager.viewmodel.grouping.YearlyHaciendaResults;
import org.pau.assetmanager.viewmodel.type.PropertyType;
import org.pau.assetmanager.viewmodel.utils.BookSelection;

import com.google.common.collect.Lists;

/**
 * @author Pau Carré Cardona
 * 
 */
public class PropertyBookPremiseTest extends GenericTest {

	@Test
	public void propertyPremiseTests() throws ParseException {
		// create a user
		User user = UsersBusiness.persistUserByUsernameAndPassword("new_user",
				"fake_password");
		Assert.assertArrayEquals(new Object[] { "new_user", "fake_password" },
				new Object[] { user.getUsername(), user.getPassword() });

		// create a client
		Client client = ClientsBusiness.addClientByClientName(user,
				"new_client");
		Assert.assertEquals("new_client", client.getName());

		// create property (premise)
		Property property = new Property();
		property.setActualValue(10000.0);
		property.setClient(client);
		property.setName("new_property");
		property.setOfficialValue(10000.0);
		property.setType(PropertyType.PREMISE);
		property = PropertiesBusiness.createProperty(property);

		// create property book
		PropertyBook propertyBook = new PropertyBook();
		propertyBook.setClient(client);
		propertyBook.setDescription("property_book_description");
		propertyBook.setName("property_book");
		propertyBook.setProperty(property);
		propertyBook = BooksBusiness.addBook(propertyBook);

		Annotation propertyIncomeAnnotationDoneForCompany = getPropertyIncomeAnnotationDoneForCompany(propertyBook);
		Annotation propertyIncomeAnnotationNotDoneNotForCompany = getPropertyIncomeAnnotationNotDoneNotForCompany(propertyBook);
		Annotation propertyIncomeAnnotationDoneNotForCompany = getPropertyIncomeAnnotationDoneNotForCompany(propertyBook);
		Annotation propertyExpensesAnnotationNotCommunityUseQuarterly = getPropertyExpensesAnnotationNotCommunityUseQuarterly(propertyBook);
		Annotation propertyExpensesAnnotationNotCommunityNotUseQuarterly = getPropertyExpensesAnnotationNotCommunityNotUseQuarterly(propertyBook);
		
		List<Annotation> annotationsExpectedToBeUsedInQuarterly = Lists.newArrayList(propertyIncomeAnnotationDoneForCompany,
				propertyIncomeAnnotationDoneNotForCompany,propertyExpensesAnnotationNotCommunityUseQuarterly
				);
		checkQuarterly(propertyBook, annotationsExpectedToBeUsedInQuarterly);

		List<Annotation> annotationsExpectedToBeUsedInYearly = Lists
				.newArrayList(propertyIncomeAnnotationDoneForCompany, propertyIncomeAnnotationDoneNotForCompany,
						propertyExpensesAnnotationNotCommunityUseQuarterly, propertyExpensesAnnotationNotCommunityNotUseQuarterly);
		checkYearly(propertyBook, annotationsExpectedToBeUsedInYearly);
	}
	
	public void checkYearly(PropertyBook propertyBook,
			List<Annotation> annotationsExpectedToBeUsedInYearly) {
		YearlyReportViewModel yearlyReportViewModel = new YearlyReportViewModel();
		yearlyReportViewModel.updateBookSelection(BookSelection
				.fromBook(propertyBook));
		yearlyReportViewModel.setYearlyReportYear(2012);
		AnnotationDateYearlyGroupingModel annotationsDateYearlyGroupingModel = yearlyReportViewModel
				.getAnnotationsDateYearlyGroupingModel();
		Integer numberOfGroups = annotationsDateYearlyGroupingModel
				.getGroupCount();
		// we expect 1 property (the one selected)
		Assert.assertEquals(new Integer(1), numberOfGroups);
		for (Integer currentProperty = 0; currentProperty < numberOfGroups; currentProperty++) {
			List<Annotation> annotationsForProperty = new LinkedList<Annotation>();
			for (Integer annotationIndex = 0; annotationIndex < annotationsDateYearlyGroupingModel
					.getChildCount(currentProperty); annotationIndex++) {
				Annotation currentAnnotation = annotationsDateYearlyGroupingModel.getChild(
						currentProperty, annotationIndex);
				annotationsForProperty.add(currentAnnotation);
			}
			Assert.assertTrue(annotationsForProperty
					.containsAll(annotationsExpectedToBeUsedInYearly));
			YearlyHaciendaResults yearlyHaciendaResults  = YearlyHaciendaResults.compute(annotationsExpectedToBeUsedInYearly.toArray(new Annotation[0]));
			
			Assert.assertEquals(new Double(300.0), yearlyHaciendaResults.getAmortizacion());
			Assert.assertEquals(new Double(0.0), yearlyHaciendaResults.getComunidad());
			Assert.assertEquals(new Double(750.0), yearlyHaciendaResults.getGastosVarios());
			Assert.assertEquals(new Double(2000.0), yearlyHaciendaResults.getIngresos());
			Assert.assertEquals(new Double(400.0), yearlyHaciendaResults.getRetenciones());
		}

	}


	public void checkQuarterly(PropertyBook propertyBook, List<Annotation> annotationsExpectedToBeUsedInQuarterly) {
		QuarterlyReportViewModel quarterlyReportViewModel = new QuarterlyReportViewModel();
		quarterlyReportViewModel.updateBookSelection(BookSelection
				.fromBook(propertyBook));
		quarterlyReportViewModel.setQuarterlyReportYear(2012);
		AnnotationDateQuarterlyGroupingModel quarterlyGroupingModel = quarterlyReportViewModel
				.getAnnotationsDateQuarterlyGroupingModel();
		Integer numberOfGroups = quarterlyGroupingModel.getGroupCount();
		// we expect 1 quarter (all the annotations are in the first quarter)
		Assert.assertEquals(new Integer(1), numberOfGroups);
		for (Integer currentQuarter = 0; currentQuarter < numberOfGroups; currentQuarter++) {
			List<Annotation> annotationsForQuarter = new LinkedList<Annotation>();
			for (Integer annotationIndex = 0; annotationIndex < quarterlyGroupingModel
					.getChildCount(currentQuarter); annotationIndex++) {
				Annotation currentAnnotation = quarterlyGroupingModel.getChild(currentQuarter, annotationIndex);
				annotationsForQuarter.add(currentAnnotation);
			}
			Assert.assertEquals(new Integer(annotationsExpectedToBeUsedInQuarterly.size()), new Integer(annotationsForQuarter.size()));
			Assert.assertTrue(annotationsForQuarter.containsAll(annotationsExpectedToBeUsedInQuarterly));
			QuarterlyHaciendaResults haciendaResults = QuarterlyHaciendaResults.compute(annotationsForQuarter.toArray(new Annotation[0]));
			Assert.assertEquals(new Double(2000.0),haciendaResults.getBaseImponibleRepercutido());
			Assert.assertEquals(new Double(500.0),haciendaResults.getBaseImponibleSoportado());
			Assert.assertEquals(new Double(400.0),haciendaResults.getIvaRepercutido());
			Assert.assertEquals(new Double(500.0),haciendaResults.getIvaSoportado());
			Assert.assertEquals(new Double(-100.0),haciendaResults.getTotal());
		}
	}

	private Annotation getPropertyIncomeAnnotationDoneForCompany(
			PropertyBook propertyBook) {
		// property income: done and for_company
		PropertyIncomeAnnotation propertyIncomeAnnotation_done_for_company = new PropertyIncomeAnnotation();
		propertyIncomeAnnotation_done_for_company.setAmount(1000.0);
		propertyIncomeAnnotation_done_for_company.setBook(propertyBook);
		propertyIncomeAnnotation_done_for_company.setConcept("Renting");
		propertyIncomeAnnotation_done_for_company.setDate(getDateFromString("01/01/2012"));
		propertyIncomeAnnotation_done_for_company.setDone(true);
		propertyIncomeAnnotation_done_for_company.setForCompany(true);
		propertyIncomeAnnotation_done_for_company.setRetention(20.0);
		propertyIncomeAnnotation_done_for_company.setVat(20.0);
		propertyIncomeAnnotation_done_for_company = AnnotationsBusiness
				.createAnnotation(propertyIncomeAnnotation_done_for_company);

		// for premises, it is always a company
		Assert.assertFalse(propertyIncomeAnnotation_done_for_company
				.getAppliesForCompany());
		// for premises, retention and vat always applies when done
		Assert.assertTrue(propertyIncomeAnnotation_done_for_company
				.getAppliesRetentionAndVAT());
		// when vat = retention --> tax_base == amount
		Assert.assertEquals(new Double(1000.0),
				propertyIncomeAnnotation_done_for_company
						.getBaseImponibleQuarterly());
		// in case of property, base tax = base tax yearly
		Assert.assertEquals(new Double(1000.0),
				propertyIncomeAnnotation_done_for_company
						.getBaseImponibleYearly());
		// in case of property, declared = base tax yearly
		Assert.assertEquals(new Double(1000.0),
				propertyIncomeAnnotation_done_for_company.getDeclaredYearly());
		// for the properties the retention amount is the retention percentage
		// of the amount
		Assert.assertEquals(new Double(1000.0 * 20.0 / 100.0),
				propertyIncomeAnnotation_done_for_company
						.getRetentionAmountYearly());
		// for income the signed amount must be the same as the unsigned amount
		Assert.assertEquals(new Double(1000.0),
				propertyIncomeAnnotation_done_for_company.getSignedAmount());
		Assert.assertEquals(new Long(100000L),
				propertyIncomeAnnotation_done_for_company
						.getTwoDecimalLongCodifiedAmount());
		return propertyIncomeAnnotation_done_for_company;
	}

	private Annotation getPropertyIncomeAnnotationNotDoneNotForCompany(
			PropertyBook propertyBook) {
		// property income: not done and not for_company
		PropertyIncomeAnnotation propertyIncomeAnnotation_not_done_not_forComapany = new PropertyIncomeAnnotation();
		propertyIncomeAnnotation_not_done_not_forComapany.setAmount(1000.0);
		propertyIncomeAnnotation_not_done_not_forComapany.setBook(propertyBook);
		propertyIncomeAnnotation_not_done_not_forComapany.setConcept("Renting");
		propertyIncomeAnnotation_not_done_not_forComapany.setDate(getDateFromString("01/01/2012"));
		propertyIncomeAnnotation_not_done_not_forComapany.setDone(false);
		propertyIncomeAnnotation_not_done_not_forComapany.setForCompany(false);
		propertyIncomeAnnotation_not_done_not_forComapany.setRetention(20.0);
		propertyIncomeAnnotation_not_done_not_forComapany.setVat(20.0);
		propertyIncomeAnnotation_not_done_not_forComapany = AnnotationsBusiness
				.createAnnotation(propertyIncomeAnnotation_not_done_not_forComapany);

		// for premises, it is always a company
		Assert.assertFalse(propertyIncomeAnnotation_not_done_not_forComapany
				.getAppliesForCompany());
		// retention and vat never applies when not done
		Assert.assertFalse(propertyIncomeAnnotation_not_done_not_forComapany
				.getAppliesRetentionAndVAT());
		// not done --> 0.0
		Assert.assertEquals(new Double(0.0),
				propertyIncomeAnnotation_not_done_not_forComapany
						.getBaseImponibleQuarterly());
		// not done --> 0.0
		Assert.assertEquals(new Double(0.0),
				propertyIncomeAnnotation_not_done_not_forComapany
						.getBaseImponibleYearly());
		// not done --> 0.0
		Assert.assertEquals(new Double(0.0),
				propertyIncomeAnnotation_not_done_not_forComapany
						.getDeclaredYearly());
		// not done --> 0.0
		Assert.assertEquals(new Double(0.0),
				propertyIncomeAnnotation_not_done_not_forComapany
						.getRetentionAmountYearly());
		// for income the signed amount must be the same as the unsigned amount
		Assert.assertEquals(new Double(1000.0),
				propertyIncomeAnnotation_not_done_not_forComapany
						.getSignedAmount());
		Assert.assertEquals(new Long(100000L),
				propertyIncomeAnnotation_not_done_not_forComapany
						.getTwoDecimalLongCodifiedAmount());
		return propertyIncomeAnnotation_not_done_not_forComapany;
	}

	private Annotation getPropertyIncomeAnnotationDoneNotForCompany(
			PropertyBook propertyBook) {
		// property income: done and for_company
		PropertyIncomeAnnotation propertyIncomeAnnotation_done_not_for_company = new PropertyIncomeAnnotation();
		propertyIncomeAnnotation_done_not_for_company.setAmount(1000.0);
		propertyIncomeAnnotation_done_not_for_company.setBook(propertyBook);
		propertyIncomeAnnotation_done_not_for_company.setConcept("Renting");
		propertyIncomeAnnotation_done_not_for_company.setDate(getDateFromString("01/01/2012"));
		propertyIncomeAnnotation_done_not_for_company.setDone(true);
		propertyIncomeAnnotation_done_not_for_company.setForCompany(false);
		propertyIncomeAnnotation_done_not_for_company.setRetention(20.0);
		propertyIncomeAnnotation_done_not_for_company.setVat(20.0);
		propertyIncomeAnnotation_done_not_for_company = AnnotationsBusiness
				.createAnnotation(propertyIncomeAnnotation_done_not_for_company);

		// for premises, it is always a company
		Assert.assertFalse(propertyIncomeAnnotation_done_not_for_company
				.getAppliesForCompany());
		// for premises, retention and vat always applies when done
		Assert.assertTrue(propertyIncomeAnnotation_done_not_for_company
				.getAppliesRetentionAndVAT());
		// when vat = retention --> tax_base == amount
		Assert.assertEquals(new Double(1000.0),
				propertyIncomeAnnotation_done_not_for_company
						.getBaseImponibleQuarterly());
		// in case of property, base tax = base tax yearly
		Assert.assertEquals(new Double(1000.0),
				propertyIncomeAnnotation_done_not_for_company
						.getBaseImponibleYearly());
		// in case of property, declared = base tax yearly
		Assert.assertEquals(new Double(1000.0),
				propertyIncomeAnnotation_done_not_for_company
						.getDeclaredYearly());
		// for the properties the retention amount is the retention percentage
		// of the amount
		Assert.assertEquals(new Double(1000.0 * 20.0 / 100.0),
				propertyIncomeAnnotation_done_not_for_company
						.getRetentionAmountYearly());
		// for income the signed amount must be the same as the unsigned amount
		Assert.assertEquals(new Double(1000.0),
				propertyIncomeAnnotation_done_not_for_company.getSignedAmount());
		Assert.assertEquals(new Long(100000L),
				propertyIncomeAnnotation_done_not_for_company
						.getTwoDecimalLongCodifiedAmount());
		
		return propertyIncomeAnnotation_done_not_for_company;
	}

	private Annotation getPropertyExpensesAnnotationNotCommunityUseQuarterly(
			PropertyBook propertyBook) {
		PropertyExpensesAnnotation propertyExpensesAnnotation_not_community_useQuarterly = new PropertyExpensesAnnotation();
		propertyExpensesAnnotation_not_community_useQuarterly.setAmount(1000.0);
		propertyExpensesAnnotation_not_community_useQuarterly
				.setBook(propertyBook);
		propertyExpensesAnnotation_not_community_useQuarterly
				.setCommunity(false);
		propertyExpensesAnnotation_not_community_useQuarterly
				.setConcept("cleaning");
		propertyExpensesAnnotation_not_community_useQuarterly
				.setDate(getDateFromString("01/01/2012"));
		propertyExpensesAnnotation_not_community_useQuarterly
				.setDeductiblePercentage(50.0);
		propertyExpensesAnnotation_not_community_useQuarterly
				.setUseQuarterly(true);
		propertyExpensesAnnotation_not_community_useQuarterly.setVat(100.0);
		propertyExpensesAnnotation_not_community_useQuarterly = AnnotationsBusiness
				.createAnnotation(propertyExpensesAnnotation_not_community_useQuarterly);

		Assert.assertEquals(new Double(500.0),
				propertyExpensesAnnotation_not_community_useQuarterly
						.getBaseImponibleQuarterly());
		Assert.assertEquals(new Double(500.0),
				propertyExpensesAnnotation_not_community_useQuarterly
						.getBaseImponibleYearly());
		Assert.assertEquals(new Double(250.0),
				propertyExpensesAnnotation_not_community_useQuarterly
						.getDeclaredYearly());
		Assert.assertEquals(new Double(-1000.0),
				propertyExpensesAnnotation_not_community_useQuarterly
						.getSignedAmount());
		Assert.assertEquals(new Long(100000L),
				propertyExpensesAnnotation_not_community_useQuarterly
						.getTwoDecimalLongCodifiedAmount());
		
		return propertyExpensesAnnotation_not_community_useQuarterly;
	}

	private Annotation getPropertyExpensesAnnotationNotCommunityNotUseQuarterly(
			PropertyBook propertyBook) {
		PropertyExpensesAnnotation propertyExpensesAnnotation_not_community_not_useQuarterly = new PropertyExpensesAnnotation();
		propertyExpensesAnnotation_not_community_not_useQuarterly
				.setAmount(1000.0);
		propertyExpensesAnnotation_not_community_not_useQuarterly
				.setBook(propertyBook);
		propertyExpensesAnnotation_not_community_not_useQuarterly
				.setCommunity(false);
		propertyExpensesAnnotation_not_community_not_useQuarterly
				.setConcept("cleaning");
		propertyExpensesAnnotation_not_community_not_useQuarterly
				.setDate(getDateFromString("01/01/2012"));
		propertyExpensesAnnotation_not_community_not_useQuarterly
				.setDeductiblePercentage(50.0);
		propertyExpensesAnnotation_not_community_not_useQuarterly
				.setUseQuarterly(false);
		propertyExpensesAnnotation_not_community_not_useQuarterly.setVat(100.0);
		propertyExpensesAnnotation_not_community_not_useQuarterly = AnnotationsBusiness
				.createAnnotation(propertyExpensesAnnotation_not_community_not_useQuarterly);

		Assert.assertEquals(new Double(0.0),
				propertyExpensesAnnotation_not_community_not_useQuarterly
						.getBaseImponibleQuarterly());
		Assert.assertEquals(new Double(1000.0),
				propertyExpensesAnnotation_not_community_not_useQuarterly
						.getBaseImponibleYearly());
		Assert.assertEquals(new Double(500.0),
				propertyExpensesAnnotation_not_community_not_useQuarterly
						.getDeclaredYearly());
		Assert.assertEquals(new Double(-1000.0),
				propertyExpensesAnnotation_not_community_not_useQuarterly
						.getSignedAmount());
		Assert.assertEquals(new Long(100000L),
				propertyExpensesAnnotation_not_community_not_useQuarterly
						.getTwoDecimalLongCodifiedAmount());
		
		return propertyExpensesAnnotation_not_community_not_useQuarterly;
	}

}
