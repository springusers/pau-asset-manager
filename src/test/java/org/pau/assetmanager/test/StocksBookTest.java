/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.test;

import java.text.ParseException;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.pau.assetmanager.business.BooksBusiness;
import org.pau.assetmanager.business.ClientsBusiness;
import org.pau.assetmanager.business.UsersBusiness;
import org.pau.assetmanager.entities.Client;
import org.pau.assetmanager.entities.StockExpensesAnnotation;
import org.pau.assetmanager.entities.StockIncomeAnnotation;
import org.pau.assetmanager.entities.StocksBook;
import org.pau.assetmanager.entities.User;

/**
 * @author Pau Carré Cardona
 * 
 */
public class StocksBookTest extends GenericTest {

	@Test
	public void propertyPremiseTests() throws ParseException {
		// create a user
		User user = UsersBusiness.persistUserByUsernameAndPassword("new_user",
				"fake_password");
		Assert.assertArrayEquals(new Object[] { "new_user", "fake_password" },
				new Object[] { user.getUsername(), user.getPassword() });

		// create a client
		Client client = ClientsBusiness.addClientByClientName(user,
				"new_client");
		Assert.assertEquals("new_client", client.getName());

		// create property book
		StocksBook stocksBook = new StocksBook();
		stocksBook.setClient(client);
		stocksBook.setDescription("stocks_book_description");
		stocksBook.setName("stocks_book");
		stocksBook = BooksBusiness.addBook(stocksBook);
		
		stocksIncomeAnnotation(stocksBook);
		stockExpensesAnnotation(stocksBook);

	}

	private void stocksIncomeAnnotation(StocksBook stocksBook){
		StockIncomeAnnotation stocksIncomeAnnotation_done = new StockIncomeAnnotation();
		stocksIncomeAnnotation_done.setAmount(1000.0);
		stocksIncomeAnnotation_done.setBook(stocksBook);
		stocksIncomeAnnotation_done.setConcept("test_concept");
		Date date = new Date();
		stocksIncomeAnnotation_done.setDate(date);
		stocksIncomeAnnotation_done.setDone(true);
		stocksIncomeAnnotation_done.setNumberOfStocks(100L); 
		
		Assert.assertEquals(new Double(1000.0),
				stocksIncomeAnnotation_done
						.getSignedAmount());
		Assert.assertEquals(new Long(100000L),
				stocksIncomeAnnotation_done
						.getTwoDecimalLongCodifiedAmount());
	}

	private void stockExpensesAnnotation(StocksBook stocksBook){
		StockExpensesAnnotation stockExpensesAnnotation_done = new StockExpensesAnnotation();
		stockExpensesAnnotation_done.setAmount(1000.0);
		stockExpensesAnnotation_done.setBook(stocksBook);
		stockExpensesAnnotation_done.setConcept("test_concept");
		Date date = new Date();
		stockExpensesAnnotation_done.setDate(date);
		stockExpensesAnnotation_done.setNumberOfStocks(100L);
		
		Assert.assertEquals(new Double(-1000.0),
				stockExpensesAnnotation_done
						.getSignedAmount());
		Assert.assertEquals(new Long(100000L),
				stockExpensesAnnotation_done
						.getTwoDecimalLongCodifiedAmount());
	}

}
