/**
 * This file is part of Pau's Asset Manager Project.
 *
 * Pau's Asset Manager Project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pau's Asset Manager Project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pau's Asset Manager Project.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pau.assetmanager.test;

import org.junit.Assert;
import org.junit.Test;
import org.pau.assetmanager.business.UsersBusiness;
import org.pau.assetmanager.entities.User;

import com.google.common.base.Optional;

/**
 * @author Pau Carré Cardona
 *
 */
public class UserTest extends GenericTest {

	@Test
	public void userCRUDTests() {
		Optional<User> optionalUser = UsersBusiness
				.getUserByUsername("unexistent_user");
		// unexistent user should not exist
		Assert.assertFalse(optionalUser.isPresent());
		User user = UsersBusiness.persistUserByUsernameAndPassword("new_user",
				"fake_password");
		// check newly create user
		Assert.assertArrayEquals(new Object[] { "new_user", "fake_password" },
				new Object[] { user.getUsername(), user.getPassword() });
		Optional<User> optionalNewUser = UsersBusiness
				.getUserByUsername("new_user");
		// check user finder
		Assert.assertTrue(optionalNewUser.isPresent());
		UsersBusiness.deleteUser(optionalNewUser.get());
		Optional<User> optionalDeltedNewUser = UsersBusiness
				.getUserByUsername("new_user");
		// check user deletion
		Assert.assertFalse(optionalDeltedNewUser.isPresent());
	}
}
